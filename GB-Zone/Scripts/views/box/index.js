﻿//view/box/index
//Funcionality to add inventory to new box or update box
const INPUT_TEXT_B_CHECK = "texBCheck";



var sysColumnInventoryGetData = "";
var sysColumnStorageLocation4 = "";
var originalItemInBox = new Array();
var itemInBox = new Array();
var itemInsertBox = new Array();


function reorderItem(s, e) {
    var rows = gvSearch.GetVisibleRowsOnPage();
    for (var i = 0; i < rows; i++) {
        var entry = "00" + (i + 1);
        gvSearch.batchEditApi.SetCellValue(i, sysColumnStorageLocation4, entry.substring(entry.length - 3));
    }
}

function EndCallback(s, e) {
    ValidateError();
}
function GetDataChange() {
    InsertData(gvSearch.cpJsonInsertData);
    UpdateData(gvSearch.cpJsonUpdateData);
}

function InsertData(jsonDataString) {
    let jsonData = $.parseJSON(jsonDataString);
    jsonData.forEach(function (item) {
        let text = item[sysColumnInventoryGetData];
        AddEnter(text);
    });
}

function UpdateData(jsonDataString) {
    let jsonData = $.parseJSON(jsonDataString);
    jsonData.forEach(function (item) {
        let inventory = item[sysColumnInventoryGetData];
        let text = item[sysColumnStorageLocation4];
        let key = GetRowNowId(inventory);
        gvSearch.batchEditApi.SetCellValue(key, sysColumnStorageLocation4, text);
    });

}

function ColorError() {
    if (gvSearch.cpJsonError !== undefined) {
        let jsonData = $.parseJSON(gvSearch.cpJsonError);
        jsonData.forEach(function (item) {
            Color(item, false);
        });
    }
}

function ValidateError() {
    var rows = gvSearch.GetVisibleRowsOnPage();
    SetTotal(rows);
    if (gvSearch.cpSuccess === "200") {
        Init();
        ShowModalResult("Saved data! ", ICON_SUCCESS);
    } else {
        ShowModalResult(gvSearch.cpMessageError, ICON_FAIL);
        GetDataChange();
        setTimeout(ColorError, 2000);
    }
}

function SetTotal(num_rows) {
    $("#spanCount").text(num_rows);
}

function Init() {
    saveDS = new Array();
    itemInBox = new Array();
    itemInsertBox = new Array();
    sysColumnInventoryGetData = gvSearch.cpSysColumnInventoryGetData;
    sysColumnStorageLocation4 = gvSearch.cpSysColumnStorageLocation4;
    $("#" + INPUT_TEXT_B_CHECK).attr("placeholder", sysColumnInventoryGetData);
    let jsonData = $.parseJSON(gvSearch.cpJsonItemInBox);
    rowNumAllToStopModal = Object.keys(jsonData).length;
    jsonData.forEach(function (item) {
        let text = item[sysColumnInventoryGetData];
        text = text.toUpperCase();
        saveDS.push(text);
        itemInBox.push(text);
    });
}

$(document).ready(function () {
    InitJQuery();
});

function InitJQuery() {
    $("#" + INPUT_TEXT_B_CHECK).keyup(function (e) {
        if (e.keyCode === 13) {
            let text = $("#" + INPUT_TEXT_B_CHECK).val();
            text = text.toUpperCase().trim();
            if (text !== "") {
                if (FindInevtory(text)) {
                    ShowModalResult("This inventory is already in stock [" + text + "]", ICON_FAIL);
                    Color(text, true);
                } else {
                    AddEnter(text);
                    itemInBox.push(text);
                    itemInsertBox.push(text);
                }
                setTimeout(function () {
                    $("#" + INPUT_TEXT_B_CHECK).val("");
                    $("#" + INPUT_TEXT_B_CHECK).focus();
                }, 100);
            }
        }
    });
}

function AddEnter(text) {
    gvSearch.AddNewRow();
    gvSearch.batchEditApi.StartEdit();
    let rows = gvSearch.GetVisibleRowsOnPage();
    SetTotal(rows);
    let rowInsertNew = gvSearch.batchEditHelper.GetEditState().insertedRowValues;
    let count = Object.keys(rowInsertNew).length;
    let entry = "00" + rows;
    let key;
    for (key in rowInsertNew) {
        //no empty
    }
    setTimeout(function () {
        gvSearch.batchEditApi.SetCellValue(key, sysColumnStorageLocation4, entry.substring(entry.length - 3));
        gvSearch.batchEditApi.SetCellValue(key, sysColumnInventoryGetData, text);
    }, 100);
}

function FindInevtory(text) {
    let found = itemInBox.find(element => element === text);
    let existe = true;
    if (typeof found === "undefined") {
        existe = false;
    }
    return existe;
}

function FindInevtoryIndex(text) {
    let index = itemInBox.findIndex(element => element === text);
    return index;
}

function GetRowNow(text) {
    let indexFind = itemInBox.findIndex(element => element === text);
    let row_now = gvSearch.GetRow(indexFind);
    if (row_now === null) {
        indexFind = itemInsertBox.findIndex(element => element === text);
        indexFind = (indexFind + 1) * -1;
        row_now = gvSearch.GetRow(indexFind);
    }
    return row_now;
}

function GetRowNowId(text) {
    let indexFind = itemInBox.findIndex(element => element === text);
    let row_now = gvSearch.GetRow(indexFind);
    if (row_now === null) {
        indexFind = itemInsertBox.findIndex(element => element === text);
        indexFind = (indexFind + 1) * -1;
        row_now = gvSearch.GetRow(indexFind);
    }
    return indexFind;
}

function Color(text, temp) {
    let row_now = GetRowNow(text);
    let id = $(row_now).attr("id");
    let previousColor = $("#" + id).css('background-color');
    $("#" + id).css({ 'background-color': COLOR_ERROR });
    if (temp) {
        setTimeout(function () {
            $("#" + id).css({ 'background-color': previousColor });
        }, 10000);
    }
}

function BatchEditChangesCanceling() {
    itemInBox = [...originalItemInBox];
}

function ShowModalResult(text, icon) {
    $("#pModalError").text(text);
    $("#spanModalError").html(icon);
    $("#resutlModal").modal({
        show: true
    });
}