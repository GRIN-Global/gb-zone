﻿const COLOR_ERROR_LIGHT = "#FDE5AD";
const COLOR_ERROR = "red";
const COLOR_NEUTRO = "white";
const KEY_ENTER = 13;
const COLOR_SUCCESS = "forestgreen";
const ICON_SUCCESS = '<i class="fa fa-check fa-lg" style="color:green"></i>';
const ICON_FAIL = '<i class="fa fa-times fa-lg" style="color:red"></i>';
const SUCCES_OK = "200";

function ShowModalResult(text, icon) {
    $("#pModalError").text(text);
    $("#spanModalError").html(icon);
    $("#resutlModal").modal({
        show: true
    });
}

function ConfirmModal(activate, title, message) {
    if (activate === true) {
        $("#confirmModalTitleH").html(title);
        $("#confimrModalTextDiv").html(message);
        $("#confirmModal").modal({
            backdrop: "static", //remove ability to close modal with click
            keyboard: false,
            show: true
        });
    } else {
        $("#confirmModal").modal("hide");
    }
}

function ModalLoad(activate, message) {
    if (activate === true) {
        $("#textLoadModal").html(message);
        $("#loadModal").modal({
            backdrop: "static", //remove ability to close modal with click
            keyboard: false, //remove option to close with keyboard
            show: true //Display loader!
        });
    } else if (activate === false) {
        $("#loadModal").modal("hide");
    }
}

function InitJQueryParent() {
    if (menssageError !== '') {
        ShowModalResult(menssageError, ICON_FAIL);
    }
}

function ValidateError(succes, messageError, messageOk) {
    if (success === SUCCES_OK) {
        ShowModalResult(messageOk, ICON_SUCCESS);
    } else {
        ShowModalResult(messageError, ICON_FAIL);
    }
}
