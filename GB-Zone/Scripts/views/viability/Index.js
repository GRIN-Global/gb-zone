﻿//# view/viability/index
//script to functionality index page view viability, dinamic search by filter
$(document).ready(function () {
    InitJQuery();
});
var jsonValuesText = new Array();
var dataFull = new Array();
var dataIncremnetYear = new Array();
var dataAccesionNumber = new Array();
var dataPopulationType = new Array();
var dataReproductiveUniform = new Array();

function InitJQuery() {
    $("form").keypress(function (e) {
        if (e.which === 13) {
            return false;
        }
    });
    $("#" + sysFieldNameIncrementYear).focusout(function (e) {
        let value = $("#" + sysFieldNameIncrementYear).val();
        if (value !== jsonValuesText[sysFieldNameIncrementYear]) {
            let formData = new FormData();
            formData.append(sysFieldNameIncrementYear, value);
            SendData(sysFieldNameIncrementYear, urlGetData, formData);
        }
    });
    $("#buttonSubmit").on("click", function (e) {
        LoadGrid(e);
    });
    jsonValuesText = new Array();
    jsonValuesText[sysFieldNameIncrementYear] = "";
    $("#spanSubmit").append('<i id="iSearch" class="fa fa-spinner" aria-hidden="true">Searh items</i> <strong> total items found: </strong><span id="spantotal"> </span> ');
    $("#iSearch").hide();
}

function SendData(field, url, formData) {
    BeforAjax();
    $.ajax({
        type: "POST",
        url: url,
        cache: false,
        contentType: false,
        processData: false,
        data: formData,
        dataType: 'json',
        success: function (data) {
            ProcessDataJson(data, field);
        },
        error: function (data) { AfterAjax(); }
    });
}

function ProcessDataJson(data, field) {
    if (field === sysFieldNameIncrementYear) {
        dataIncremnetYear = new Array();
        let jsonData = $.parseJSON(data);
        jsonData.forEach(function (item) {
            let inventory = [item[sysFieldNamePopulationType], item[sysFieldNameReproductiveUniform], item[sysFieldNameInventoryNumberPart3], item[sysFieldNameTotal]];
            dataIncremnetYear.push(inventory);
        });
        console.log(dataIncremnetYear);
        AddSelect(sysFieldNamePopulationType, 0);
        AddSelect(sysFieldNameReproductiveUniform, 1);
        AddSelect(sysFieldNameInventoryNumberPart3, 2);
        $("#" + sysFieldNamePopulationType).show();
        $("#" + sysFieldNameReproductiveUniform).show();
        $("#" + sysFieldNameInventoryNumberPart3).show();
        setTimeout(function () {
            $("#spantotal").html(GetAllInIncrementYear($("#" + sysFieldNamePopulationType).val(), $("#" + sysFieldNameReproductiveUniform).val()));
        }, 1000);

        AfterAjax();
    }
}
/*
function SuccessGetData(data) {
    let jsonData = $.parseJSON(data);
    let newSet = new Array();
    jsonData.forEach(function (item) {
        let inventory = [item[sysFieldNamePopulationType], item[sysFieldNameReproductiveUniform],item[sysFieldNameTotal]];
        newSet.push(inventory);
    });
    return newSet;
}

function Merge() {
    let allArray = new Array();
    if(dataIncremnetYear.length>0){
        allArray.push(dataIncremnetYear);
    }
    switch (allArray.length) {
        case 1:
            dataFull = allArray[0];
            break;
        case 2:
            setC = new Set(allArray[0].filter(x => new Set(allArray[1]).has(x)));
            dataFull = [...setC];
            break;
        case 3:
            setC = new Set(allArray[0].filter(x => new Set(allArray[1]).has(x)));
            setD = new Set([...setC].filter(x =>new Set(allArray[2]).has(x)));
            dataFull = [...setD];
            break;
        case 4:
            setC = new Set(allArray[0].filter(x => new Set(allArray[1]).has(x)));
            setD = new Set([...setC].filter(x =>new Set(allArray[2]).has(x)));
            setE = new Set([...setC].filter(x =>new Set(allArray[3]).has(x)));
            dataFull = [...setE];
            break;
        default:
            dataFull = new Array();
            break;
    }
    $("#" + sysInventoryId).val(dataFull.join(","));
    $("#spantotal").html(dataFull.length);
}

*/
function AddSelect(idSelect, index) {
    let setA = new Set(GetColumnIncrementeYear(index));
    let arr = [...setA];
    $("#" + idSelect).html("");
    arr.forEach(element => {
        $("#" + idSelect).append(new Option(element, element, true, true));
    }
    );
}

function GetColumnIncrementeYear(index) {
    let result = new Array();
    dataIncremnetYear.forEach(e => {
        result.push(e[index]);
    });
    return result;
}

function BeforAjax() {
    $("#spantotal").html("-");
    $("#buttonSubmit").attr("disabled", true);
    $("#iSearch").show();
}

function AfterAjax() {
    $('#buttonSubmit').removeAttr("disabled");
    $("#iSearch").hide();
}
function GetAllInIncrementYear(popultaion, reproductation, inventoryNumberPart3) {
    let sum = 0;
    let population = dataIncremnetYear.filter(p=> p[0] === popultaion);
    let populationReproductation = population.filter(p=> p[1] === reproductation);
    let pupulationReproductationInventoryNumberPart3 = populationReproductation.filter(p=> p[2] === inventoryNumberPart3);
    pupulationReproductationInventoryNumberPart3.forEach(e => {
        sum = sum + e[3];
    });
    return sum;
}

function LoadGrid(e) {
    e.preventDefault();
    let formData = new FormData();
    formData.append(sysFieldNameIncrementYear, $("#" + sysFieldNameIncrementYear).val());
    formData.append(sysFieldNamePopulationType, $("#" + sysFieldNamePopulationType).val());
    formData.append(sysFieldNameReproductiveUniform, $("#" + sysFieldNameReproductiveUniform).val());
    formData.append(sysFieldNameInventoryNumberPart3, $("#" + sysFieldNameInventoryNumberPart3).val());
    SendData(sysPreload, urlPreload, formData);

}

