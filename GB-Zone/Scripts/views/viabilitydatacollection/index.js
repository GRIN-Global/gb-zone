﻿// vaibilitydatacollecion/index/index.js
$(document).ready(function () {
    InitJQuery();
});

function InitJQuery() {
    $("#divContendCampureData").hide();
    HiddenShowHPanel('divHpanelViabilityHistory');
    HiddenShowHPanel('divHpanelInventoryViabilityData');
    $(".showhidegrid").on("click", function (s) { ShowHiddeGridHPanel(s); });
    $("#buttonSubmit").on("click", function (event) { SubmitButton(event); });
    $("#buttonSave").on("click", function (event) { SaveButton(event); });
    $("#buttonDelete").on("click", function (event) { DeleteButton(event); });
    $("#buttonClear").on("click", function (event) { ClearButton(event); });
    $("#inputCountAbnormal").focusout(function (event) { CountNormal(); });
    $("#inputCountAbnormal").keypress(function (event) { KeyPressOnlyNumber(event); });
    $("#inputCountDead").focusout(function (event) { CountNormal(); });
    $("#inputCountHard").focusout(function (event) { CountNormal(); });
    $("#inputCountInfested").focusout(function (event) { CountNormal(); });
    $("#inputCountDormant").focusout(function (event) { CountNormal(); });
    $("#inputCountEmpty").focusout(function (event) { CountNormal(); });
    $("#inputCountUnknown").focusout(function (event) { CountNormal(); });
    $("#inputStartingDate").focusout(function (event) { $("#inputDay").val(DateDiffDay($("#inputStartingDate").val(), $("#inputCountDate").val())); });
    $("#inputCooperatorId").focusout(function (event) { ValidateCooperator(); });
}
/**
* Only permit number without sign or without  point decimal and back
* @param {event} e - event key press.
*/
function KeyPressOnlyNumber(e) {
    if (!((e.keyCode > 95 && e.keyCode < 106) || (e.keyCode > 47 && e.keyCode < 58) || e.keyCode === 8)) {
        e.preventDefault();
    }
}
//#region Event to button
function SubmitButton(event) {
    event.preventDefault();
    try {
        SearchInventory(event);
    } catch (error) {
        $("#divContendCampureData").hide();
        Clear();
        ShowModalResult('QR error is not valid or not exist Id', ICON_FAIL);
    }
}

function ClearButton() {
    ClearCount();
    CountNormal();
}

function DeleteButton(event) {
    let result = CreateResult();
    result = ValidateNote(result, $("#inputNote").val());
    if (result.Success === 200) {
        $("#confirmModalButtonContinue").on("click", function (s) { DeleteSave(s); });
        $("#confirmModalButtonCancel").on("click", function (s) { DeleteCancel(s); });
        ConfirmModal(true, "DELETE experiment", "Are you sure you delete this experiment? The information is saved as null.");
    } else {
        ShowModalResult(result.Message, ICON_FAIL);
    }
}

function SaveButton() {
    let result = ValidateForm();
    if (result.Success === 200) {
        let formData = new FormData();
        formData.append("abnormal_count", $("#inputCountAbnormal").val());
        formData.append("dead_count", $("#inputCountDead").val());
        formData.append("hard_count", $("#inputCountHard").val());
        formData.append("infested_count", $("#inputCountInfested").val());
        formData.append("dormant_count", $("#inputCountDormant").val());
        formData.append("empty_count", $("#inputCountEmpty").val());
        formData.append("unknown_count", $("#inputCountUnknown").val());
        formData.append("normal_count", $("#inputCountNormal").val());
        formData.append("inputGermination", $("#inputGermination").val());
        formData.append("tested_date", $("#inputStartingDate").val());
        formData.append("inventory_viability_id", $("#inventory_viability_id").val());
        formData.append("replication_number", $("#inputReplicante").val());
        formData.append("note", $("#inputNote").val());
        formData.append("count_number", $("#inputSeedPerReplicateRule").val());
        formData.append("counter_cooperator_id", $("#inputCooperatorId").val());
        SaveData(urlSave, formData);
    } else {
        ShowModalResult(result.Message, ICON_FAIL);
        $("#modalButtonClose").click(function () { CloseModalScroll(); });
    }
}

function Clear() {
    $("#inputInventoryNumber").val("");
    $("#inputAccesion").val("");
    $("#inventory_id").val("");
    $("#inputAccesionName").val("");
    $("#inputTaxonomy").val("");
    $("#inputNameRule").val("");
    $("#inputNumberReplicanteRule").val("");
    $("#inputSeedPerReplicateRule").val("");
    $("#inputCountDate").val("");
    $("#inputStartingDate").val("");
    $("#inputCountNormal").val("");
    $("#inputDay").val("");
    $("#inputReplicante").val("");
    $("#divReplicanteNumber").html("");
    $("#inventory_viability_id").val();
    $("#inputGermination").val("");
    $("#inputStartingDate").val("");
    $("#inventory_viability_id").val("");
    $("#inputReplicante").val("");
    $("#inputNote").val("");
    $("#inputSeedPerReplicateRule").val("");
    $("#inventory_viability_id_readonly").val("");
    //$("#inputCooperatorId").val("");
    ClearCount();
}

function ClearCount() {
    $("#inputCountAbnormal").val("");
    $("#inputCountDead").val("");
    $("#inputCountHard").val("");
    $("#inputCountInfested").val("");
    $("#inputCountDormant").val("");
    $("#inputCountEmpty").val("");
    $("#inputCountUnknown").val("");
    $("#inputCountNormal").val("");
}

function DisablePageFunctionality(disable) {
    $("#inputCountAbnormal").prop("disabled", disable);
    $("#inputCountDead").prop("disabled", disable);
    $("#inputCountHard").prop("disabled", disable);
    $("#inputCountInfested").prop("disabled", disable);
    $("#inputCountDormant").prop("disabled", disable);
    $("#inputCountEmpty").prop("disabled", disable);
    $("#inputCountUnknown").prop("disabled", disable);
    $("#inputNote").prop("disabled", disable);
    $("#buttonSave").prop("disabled", disable);
    $("#buttonDelete").prop("disabled", disable);
    $("#buttonClear").prop("disabled", disable);
    $("#inputStartingDate").prop("disabled", disable);
    $("#inputCooperatorId").prop("disabled", disable);
}

function SearchInventory(event) {
    let fieldData = $("#" + fieldQR).val().toLowerCase();
    Clear();
    $("#" + fieldQR).val("");
    let formData = new FormData();
    formData.append("inventory_viability_data_id", fieldData);
    SendData(urlSearch, formData);
}

function SendData(url, formData) {
    $.ajax({
        type: "POST",
        url: url,
        cache: false,
        contentType: false,
        processData: false,
        data: formData,
        dataType: 'json',
        success: function (data) {
            ProccessData(data);
        },
        error: function (data) {
            ShowModalResult("Error, " + data.Message, ICON_FAIL);
            $("#qr_json").focus();
        }
    });
}

function StringToDate(valor) {
    var d = new Date(valor),
         month = '' + (d.getMonth() + 1),
         day = '' + d.getDate(),
         year = d.getFullYear();
    if (month.length < 2)
        month = '0' + month;
    if (day.length < 2)
        day = '0' + day;
    return [year, month, day].join('-');
}

function DateDiffDay(valueInit, valueFinish) {
    let dateI = new Date(valueInit);
    let dateF = new Date(valueFinish);
    let Difference_In_Time = dateF.getTime() - dateI.getTime();
    return Difference_In_Time / (1000 * 3600 * 24);
}

Date.prototype.toDateInputValue = function () {
    var local = new Date(this);
    local.setMinutes(this.getMinutes() - this.getTimezoneOffset());
    return local.toJSON().slice(0, 10);
};

function ProccessData(jsonResponse) {
    let json = $.parseJSON(jsonResponse);
    if (json.Success === 200) {
        let jsonDataInventory = json["Data"].viability_wizard_get_inventory[0];
        let jsonDataRule = json["Data"].viability_wizard_get_inventory_viability_rule[0];
        let jsonDataInventoryViability = json["Data"].viability_wizard_get_inventory_viability[0];
        let jsonDataInventoryViabilityData = json["Data"].viability_wizard_get_inventory_viability_data;
        let jsonDataAccession = json["Data"].get_accession[0];
        $("#inputReplicante").val(jsonDataInventoryViabilityData[0].replication_number);

        let titleReplication = `${jsonDataInventory.inventory_number_part1} ${jsonDataInventory.inventory_number_part2} ${jsonDataInventory.inventory_number_part3}  [ # ${jsonDataInventoryViabilityData[0].replication_number} ]`;
        $("#divReplicanteNumber").html(titleReplication);
        $("#inventory_viability_id").val(jsonDataInventoryViability.inventory_viability_id);
        $("#inventory_viability_id_readonly").val(jsonDataInventoryViability.inventory_viability_id);
        let result = CreateResult();
        result = ValidateNumberReplicate(result, jsonDataRule.number_of_replicates, $("#inputReplicante").val());
        result = GetDateToProceesData(result, jsonDataInventoryViabilityData);
        if (result.Success === 200 || result.Success === 301) {
            $("#divContendCampureData").show();
            let inventoryNumber = [jsonDataInventory.inventory_number_part1
                , jsonDataInventory.inventory_number_part2
                , jsonDataInventory.inventory_number_part3];
            inventoryNumber = inventoryNumber.filter(function (element) {
                return element !== null;
            });
            $("#inputInventoryNumber").val(inventoryNumber.join("-"));
            let accessionNumber = [jsonDataAccession.accession_number_part1
                , jsonDataAccession.accession_number_part2
                , jsonDataInventory.accession_number_part3];
            accessionNumber = accessionNumber.filter(function (element) {
                return element !== null;
            });
            $("#inputAccesion").val(accessionNumber.join(" "));
            $("#inventory_id").val(jsonDataInventory.inventory_id);
            $("#inputAccesionName").val(jsonDataInventory.plant_name);
            $("#inputTaxonomy").val(jsonDataInventory.taxonomy_species_name);
            $("#inputNameRule").val(jsonDataRule.name);
            $("#inputNumberReplicanteRule").val(jsonDataRule.number_of_replicates);
            $("#inputSeedPerReplicateRule").val(jsonDataRule.seeds_per_replicate);
            $("#inputCountDate").val(new Date().toDateInputValue());
            $("#inputStartingDate").val(StringToDate(jsonDataInventoryViability.tested_date));
            $("#inputCountNormal").val(jsonDataRule.seeds_per_replicate);
            $("#inputDay").val(DateDiffDay($("#inputStartingDate").val(), $("#inputCountDate").val()));
            DisablePageFunctionality(false);
            if (result.Success === 301) {
                ShowModalResult(result.Message, ICON_FAIL);
                CountNormal();
                DisablePageFunctionality(true);
                $("#modalButtonClose").click(function () { CloseModalScroll(); });
            } else if (result.Success === 200) {
                Scroll();
            }
        } else {
            $("#divContendCampureData").hide();
            ShowModalResult(result.Message, ICON_FAIL);
        }
    } else {
        $("#divContendCampureData").hide();
        ShowModalResult('QR error is not valid or not exist Id' + json.Message, ICON_FAIL);
    }

}

function Scroll() {
    $('html,body').animate({
        scrollTop: $("#divHPanelCountData").offset().top
    }, 2000);
    $("#inputCountAbnormal").focus();
}

function CreateResult() {
    return result = { Success: 200, Message: "" };
}

function ValidateNumberReplicate(result, totalReplicationNumber, replicationNumber) {
    if (replicationNumber > totalReplicationNumber) {
        result.Success = 500;
        result.Message = `The experiment cannot be created because the replication number (${replicationNumber})exceeds the total number of replicas (${totalReplicationNumber})`;
    }
    return result;
}

function GetDateToProceesData(result, jsonDataInventoryViabilityData) {
    let replicante = $("#inputReplicante").val();
    let fechas = jsonDataInventoryViabilityData.map(function (inventoryViabilityData) {
        let returnD = "";
        if (inventoryViabilityData.replication_number === replicante) {
            if (inventoryViabilityData.normal_count > 0 || inventoryViabilityData.note !== ""
                || inventoryViabilityData.abnormal_count !== "" || inventoryViabilityData.dead_count !== ""
                || inventoryViabilityData.dormant_count !== "" || inventoryViabilityData.empty_count !== ""
                || inventoryViabilityData.unknown_count !== "" || inventoryViabilityData.infested_count !== ""
                || inventoryViabilityData.hard_count !== "") {
                returnD = inventoryViabilityData.count_date;
                $("#inputCountAbnormal").val(inventoryViabilityData.abnormal_count);
                $("#inputCountDead").val(inventoryViabilityData.dead_count);
                $("#inputCountHard").val(inventoryViabilityData.hard_count);
                $("#inputCountInfested").val(inventoryViabilityData.infested_count);
                $("#inputCountDormant").val(inventoryViabilityData.dormant_count);
                $("#inputCountEmpty").val(inventoryViabilityData.empty_count);
                $("#inputCountUnknown").val(inventoryViabilityData.unknown_count);
                $("#inputCooperatorId").val(inventoryViabilityData.counter_cooperator_id);
                $("#inputNote").val(inventoryViabilityData.note);
            }
        }
        return returnD;
    });
    let fecha = "";
    fechas.forEach(function (element) {
        if (element !== "") {
            fecha = element;
        }
    });
    if (fecha !== "") {
        result.Success = 301;
        result.Message = `Inventory viability data for this barcode were processed in ${fecha}`;
    }
    return result;
}

function HiddenShowHPanel(divHPanel) {
    let hpanel = $("#" + divHPanel);
    let icon = hpanel.find('i:first');
    let body = hpanel.find('div.panel-body');
    let footer = hpanel.find('div.panel-footer');
    body.slideToggle(300);
    footer.slideToggle(200);
    // Toggle icon from up to down
    icon.toggleClass('fa-chevron-up').toggleClass('fa-chevron-down');
    hpanel.toggleClass('').toggleClass('panel-collapse');
    setTimeout(function () {
        hpanel.resize();
        hpanel.find('[id^=map-]').resize();
    }, 50);
}

function ShowHiddeGridHPanel(s) {
    event.preventDefault();
    let hpanel = $(this).closest('div.hpanel');
    hpanel = $("#" + s.currentTarget.id).parent().parent().parent();
    let icon = $(this).find('i:first');
    let body = hpanel.find('div.panel-body');
    let footer = hpanel.find('div.panel-footer');
    body.slideToggle(300);
    footer.slideToggle(200);
    LoadGridExtra(s.currentTarget.id);
    // Toggle icon from up to down
    icon.toggleClass('fa-chevron-down').toggleClass('fa-chevron-up');
    hpanel.toggleClass('panel-collapse').toggleClass('');
    setTimeout(function () {
        hpanel.resize();
        hpanel.find('[id^=map-]').resize();
    }, 50);
}

function LoadGridExtra(hPanelId) {
    let formData = "";
    let id = "";
    switch (hPanelId) {
        case "aHpanelInventoryViabilityData":
            formData = new FormData();
            id = $("#inventory_viability_id").val();
            if (id !== "") {
                formData.append("inventory_viability_id", id);
                if ($("#divInventoryViabilityData").html().trim() === "") {
                    SetGridInDiv(urlGridInventoryViabilityData, formData, "divInventoryViabilityData");
                }
            }
            break;
        case "aHpanelViabilityHistory":
            formData = new FormData();
            id = $("#inventory_id").val();
            if (id !== "") {
                formData.append("inventory_id", id);
                if ($("#divInventoryViability").html().trim() === "") {
                    SetGridInDiv(urlGridInventoryViability, formData, "divInventoryViability");
                }
            }
            break;
    }
}

function SetGridInDiv(url, formData, div) {
    $.ajax({
        type: "POST",
        url: url,
        cache: false,
        contentType: false,
        processData: false,
        data: formData,
        success: function (data) {
            $("#" + div).html(data);
        },
        error: function (data) {
            ShowModalResult("Error, " + data.Message, ICON_FAIL);
        }
    });
}

function CountNormal() {
    let abnormal = parseInt($("#inputCountAbnormal").val()) || 0;
    let dead = parseInt($("#inputCountDead").val()) || 0;
    let hard = parseInt($("#inputCountHard").val()) || 0;
    let infested = parseInt($("#inputCountInfested").val()) || 0;
    let dormant = parseInt($("#inputCountDormant").val()) || 0;
    let empty = parseInt($("#inputCountEmpty").val()) || 0;
    let unknown = parseInt($("#inputCountUnknown").val()) || 0;
    let normal = (parseInt($("#inputSeedPerReplicateRule").val()) || 0) - abnormal - dead - hard - infested - dormant - empty - unknown;
    $("#inputCountNormal").val(normal);
    let percentaje = $("#inputCountNormal").val() / parseInt($("#inputSeedPerReplicateRule").val()) * 100;
    $("#inputGermination").val(percentaje);
}

function CloseModalScroll() {
    Scroll();
    $("#modalButtonClose").click(function () { });
}

function ValidateForm() {
    let result = CreateResult();
    if ($("#inputCountNormal").val() < 0) {
        result.Success = 501;
        result.Message = "The sum of the elements cannot be greater than the number of the element in the experiment.";
    }
    let abnormal = parseInt($("#inputCountAbnormal").val()) || 0;
    let dead = parseInt($("#inputCountDead").val()) || 0;
    let hard = parseInt($("#inputCountHard").val()) || 0;
    let infested = parseInt($("#inputCountInfested").val()) || 0;
    let dormant = parseInt($("#inputCountDormant").val()) || 0;
    let empty = parseInt($("#inputCountEmpty").val()) || 0;
    let unknown = parseInt($("#inputCountUnknown").val()) || 0;
    if (abnormal < 0 || dead < 0 || hard < 0 || infested < 0 || dormant < 0 || empty < 0 || unknown < 0) {
        result.Success = 501;
        result.Message = "The count should not have negative values.";
    }
    return result;
}

function SaveData(url, formData) {
    $.ajax({
        type: "POST",
        url: url,
        cache: false,
        contentType: false,
        processData: false,
        data: formData,
        dataType: 'json',
        success: function (data) {
            let json = $.parseJSON(data);
            if (json.Success === 200) {
                ShowModalResult("Inventories viability data save", ICON_SUCCESS);
                Clear();
                $("#divContendCampureData").hide();
                $("#" + fieldQR).val("");
            } else {
                ShowModalResult("Error, " + json.Message, ICON_FAIL);
            }
        },
        error: function (data) {
            ShowModalResult("Error, " + json.Message, ICON_FAIL);
        }
    });
}

function DeleteCancel() {
    ConfirmModal(false, "", "");
}

function DeleteSave() {
    ConfirmModal(false, "", "");
    let formData = new FormData();
    formData.append("normal_count", 0);
    formData.append("inputGermination", $("#inputGermination").val());
    formData.append("tested_date", $("#inputStartingDate").val());
    formData.append("inventory_viability_id", $("#inventory_viability_id").val());
    formData.append("replication_number", $("#inputReplicante").val());
    formData.append("note", $("#inputNote").val());
    formData.append("count_number", $("#inputSeedPerReplicateRule").val());
    formData.append("counter_cooperator_id", $("#inputCooperatorId").val());
    SaveData(urlSave, formData);
}

function ValidateNote(result, note) {
    if (note === "") {
        result.Success = 500;
        result.Message = "To DELETE this experiment, you must write the reason why it is deleted.";
    }
    return result;
}

function ValidateCooperator() {
    let formData = new FormData();
    if ($("#inputCooperatorId").val() !== '') {
        formData.append("cooperator_id", $("#inputCooperatorId").val());
        let url = urlValidateCooperateId;
        $.ajax({
            type: "POST",
            url: url,
            cache: false,
            contentType: false,
            processData: false,
            data: formData,
            dataType: 'json',
            success: function (data) {
                let json = $.parseJSON(data);
                if (json.Success === 200) {
                    $("#cooperatorIdHelp").hide();
                    $("#buttonSave").prop("disabled", false);
                } else {
                    $("#cooperatorIdHelp").html("Error, the cooperator was not found");
                    $("#cooperatorIdHelp").show();
                    $("#buttonSave").prop("disabled", true);
                    //ShowModalResult("Error, the cooperator was not found", ICON_FAIL);
                }
            },
            error: function (data) {
                ShowModalResult("Error, " + json.Message, ICON_FAIL);
            }
        });
    }
}