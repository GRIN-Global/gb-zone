﻿//### views/vaibilityprint/index
var emptyViabilityJson = new Array();
var allItems = new Array();
$(document).ready(function () {
    InitJQuery();
});

function InitJQuery() {
    InitJQueryParent();
    $("#saveButton").on("click", function (event) { SaveInventoryViability(event); });
}

function SaveInventoryViability() {
    let parameters = textParameters.GetValue();
    let invenotryViabilityRuleId = comboBoxInventoryViabilityRule.GetValue();
    if (invenotryViabilityRuleId > 0) {
        let formData = new FormData();
        formData.append("parameters", parameters);
        formData.append("inventoryViabilityRuleId", invenotryViabilityRuleId);
        SendData(urlSave, formData);
    } else {
        ShowModalResult("Select a viability rule for the order.", ICON_FAIL);
    }
}

function SendData(url, formData) {
    $.ajax({
        type: "POST",
        url: url,
        cache: false,
        contentType: false,
        processData: false,
        data: formData,
        dataType: 'json',
        success: function (data) {
            if (data.Success === 200) {
                ShowModalResult("Inventories viability created!", ICON_SUCCESS);
                $("#saveButton").prop("disabled", true);
            } else {
                ShowModalResult(data.MenssageError, ICON_FAIL);
            }
        },
        error: function (data) {
            $("#saveButton").prop("disabled", false);
        }
    });
}

function InitGridView(s, e) {
    emptyViabilityJson = new Array();
    let jsonData = $.parseJSON(gvSearch.cpEmptyViabilityJson);
    jsonData.forEach(function (item) {
        emptyViabilityJson.push(item);
    });
    allItems = new Array();
    jsonData = $.parseJSON(gvSearch.cpAllItemJson);
    jsonData.forEach(function (item) {
        allItems.push(item["inventory_id"]);
    });
    ValidateEmptyViability();
    $("#spanOrderId").html(gvSearch.cpOrderId);
}

function ValidateEmptyViability() {
    if (emptyViabilityJson.length > 0) {
        $("#saveButton").prop("disabled", true);
        for (let i = 0; i < allItems.length; i++) {
            let row_now = gvSearch.GetRow(i);
            for (let j = 0; j <= emptyViabilityJson.length; j++) {
                if ($(row_now).text().indexOf(emptyViabilityJson[j]) > 0) {
                    let id = $(row_now).attr("id");
                    $("#" + id).css({ 'background-color': COLOR_ERROR_LIGHT });
                }
            }
        }
        ShowModalResult("It is not possible to process the viability order until the open inventories viability are closed.", ICON_FAIL);
    }
    if (allItems.length === 0) {
        $("#saveButton").prop("disabled", false);
        ShowModalResult("The order is not found or is empty.", ICON_FAIL);
    }
}

