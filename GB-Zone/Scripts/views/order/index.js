﻿//# view/order/index
//script to functionality index page view order, check item, hidden element and save data
var sysColumNameCheckList = "";
var sysColumNameCheckToSaveAction = "";
var sysCheckBefore = "";
const LI_TRAFIC_LIGHT = "iTrafficLight";
const INPUT_TEXT_QR = "textBoxCheck_I";
const SPAN_LAST_CHECK_ITEM = "spanCheck_Item";
const SPAN_FIND_ITEM = "spanFind_Item";
const DIV_LOAD_MODAL = "divLoadMe";

const SPAN_COMPLETE = "spanComplete";
const SPAN_SAVE = "spanSave";
var itemsWithCheck = new Array();
var itemsWithOutCheck = new Array();
var ALL_ITEMS = new Array();
var rowNumAllToStopModal = 0;
var rowNumGetToSotpoModal = 0;
var itemPreviewCheck = new Array();

$(document).ready(function () {
    InitJQuery();
});

function InitJQuery() {
    hiddenColumn();
    missingItems(0);
}

function hiddenColumn() {
    let columnes = '.id_' + sysColumNameCheckToSaveAction;
    $(columnes).css('display', 'none');
    setTimeout(function () {
        $(columnes).css('display', 'none');
    }, 2000);
    setTimeout(function () {
        $(columnes).css('display', 'none');
    }, 6000);
    setTimeout(function () {
        $(columnes).css('display', 'none');
    }, 10000);
}

function BatchEditChangesSaving() {
    setTimeout(function () {
        Init();
    }, 1000);
    hiddenColumn();//Init();
    $("#" + LI_TRAFIC_LIGHT).css({ 'color': COLOR_NEUTRO });
    $("#" + SPAN_LAST_CHECK_ITEM).html("");
    $("#" + INPUT_TEXT_QR).focus();
    $("#" + SPAN_SAVE).html("<b> Save in Database </b>");
}

function KeyPressTexBCheck(s, e) {
    if (e.htmlEvent.keyCode === KEY_ENTER) {
        let text = $("#" + INPUT_TEXT_QR).val().trim();
        CheckElement(text);
        $("#" + SPAN_LAST_CHECK_ITEM).html("<b>" + $("#" + INPUT_TEXT_QR).val() + "</b>");
        $("#" + INPUT_TEXT_QR).val('');
    }
}

function CheckElement(text) {
    $("#" + SPAN_SAVE).html("");
    let indice = ALL_ITEMS.indexOf(text);
    if (indice !== -1) {
        $("#" + LI_TRAFIC_LIGHT).css({ 'color': COLOR_SUCCESS });
        indice = itemsWithOutCheck.indexOf(text);
        if (indice !== -1) {
            itemsWithCheck.push(itemsWithOutCheck[indice]);
            itemsWithOutCheck.splice(indice, 1);
            missingItems(itemsWithCheck.length);
            $("#audioSuccess")[0].play();
            Color();
        }
    }
    else {
        $("#" + LI_TRAFIC_LIGHT).css({ 'color': COLOR_ERROR });
        $("#audioError")[0].play();
    }
    hiddenColumn();
}

function Color() {
    let itemTemp = itemsWithCheck;
    let indexDelete = -1;
    let indexCellColor = 0;
    for (let i = 0; i < ALL_ITEMS.length; i++) {
        let row_now = gvSearch.GetRow(i);
        let id = $(row_now).attr("id");
        $("#" + id).css({ 'background-color': COLOR_NEUTRO });
    }
    for (let i = 0; i < ALL_ITEMS.length; i++) {
        let row_now = gvSearch.GetRow(i);
        indexDelete = -1;
        for (let j = 0; j <= itemTemp.length; j++) {
            if ($(row_now).text().indexOf(itemTemp[j]) !== -1) {
                indexDelete = j;
                indexCellColor++;
                let id = $(row_now).attr("id");
                $("#" + id).css({ 'background-color': COLOR_SUCCESS });
                let key = parseInt(id.replace("gvSearch_DXDataRow", ""), 10);
                gvSearch.batchEditApi.SetCellValue(key, sysColumNameCheckToSaveAction, true);
            }
        }
    }
    hiddenColumn();
}

function ColumnSorting() {
    setTimeout(function () {
        Color();
    }, 1000);
}

function Init() {
    itemsWithOutCheck = new Array();
    itemPreviewCheck = new Array();
    itemsWithCheck = new Array();
    let jsonData = $.parseJSON(gvSearch.cpModelData);
    sysColumNameCheckList = gvSearch.cpSystemCheckListColumName;
    sysColumNameCheckToSaveAction = gvSearch.cpSystemCheckToSaveAction;
    sysCheckBefore = gvSearch.cpSystemCheckBefore;
    ALL_ITEMS = new Array();
    rowNumAllToStopModal = 0;
    rowNumGetToSotpoModal = 0;
    ModalLoad();
    rowNumAllToStopModal = Object.keys(jsonData).length;
    jsonData.forEach(function (item) {
        ALL_ITEMS.push(item[sysColumNameCheckList]);
        itemsWithOutCheck.push(item[sysColumNameCheckList]);
        if (item[sysCheckBefore] === true) {
            itemPreviewCheck.push(item[sysColumNameCheckList]);
        }
        rowNumGetToSotpoModal++;
    });
    missingItems(0);
    ModalClose();
    $("#labelChceck").text("Scan QR: [" + gvSearch.cpCaptionSearch + "]");
    hiddenColumn();
    $("#" + INPUT_TEXT_QR).focus();
}

function ModalLoad() {
    $("#" + DIV_LOAD_MODAL).modal({
        backdrop: "static",
        keyboard: false,
        show: true
    });
}

function ModalClose() {
    if (rowNumGetToSotpoModal < rowNumAllToStopModal) {
        setTimeout(function () {
            ModalClose();
        }, 1000);
    } else {
        $("#" + DIV_LOAD_MODAL).modal("hide");
    }
}

function missingItems(indexCellColor) {
    let setA = new Set(itemsWithCheck);
    let setB = new Set(itemPreviewCheck);
    let setC = new Set([...setA].filter(x => setB.has(x)));
    if (ALL_ITEMS.length !== 0) {
        let total = indexCellColor + setB.size - setC.size;
        if (total >= ALL_ITEMS.length) {
            total = ALL_ITEMS.length;
            $("#" + SPAN_COMPLETE).html("<b> Order completed </b>");
        }
        else {
            $("#" + SPAN_COMPLETE).html("");
        }
        $("#" + SPAN_FIND_ITEM).html(total + "/" + ALL_ITEMS.length);
    }
}

function BatchEditChangesCanceling() {
    itemsWithCheck = new Array();
    itemsWithOutCheck = ALL_ITEMS.slice();
    Color();
    $("#" + LI_TRAFIC_LIGHT).css({ 'color': COLOR_NEUTRO });
    $("#" + SPAN_LAST_CHECK_ITEM).html("");
    missingItems(0);
}
