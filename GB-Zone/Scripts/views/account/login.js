﻿$(document).ready(function () {
    InitJQuery();
});


function InitJQuery() {
    $("form").on('submit', function (e) {
        $('#buttonLogin').attr('disabled', 'disabled');
        setTimeout(function () {
            $('#buttonLogin').removeAttr('disabled');
        }, 5000);
    });
}