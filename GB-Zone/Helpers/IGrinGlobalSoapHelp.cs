﻿using System.Collections.Generic;
using System.Data;
using System.Web.Mvc;
using GrinGlobal.Zone.Helpers.setting;

namespace GrinGlobal.Zone.Helpers
{
    public interface IGrinGlobalSoapHelp
    {
        SettingsHelp SetH { get; }

        DataTable GetCategories(string groupName);
        DataSet GetData(string param, string dataviewSpecific = "");
        DataTable GetDataActionOne(string originalParameter, Dictionary<string, string> newParameter, string idAction);
        Dictionary<string, string> GetOnlyParameters(string dataviewName);
        Dictionary<string, string> GetParameters(FormCollection dataform, string dataviewSpecific = "", Dictionary<string, string> defaultParameter = null);
        string Parameters_DictionaryToString(Dictionary<string, string> dic);
        Dictionary<string, string> Parameters_StringToDictionary(string parameters);
        DataSet SaveData(string parameters, DataTable newDataTable, string dataviewnameSpecific = null);
        DataSet SaveDataAction(string originalParameter, Dictionary<string, string> newParameter, string idAction, DataTable newDataTable);
    }
}