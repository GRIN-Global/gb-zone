﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Xml.Linq;

namespace GrinGlobal.Zone.Helpers.setting
{
    public class HtmlHelp
    {
        #region public const attributes
        public readonly string SYS_VIEWDATA_MODULEID = "moduleId";
        public readonly string SYS_VIEWDATA_FORMID = "formId";
        public readonly string SYS_VIEWDATA_FORMURL = "formUrl";
        public readonly string SYS_VIEWDATA_FORMCONTROLLER = "formController";
        public readonly string SYS_VIEWDATA_FORMIDHTML = "formIdHtml";
        public readonly string SYS_VIEWDATA_TITLEMENU = "titleMenu";
        public readonly string SYS_VIEWDATA_MESSAGEERROR = "messageError";
        public readonly string SYS_VIEWDATA_PARAMETERS = "parameters";
        public readonly string SYS_VIEWDATA_DATATABLE = "dataTable";
        public readonly string SYS_VIEWDATA_CONTROLLERACTION = "controllerAction";
        public readonly string SYS_VIEWDATA_CONTROLLER = "controller";
        public readonly string SYS_VIEWDATA_SUCCESS = "success";
        public readonly string SYS_VIEWDATA_GRIDRESULT = "gridResult";
        public readonly string SYS_VIEWDATA_EDIT_ERROR = "EditError";
        public readonly string SYS_TABLE_HEAD = "head";
        public readonly string SYS_TABLE_TITLE = "title";


        public readonly string SYS_RAZO_FIELDSEARCH = "~/Views/Shared/_GenericSearchFields.cshtml";
        public readonly string SYS_RAZO_HEADERGRIDVIEW = "_HeaderGridView";
        public readonly string SYS_RAZO_GRIDVIEWERROR = "_GridViewError";
        public readonly string SYS_RAZO_GENERICINDEX = "~/Views/Shared/_GenericIndex.cshtml";
        public readonly string SYS_RAZO_GENERICGRIDVIEWSEARCH = "_GridViewSearch";
        public readonly string SYS_RAZO_COMPONENTGRIDVIEW = "_ComponentGridView";

        public readonly string SYS_SESSION_SERVER = "server";

        public readonly string SYS_JSON_ERRORDATA = "jsonErrorData";
        public readonly string SYS_JSON_UPDATEDATA = "jsonUpdateData";
        public readonly string SYS_JSON_INSERTDATA = "jsonInsertData";
        #region boxController
        public readonly string SYS_BOX_COLUMNNAME_ID = "sysColumnID";
        public readonly string SYS_BOX_COLUMNNAME_INVENTORY_GETDATA = "sysColumnInventoryGetData";
        public readonly string SYS_BOX_COLUMNNAME_STORAGELOCATION1 = "sysColumnStorageLocaltion1";
        public readonly string SYS_BOX_COLUMNNAME_STORAGELOCATION2 = "sysColumnStorageLocaltion2";
        public readonly string SYS_BOX_COLUMNNAME_STORAGELOCATION3 = "sysColumnStorageLocaltion3";
        public readonly string SYS_BOX_COLUMNNAME_STORAGELOCATION4 = "sysColumnStorageLocaltion4";
        public readonly string SYS_BOX_ATTRIBUTE_ALLOCATION_SPLIT = "sysAllocationSplit";
        public readonly string SYS_BOX_ATTRIBUTE_SAVE = "sysSave";
        public readonly string SYS_BOX_ATTRIBUTE_SPLIT = "sysSplit";
        public readonly string SYS_BOX_ERROR = "Error";
        public readonly string SYS_BOX_DATAVIEW_STORAGELOCATION = "storageLocation";
        public readonly string SYS_BOX_JSON_ITEMINBOX = "jsonItemInBox";
        #endregion
        #region OrderController
        public readonly string SYS_ORDER_COLUMNNAME_CHECK_LIST = "systemCheckListColumName";
        public readonly string SYS_ORDER_COLUMNNAME_CHECK_TO_SAVE_DATAVIEW = "systemCheckToSaveAction";
        public readonly string SYS_ORDER_DATAVIEWACTIONNAME_ORDER_REQUEST_ITEM_ACTION = "systemOrderRequestItemAction";
        public readonly string SYS_ORDER_COLUMNNAME_CHECK_BEFORE = "systemCheckBefore";
        public readonly string SYS_ORDER_DATAVIEWACTIONVALUE_CHECK_HISTORY_ACTION = "systemCheckHistoryAction";
        public readonly string SYS_ORDER_JSON_CHECK_SECCION_ITEM = "jsonChekLastSeccionItems";
        #endregion
        #region viabilityController
        public readonly string SYS_VIABILITY_FIELDNAME_INCREMENTYEAR = "sysIncrementYear";
        public readonly string SYS_VIABILITY_FIELDNAME_IMPROVEMENTSTATUS = "sysImprovementStatusCode";
        public readonly string SYS_VIABILITY_FIELDNAME_REPRODUCTIVEUNIFORM = "sysReproductiveUniformityCode";
        public readonly string SYS_VIABILITY_FIELDNAME_ACCESIONNUMBER = "sysAccesionNumber";
        public readonly string SYS_VIABILITY_FIELDNAME_INVENTORYID = "sysInventoryId";
        public readonly string SYS_VIABILITY_FIELDNAME_INVENTORYNUMBERPART3 = "sysInvenotryNumber3";
        public readonly string SYS_VIABILITY_FIELDNAME_TOTAL = "sysTotal";
        public readonly string SYS_VIABILITY_FIELDNAME_PRELOAD = "sysPreload";
        #endregion
        #region ViabilityPrintController
        public readonly string SYS_VIABILITYPRINT_FIELDNAME_INVNETORYVIABILITYRULEID = "inventoryViabilityRuleId";
        public readonly string SYS_VIABILITYPRINT_FIELDNAME_INVNETORYVIABILITYRULECOMBO = "inventoryViabilityRuleCombo";
        public readonly string SYS_VIABILITYPRINT_FIELDNAME_INVNETORYVIABILITYDATA = "inventoryViabilityData";
        public readonly string SYS_VIABILITYPRINT_FIELDNAME_INVNETORYVIABILITY = "inventoryViability";
        public readonly string SYS_VIABILITYPRINT_FIELDNAME_NUMBERORDER = "numberOrder";
        public readonly string SYS_VIABILITYPRINT_COMBOBOX_INVNETORYVIABILITYRULE = "comboBoxInventoryViabilityRule";
        public readonly string SYS_VIABILITYPRINT_DATAVIEW_HEADER = "dataTableHeaders";
        public readonly string SYS_VIABILITYPRINT_JSON_EMPTYVIABILITYJSON = "emptyViabilityJson";
        public readonly string SYS_VIABILITYPRINT_JSON_ALLITEMJSON = "allItemJson";
        public readonly string SYS_VIABILITYPRINT_JSON_ORDERID = "orderId";

        public readonly string SYS_VIABILITY_COLUMNNAME_INVENTORYVIABILITYID = "inventory_viability_id";
        public readonly string SYS_VIABILITY_COLUMNNAME_INVENTORYID = "inventory_id";
        public readonly string SYS_VIABILITYPRINT_COLUMNNAME_ORDERREQUESTITEMID = "order_request_item_id";
        public readonly string SYS_VIABILITYPRINT_COLUMNNAME_NUMBEROFREPLICANTE = "number_of_replicates";
        public readonly string SYS_VIABILITYPRINT_COLUMNNAME_SEEDSPERREPLICATE = "seeds_per_replicate";
        public readonly string SYS_VIABILITYPRINT_COLUMNNAME_PERCENTVIABLE = "percent_viable";
        public readonly string SYS_VIABILITYPRINT_COLUMNNAME_INVENTORYVIABILITYDATAID = "inventory_viability_data_id";
        public readonly string SYS_VIABILITYPRINT_COLUMNNAME_REPLICATIONNUMBER = "replication_number";
        public readonly string SYS_VIABILITYPRINT_COLUMNNAME_COUNTNUMBER = "count_number";
        public readonly string SYS_VIABILITYPRINT_COLUMNNAME_COUNTDATE = "count_date";
        public readonly string SYS_VIABILITYPRINT_COLUMNNAME_NORMALCOUNT = "normal_count";
        public readonly string SYS_VIABILITYPRINT_COLUMNNAME_TESTEDDATE = "tested_date";
        #endregion
        #region VaibilityDataCollection
        public readonly string SYS_VIABILITYDATACOLLECTION_FIELDNAME_INVENTORYVIABILITYDATA = "sysInventoryViabilityData";
        public readonly string SYS_VIABILITYDATACOLLECTION_FIELDNAME_INVENTORYVIABILITY = "sysInventoryViability";
        public readonly string SYS_VIABILITYDATACOLLECTION_FIELDNAME_INVENTORY = "sysInventory";
        public readonly string SYS_VIABILITYDATACOLLECTION_FIELDNAME_INVENTORYVIABILITYRULE = "sysInventoryViabilityRule";
        public readonly string SYS_VIABILITYDATACOLLECTION_FIELDNAME_SCANDQR = "sysQrJson";
        public readonly string SYS_VIABILITYDATACOLLECTION_FIELDNAME_ACCESSION = "sysAccession";
        public readonly string SYS_VIABILITYDATACOLLECTION_COLUMNNAME_INVENTORYID = "inventory_id";

        public readonly string SYS_VIABILITY_COLUMNNAME_INVENTORYVIABILITYRULEID = "inventory_viability_rule_id";

        #endregion

        #region Dataview action
        public readonly string SYS_DATAVIEWACTIONNAME_GET_INVENTORYACTION = "sysInventoryAction";
        public readonly string SYS_DATAVIEWACTIONNAME_FINISHVIABILITY_INVENTORYACTION = "sysInventoryActionFinish";
        #endregion
        #endregion
        private CoreSettingHelp help;
        private string pathTest;

        /// <summary>
        /// Initial Builder
        /// </summary>
        /// <param name="pathTest">Path to setting.xml file in case of unity test </param>
        public HtmlHelp(string pathTest = "")
        {
            this.pathTest = pathTest;
        }
        #region public methods
        /// <summary>
        /// Get the DataTavble with configuration of combobox
        /// </summary>
        /// <returns> DataTable with column: value, display </returns>
        public DataTable GetServerCombo()
        {
            help = new CoreSettingHelp();
            return help.LoadServerCombo();
        }
        #endregion

    }
}