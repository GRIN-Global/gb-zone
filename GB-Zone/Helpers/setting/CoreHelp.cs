﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Xml.Linq;

namespace GrinGlobal.Zone.Helpers.setting
{
    /// <summary>
    /// Class to get all the values ​​embedded in the settings file.
    /// </summary>
    public class CoreSettingHelp
    {
        #region public Attribute
        /// <summary>
        /// Get all inputs that corresponds to the formId provided.
        /// </summary>
        /// <returns>
        /// The IEnumerable XElements that is obtained from the Xpath parameters provided to constructor
        /// </returns>
        public IEnumerable<XElement> Fields { get { return fields; } }
        /// <summary>
        /// Get all inputs that corresponds to the serverId provided.
        /// </summary>
        /// <returns>
        /// The XElement that is obtained from the Xpath parameters provided to constructor
        /// </returns>
        public XElement Server { get { return server; } }
        /// <summary>
        /// Get all inputs that corresponds to the moduleId provided.
        /// </summary>
        /// <returns>
        /// The XElement that is obtained from the Xpath parameters provided to constructor
        /// </returns>
        public XElement Module { get { return module; } }
        /// <summary>
        /// Get all inputs that corresponds to the formId provided.
        /// </summary>
        /// <returns>
        /// The XElement that is obtained from the Xpath parameters provided to constructor
        /// </returns>
        public XElement Form { get { return form; } }
        /// <summary>
        /// Get the tag named Parameters, that corresponds to the action.
        /// </summary>
        /// <returns>
        /// The XElement that is obtained from the Xpath parameters provided to constructor
        /// </returns>
        public XElement Parameter { get { return parameter; } }
        /// <summary>
        /// Get the tag named Columns, that corresponds to the action.
        /// </summary>
        /// <returns>
        /// The XElement that is obtained from the Xpath parameters provided to constructor
        /// </returns>
        public XElement Column { get { return column; } }
        /// <summary>
        /// Get the tag named extendedProperties, that corresponds to the action.
        /// </summary>
        /// <returns>
        /// The XElement that is obtained from the Xpath parameters provided to constructor
        /// </returns>
        public XElement ExtendedPropertie { get { return extendedPropertie; } }
        /// <summary>
        /// Get the dataview name from the parameters
        /// </summary>
        /// <returns>
        /// The dataview name
        /// </returns>
        public string DataViewName { get { return dataViewName; } }
        /// <summary>
        /// Get the value from the DataViewAction
        /// </summary>
        /// <returns>
        /// Get IEnumerable<XElement> from Dataview Action defined in the settings
        /// </returns>
        public IEnumerable<XElement> DataViewAction { get { return dataViewAction; } }
        /// <summary>
        /// Xml global with data by catalogue
        /// </summary>
        public XElement GlobalCatalogue { get { return globalCatalogue; } }
        /// <summary>
        /// Xml global with information by paramter
        /// </summary>
        public XElement GlobalParameter { get { return globalParameter; } }
        /// <summary>
        /// Max charater in json 
        /// </summary>
        public int GlobalMaxJsonLength { get { return globalMaxJsonLength; } }
        #region readonly secction 
        /// <summary>
        /// Const is_visible
        /// </summary>
        public readonly string GRINGLOBAL_PROPERTIES_NAME_ISVISIBLE = "is_visible";
        /// <summary>
        /// Const actionDataview
        /// </summary>
        public readonly string SETTING_NAME_ACTIONDATAVIEW = "actionDataview";
        public readonly string SETTING_NAME_ACTIONS = "actions";
        public readonly string SETTING_NAME_ACTIONVALUE = "actionValue";
        public readonly string SETTING_NAME_ASSIGNMENT = "assignment";
        public readonly string SETTING_NAME_CATALOGUES = "catalogues";
        public readonly string SETTING_NAME_COLUMN = "column";
        public readonly string SETTING_NAME_COLUMNS = "columns";
        public readonly string SETTING_NAME_DATAVIEW = "dataviewName";
        public readonly string SETTING_NAME_DEFAULT = "default";
        public readonly string SETTING_NAME_DEFAULTS = "defaults";
        public readonly string SETTING_NAME_EXTENDEDPROPERTIES = "extendedProperties";
        public readonly string SETTING_NAME_FIELD = "field";
        public readonly string SETTING_NAME_FORM = "form";
        public readonly string SETTING_NAME_GENERIC_ID = "id";
        public readonly string SETTING_NAME_GLOBAL = "global";
        public readonly string SETTING_NAME_GROUPNAME = "groupName";
        public readonly string SETTING_NAME_INITIAL = "initial";
        public readonly string SETTING_NAME_JAVASCRIPT = "javascript";
        public readonly string SETTING_NAME_LIMIT = "limit";
        public readonly string SETTING_NAME_MAXJSONLENGTH = "maxJsonLength";
        public readonly string SETTING_NAME_MODULE = "module";
        public readonly string SETTING_NAME_NAME = "name";
        public readonly string SETTING_NAME_OFFSET = "offset";
        public readonly string SETTING_NAME_OPTIONS = "options";
        public readonly string SETTING_NAME_PARAMETERS = "parameters";
        public readonly string SETTING_NAME_SEPARATOR = "separator";
        public readonly string SETTING_NAME_SERVER = "server";
        public readonly string SETTING_NAME_SUPPRESSEXCEPTIONS = "suppressExceptions";
        public readonly string SETTING_NAME_TYPE = "type";
        public readonly string SETTING_NAME_URL = "url";
        public readonly string SETTING_NAME_VALUE = "value";
        public readonly string SYSTEM_ACTION_DATAVIEW = "systemActionDataview";
        public readonly string SYSTEM_ACTION_DATAVIEW_VALUE = "systemActionDataviewValue";
        public readonly string SYSTEM_COLUMN = "systemColumn";
        public readonly string SYSTEM_FIELD_NAME = "systemFieldName";

        #endregion
        #endregion
        #region private Attribute
        private string fileXml;
        private XElement xmlElement;
        private IEnumerable<XElement> fields;
        private XElement server;
        private XElement module;
        private XElement form;
        private XElement parameter;
        private XElement column;
        private XElement extendedPropertie;
        private IEnumerable<XElement> dataViewAction;
        private string dataViewName;
        private Dictionary<string, string> columnVariables;
        private XElement globalCatalogue;
        private XElement globalParameter;
        private int globalMaxJsonLength;
        #endregion
        #region Constructor
        /// <summary>
        /// Initial constructor
        /// </summary>
        /// <param name="path">Path to find the setting.xml file</param>
        public CoreSettingHelp(string path = "")
        {
            if (path == "")
            {
                fileXml = System.IO.Path.Combine(HttpContext.Current.Server.MapPath("~"), "Setting.xml");
            }
            else
            {
                fileXml = path;
            }
            xmlElement = XElement.Load(fileXml);
        }
        #endregion
        #region private Methods 
        private void GetXElement(string serverId, string moduleId, string formId)
        {
            server = xmlElement.Elements(SETTING_NAME_SERVER)
                                          .Where(w => (string)w.Attribute(SETTING_NAME_GENERIC_ID) == serverId)
                                          .SingleOrDefault();
            XElement global = server.Elements(SETTING_NAME_GLOBAL).SingleOrDefault();
            XElement javaS = global.Elements(SETTING_NAME_JAVASCRIPT).SingleOrDefault();
            module = (from el in server.Elements(SETTING_NAME_MODULE)
                      where (string)el.Attribute(SETTING_NAME_GENERIC_ID) == moduleId
                      select el).SingleOrDefault();
            form = (from el in module.Elements(SETTING_NAME_FORM)
                    where (string)el.Attribute(SETTING_NAME_GENERIC_ID) == formId
                    select el).SingleOrDefault();
            fields = from fi in form.Descendants(SETTING_NAME_FIELD) select fi;
            XElement action = form.Elements(SETTING_NAME_ACTIONS).SingleOrDefault();
            if (action != null)
            {
                parameter = action.Elements(SETTING_NAME_PARAMETERS).SingleOrDefault();
                column = action.Elements(SETTING_NAME_COLUMNS).SingleOrDefault();
                extendedPropertie = action.Elements(SETTING_NAME_EXTENDEDPROPERTIES).SingleOrDefault();
            }
            if (parameter != null)
            {
                dataViewName = parameter.Element(SETTING_NAME_DATAVIEW).Value;
            }
            if (extendedPropertie != null)
            {
                dataViewAction = from el in extendedPropertie.Descendants(SETTING_NAME_ACTIONDATAVIEW) select el;
            }
            globalCatalogue = global.Elements(SETTING_NAME_CATALOGUES).FirstOrDefault();
            globalParameter = global.Elements(SETTING_NAME_PARAMETERS).FirstOrDefault();
            string maxjson = javaS.Elements(SETTING_NAME_MAXJSONLENGTH).FirstOrDefault().Value.ToString();
            globalMaxJsonLength = string.IsNullOrEmpty(maxjson) ? int.MaxValue : int.Parse(maxjson);
        }

        private void LoadColumnVariable()
        {
            columnVariables = new Dictionary<string, string>();
            foreach (XElement col in GetColumn_Attribute(SYSTEM_COLUMN))
            {
                columnVariables.Add(col.Attribute(SYSTEM_COLUMN).Value.ToString(), col.Value.ToString().Trim());
            }
        }

        protected void Init(string serverId, string moduleId, string formId)
        {
            GetXElement(serverId, moduleId, formId);
            LoadColumnVariable();
        }
        #region dataview action
        private DataRow _GetElement(DataRow dr, string key, string value, string type, Dictionary<string, object> data)
        {
            dr[key] = _GetElementCell(value, type, data);
            /*
            switch (type)
            {
                case "dateTimeNow":
                    dr[key] = DateTime.Now;
                    break;
                case "const":
                    dr[key] = value;
                    break;
                case "var":
                    if(data != null && data.Count > 0)
                    {
                        if (data.ContainsKey(value))
                        {
                            dr[key] = data[value];
                        }
                    }
                    break;
            }
            */
            return dr;
        }

        private object _GetElementCell(string value, string type, Dictionary<string, object> data)
        {
            object returnData = null;
            switch (type)
            {
                case "dateTimeNow":
                    returnData = DateTime.Now;
                    break;
                case "const":
                    returnData = value;
                    break;
                case "var":
                    if (data != null && data.Count > 0)
                    {
                        if (data.ContainsKey(value))
                        {
                            returnData = data[value];
                        }
                    }
                    break;
            }
            return returnData;
        }
        #endregion


        #endregion
        #region public methods
        /// <summary>
        /// Find the specific column from the action, columns
        /// </summary>
        /// <param name="name">Name of column to find is automatic cast to uppercase</param>
        /// <returns>
        /// Get the column XElement or null in case not found
        /// </returns>
        public XElement GetColumn_Name(string name)
        {
            XElement col = (from e in column.Descendants(SETTING_NAME_COLUMN) where e.Value.Trim().ToUpper() == name.ToUpper() select e).DefaultIfEmpty(null).FirstOrDefault();
            return col;
        }
        /// <summary>
        /// Sear by espesific field in the setting.xml
        /// </summary>
        /// <param name="value">Value to searh</param>
        /// <param name="atribute">The attribute name to search by default uses the value of STTING_NAME_FIELD</param>
        /// <returns>Xml find by the value and atribute defined in case the not found return null</returns>
        public XElement GetField_AtributeValue(string value, string atribute = "")
        {
            if (string.IsNullOrEmpty(atribute))
            {
                atribute = SYSTEM_FIELD_NAME;
            }
            XElement col = (from e in form.Descendants(SETTING_NAME_FIELD) where e.Attribute(atribute) != null && e.Attribute(atribute).Value.Trim().ToUpper() == value.ToUpper() select e).DefaultIfEmpty(null).FirstOrDefault();
            return col;
        }

        /// <summary>
        /// Find all column that and spesific atribute
        /// </summary>
        /// <param name="attribute">Name to Attribute for search</param>
        /// <returns>
        /// Get the list XElement or list empty in case not found
        /// </returns>
        public List<XElement> GetColumn_Attribute(string attribute)
        {
            List<XElement> cols = new List<XElement>();
            if (column != null)
            {
                cols = (from e in column.Descendants(SETTING_NAME_COLUMN) where e.Attribute(attribute) != null select e).ToList();
            }
            return cols;
        }
        /// <summary>
        /// Complete the DataRow with parameter from setting
        /// </summary>
        /// <param name="dr">DataRow to fill with setting</param>
        /// <param name="idDataViewAction">id the node from DataViewAction</param>
        /// <returns>
        /// Return the DataRow with values
        /// </returns>
        public DataRow GetRowAction(DataRow dr, string idDataViewAction, Dictionary<string, object> dataAction)
        {
            foreach (XElement act in dataViewAction)
            {
                string id = (act.Attribute(SETTING_NAME_GENERIC_ID) != null) ? act.Attribute(SETTING_NAME_GENERIC_ID).Value.ToString().Trim() : "";
                if (idDataViewAction == id)
                {
                    IEnumerable<XElement> values = from el in act.Descendants(SETTING_NAME_ACTIONVALUE) select el;
                    foreach (XElement val in values)
                    {
                        string key = val.Attribute(SETTING_NAME_NAME).Value.ToString().Trim();
                        string value = val.Attribute(SETTING_NAME_VALUE).Value.ToString().Trim();
                        string type = val.Attribute(SETTING_NAME_TYPE).Value.ToString().Trim();
                        dr = _GetElement(dr, key, value, type, dataAction);
                    }
                }
            }
            return dr;
        }

        public object GetCellAction(string columnName, string idDataViewAction, Dictionary<string, object> dataAction)
        {
            object celdaValue = null;
            foreach (XElement act in dataViewAction)
            {
                string id = (act.Attribute(SETTING_NAME_GENERIC_ID) != null) ? act.Attribute(SETTING_NAME_GENERIC_ID).Value.ToString().Trim() : "";
                if (idDataViewAction == id)
                {
                    IEnumerable<XElement> values = from el in act.Descendants(SETTING_NAME_ACTIONVALUE) where el.Attribute(SETTING_NAME_NAME).Value == columnName select el;
                    foreach (XElement val in values)
                    {
                        //string key = val.Attribute(SETTING_NAME_NAME).Value.ToString().Trim();
                        string value = val.Attribute(SETTING_NAME_VALUE).Value.ToString().Trim();
                        string type = val.Attribute(SETTING_NAME_TYPE).Value.ToString().Trim();
                        celdaValue = _GetElementCell(value, type, dataAction);
                    }
                }
            }
            return celdaValue;
        }
        /// <summary>
        /// Find the value form variable in javascript in the setting
        /// </summary>
        /// <param name="variableName">Value to find in the array the javaScriptVar</param>
        /// <returns>
        /// the value of name of variable or "" in case not found
        /// </returns>
        public string GetColumnVariable(string variableName)
        {
            return (columnVariables.ContainsKey(variableName)) ? columnVariables[variableName] : "";
        }
        /// <summary>
        /// Get the value of attribute that relacionate to the search attribute 
        /// </summary>
        /// <param name="attributeSearch">The attribute name to match with attributeSearchValue</param>
        /// <param name="attributeSearchValue">Value to match in the settings</param>
        /// <returns>
        /// Return the node in the attribute to find, if that the attribute search not found return null element
        /// </returns>
        public XElement GetNodeAction(string attributeSearch, string attributeSearchValue)
        {
            XElement node = null;
            foreach (XElement act in dataViewAction)
            {
                string value = (act.Attribute(attributeSearch) != null) ? act.Attribute(attributeSearch).Value.ToString().Trim() : "";
                if (attributeSearchValue == value)
                {
                    node = act;
                }
            }
            return node;
        }
        /// <summary>
        /// Get the node from the xml setting the name actionDataviews 
        /// </summary>
        /// <param name="idAction"> attribute id to find the node</param>
        /// <returns>XElement with node</returns>
        public XElement GetNodeAction(string idAction)
        {
            XElement node = null;
            foreach (XElement act in dataViewAction)
            {
                string value = (act.Attribute(SETTING_NAME_GENERIC_ID) != null) ? act.Attribute(SETTING_NAME_GENERIC_ID).Value.ToString().Trim() : "";
                if (idAction == value)
                {
                    node = act;
                }
            }
            return node;
        }
        /// <summary>
        /// Get any node with attribute idSystemDataviewValue  
        /// </summary>
        /// <param name="idSystemDataviewValue">Id to find node</param>
        /// <returns>XElement node find</returns>
        public XElement GetDataviewValueVariable(string idSystemDataviewValue)
        {
            XElement node = null;
            foreach (XElement act in dataViewAction)
            {
                IEnumerable<XElement> nodes = from var in act.Elements(SETTING_NAME_ACTIONVALUE)
                                              where (string)var.Attribute(SYSTEM_ACTION_DATAVIEW_VALUE) == idSystemDataviewValue
                                              select var;
                if ((nodes.Count() > 0))
                {
                    node = nodes.First();
                }

            }
            return node;
        }
        /// <summary>
        /// Return All Server in dataTable
        /// </summary>
        /// <returns>DataTable with column value (id server), display (name server)</returns>
        public DataTable LoadServerCombo()
        {
            DataTable dataTableAllServer = new DataTable();
            dataTableAllServer.TableName = "HtmlCombo";
            dataTableAllServer.Columns.Add("value", typeof(string));
            dataTableAllServer.Columns.Add("display", typeof(string));
            List<XElement> servers = xmlElement.Elements(SETTING_NAME_SERVER).ToList();
            foreach (XElement serv in servers)
            {
                DataRow dat = dataTableAllServer.NewRow();
                dat["value"] = serv.Attribute("id").Value;
                dat["display"] = serv.Attribute("name").Value;
                dataTableAllServer.Rows.Add(dat);
            }
            return dataTableAllServer;
        }
        /// <summary>
        /// Return parameter from the node in setting or global parameter
        /// </summary>
        /// <param name="parameterNodeName"> parameter node name</param>
        /// <returns></returns>
        public string GetParameterAnyGlobal(string parameterNodeName)
        {
            string returnNode = globalParameter.Element(parameterNodeName) != null ? globalParameter.Element(parameterNodeName).Value : "";
            if (parameter != null && parameter.Element(parameterNodeName) != null)
            {
                returnNode = parameter.Element(parameterNodeName).Value;
            }
            if (string.IsNullOrEmpty(returnNode))
            {
                throw new ArgumentNullException("Undefined parameter " + parameterNodeName);
            }
            return returnNode;
        }
        /// <summary>
        /// Search the dataviewname in the XElement and find it existe node paramters
        /// </summary>
        /// <param name="node">Xelement to search</param>
        /// <returns>String with dataview name</returns>
        public string GetDataviewName_Parameter(XElement node)
        {
            string dataviewName = "";
            XElement nodeParameter = node.Elements(SETTING_NAME_PARAMETERS).FirstOrDefault();
            if (nodeParameter != null)
            {
                node = nodeParameter;
            }
            if (node.Element(SETTING_NAME_DATAVIEW) != null)
            {
                dataviewName = node.Element(SETTING_NAME_DATAVIEW).Value;
            }
            return dataviewName;
        }
        /// <summary>
        /// Search defautl parameter in XElement
        /// </summary>
        /// <param name="node">Xelement to search</param>
        /// <returns>Dictionary composed of the key field Id and default value</returns>
        public Dictionary<string, string> GetDefaultFields_XElemnt(XElement node)
        {
            Dictionary<string, string> dic = new Dictionary<string, string>();
            List<XElement> defualNodes = (from e in node.Descendants(SETTING_NAME_DEFAULT) select e).ToList();
            foreach (XElement nod in defualNodes)
            {
                dic.Add(nod.Attribute(SETTING_NAME_GENERIC_ID).Value.ToString(), nod.Attribute(SETTING_NAME_VALUE).Value.ToString());
            }
            return dic;
        }
        #endregion
    }
}