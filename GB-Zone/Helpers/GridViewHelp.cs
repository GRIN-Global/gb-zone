﻿using DevExpress.Web.Mvc;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;

namespace GrinGlobal.Zone.Helpers
{
    public class GridViewHelp
    {
        #region public methods
        /// <summary>
        /// Insert new row by devexpress grid in datatable
        /// </summary>
        /// <param name="keysToInsert">Data from devexpress</param>
        /// <param name="ds">DataTable to add the new row</param>
        /// <returns>DataTable plus new rows</returns>
        public DataTable InsertRows(List<string> keysToInsert, DataTable ds)
        {
            foreach (string key in keysToInsert)
            {
                ds.Rows.Add(key);
            }
            return ds;
        }
        /// <summary>
        ///Update the specific column with the update value obtained from devexpress grid
        /// </summary>
        /// <param name="columnName">Column name</param>
        /// <param name="newValues">Data from devexpress</param>
        /// <param name="ds">DataTable to place the updated column value</param>
        /// <returns>DataTable plus new columns with changes</returns>
        public DataTable UpdateColumn(string columnName, Dictionary<string, string> newValues, DataTable ds)
        {
            foreach (string item in newValues.Keys)
            {
                var row = ds.Rows.Find(item);
                row[columnName] = newValues[item];
            }
            return ds;
        }
        /// <summary>
        /// Update to a specific cell in DataTable
        /// </summary>
        /// <param name="columnName">Columnm name</param>
        /// <param name="columnKey">Primary column key</param>
        /// <param name="index">Value to find in the Primary column key</param>
        /// <param name="newValue">value to set in the column name</param>
        /// <param name="dt">DataTable to remplace</param>
        /// <returns>Return the DataTable with the change</returns>
        public DataTable AddRow(string columnName, string columnKey, string index, string newValue, DataTable dt)
        {
            DataRow dr = dt.Select(columnKey + "= " + index).DefaultIfEmpty(null).FirstOrDefault();
            if (dr != null)//Update value existe
            {
                dr[columnName] = newValue;
            }
            else// Add new row
            {
                dr = dt.NewRow();
                dr[columnKey] = index;
                dr[columnName] = newValue;
                dt.Rows.Add(dr);
            }
            return dt;
        }
        /// <summary>
        /// Convert DataTable in json 
        /// </summary>
        /// <param name="table">DataTable to convert</param>
        /// <param name="maxJsonLength">Maximum characters in the json</param>
        /// <returns>string with json</returns>
        public string DataTableToJsonWithJavaScriptSerializer(DataTable table, int maxJsonLength)
        {
            JavaScriptSerializer jsSerializer = new JavaScriptSerializer();
            jsSerializer.MaxJsonLength = maxJsonLength;
            List<Dictionary<string, object>> parentRow = new List<Dictionary<string, object>>();
            Dictionary<string, object> childRow;
            foreach (DataRow row in table.Rows)
            {
                childRow = new Dictionary<string, object>();
                foreach (DataColumn col in table.Columns)
                {
                    childRow.Add(col.ColumnName, row[col]);
                }
                parentRow.Add(childRow);
            }
            return jsSerializer.Serialize(parentRow);
        }
        /// <summary>
        /// Convert array in json
        /// </summary>
        /// <param name="datos">Array to convert</param>
        /// <param name="maxJsonLength">Maximum characters in the json</param>
        /// <returns>string with json</returns>
        public string ArrayToJsonWithJavaScriptSerializer(string[] datos, int maxJsonLength)
        {
            JavaScriptSerializer jsSerializer = new JavaScriptSerializer();
            jsSerializer.MaxJsonLength = maxJsonLength;
            return jsSerializer.Serialize(datos);
        }
        /// <summary>
        /// Create Column for gridview in devexpress
        /// </summary>
        /// <param name="dc">Data colum to convert</param>
        /// <param name="mod">DataSet to get all the information about the column in case the combobox</param>
        /// <returns>Collumn create in devexpress</returns>
        public MVCxGridViewColumn CreateColumn(DataColumn dc, DataSet mod)
        {
            MVCxGridViewColumn col = new MVCxGridViewColumn();
            col.FieldName = dc.ColumnName;
            col.Name = dc.ColumnName;
            col.Caption = dc.Caption;
            if (dc.ColumnMapping == System.Data.MappingType.Hidden)
            {
                col.Visible = false;
            }
            if (dc.ExtendedProperties["gui_hint"].ToString() == "DATE_CONTROL")
                col.ColumnType = MVCxGridViewColumnType.DateEdit;
            else if (dc.ExtendedProperties["gui_hint"].ToString() == "INTEGER_CONTROL")
                col.ColumnType = MVCxGridViewColumnType.SpinEdit;
            else if (dc.ExtendedProperties["gui_hint"].ToString() == "TEXT_CONTROL")
                col.ColumnType = MVCxGridViewColumnType.TextBox;
            else if (dc.ExtendedProperties["gui_hint"].ToString() == "TOGGLE_CONTROL")
                col.ColumnType = MVCxGridViewColumnType.CheckBox;
            else if (dc.ExtendedProperties["gui_hint"].ToString() == "SMALL_SINGLE_SELECT_CONTROL")
            {
                col.EditorProperties().ComboBox(p =>
                {
                    p.DataSource = mod.Tables[dc.ExtendedProperties["group_name"].ToString()];
                    p.TextField = "title";
                    p.ValueField = "value";
                    p.ValueType = typeof(string);
                });
            }
            return col;
        }
        #endregion
    }
}