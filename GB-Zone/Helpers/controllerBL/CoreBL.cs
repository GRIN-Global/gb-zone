﻿using GrinGlobal.Zone.Helpers.setting;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Xml.Linq;

namespace GrinGlobal.Zone.Helpers.controllerBL
{
    public class CoreBL
    {
        protected HtmlHelp hHelp;
        protected IGrinGlobalSoapHelp sopH;
        protected GridViewHelp gvH;
        protected Dictionary<string, string> dicDataView;
        protected Dictionary<string, object> dataCollectionAction = new Dictionary<string, object>();
        /// <summary>
        /// This variable will become ViewData in controller
        /// </summary>
        public Dictionary<string, string> DicDataView { get { return dicDataView; } }

        /// <summary>
        /// Build initial for the entire business layer and instantiate HtmlHelp, GridviewHelp
        /// </summary>
        /// <param name="sopH">Instancia form class GrinGlobalSoapHelp, for get the setting fiel and service GirnGlobal SOAP </param>
        public CoreBL(IGrinGlobalSoapHelp sopH)
        {
            this.sopH = sopH;
            hHelp = new HtmlHelp();
            dicDataView = new Dictionary<string, string>();
            gvH = new GridViewHelp();
        }

        #region public methods
        /// <summary>
        /// Saved to save the data table in the data view and insert the action specified in the stteing.xml file
        /// </summary>
        /// <param name="dt">DataTable to save</param>
        /// <param name="newParameters">parameter to search the dataview</param>
        /// <param name="sysActionId">Action id in the setting.xml file</param>
        /// <param name="dataViewAction">Specific data view to save the data table</param>
        public void SaveAction(DataTable dt, string newParameters, string sysActionId, string dataViewAction = "")
        {
            XElement nodeAction = sopH.SetH.GetNodeAction(sopH.SetH.SYSTEM_ACTION_DATAVIEW, sysActionId);
            string idAction = nodeAction.Attribute(sopH.SetH.SETTING_NAME_GENERIC_ID).Value != null ? nodeAction.Attribute(sopH.SetH.SETTING_NAME_GENERIC_ID).Value.ToString() : "";
            string dataViewNameAction = nodeAction.Element(sopH.SetH.SETTING_NAME_PARAMETERS).Element(sopH.SetH.SETTING_NAME_DATAVIEW).Value != null ? nodeAction.Element(sopH.SetH.SETTING_NAME_PARAMETERS).Element(sopH.SetH.SETTING_NAME_DATAVIEW).Value.ToString() : "";
            if (dataViewAction != "")
            {
                dataViewNameAction = dataViewAction;
            }
            DataTable actionDT = sopH.GetDataActionOne(newParameters, new Dictionary<string, string>(), idAction);
            int ii = -1;
            string keyColumnAct = actionDT.PrimaryKey[0] != null ? actionDT.PrimaryKey[0].ColumnName : "";
            string keyColum = dt.PrimaryKey[0] != null ? dt.PrimaryKey[0].ColumnName : "";
            foreach (DataRow dtF in dt.Rows)
            {
                DataRow dr = actionDT.NewRow();
                dr[keyColumnAct] = ii--;
                dr[keyColum] = dtF[keyColum].ToString();
                dr = sopH.SetH.GetRowAction(dr, idAction, dataCollectionAction);
                actionDT.Rows.Add(dr);
            }
            DataSet newDaS = sopH.SaveDataAction(newParameters, new Dictionary<string, string>(), idAction, actionDT);
        }

        public void UpdateAction(string parametersId, string sysActionId, DataviewElement dvElement)
        {
            XElement nodeAction = sopH.SetH.GetNodeAction(sopH.SetH.SYSTEM_ACTION_DATAVIEW, sysActionId);
            string idAction = nodeAction.Attribute(sopH.SetH.SETTING_NAME_GENERIC_ID).Value != null ? nodeAction.Attribute(sopH.SetH.SETTING_NAME_GENERIC_ID).Value.ToString() : "";
            string dataViewNameAction = nodeAction.Element(sopH.SetH.SETTING_NAME_PARAMETERS).Element(sopH.SetH.SETTING_NAME_DATAVIEW).Value != null ? nodeAction.Element(sopH.SetH.SETTING_NAME_PARAMETERS).Element(sopH.SetH.SETTING_NAME_DATAVIEW).Value.ToString() : "";
            DataSet inventNew = sopH.GetData(parametersId, dvElement.DataviewName);
            foreach (DataRow dr in inventNew.Tables[dvElement.DataviewName].Rows)
            {
                foreach (DataColumn dc in inventNew.Tables[dvElement.DataviewName].Columns)
                {
                    if (dc.ReadOnly != true)
                    {
                        object data = sopH.SetH.GetCellAction(dc.ColumnName, idAction, dataCollectionAction);
                        if (data != null)
                        {
                            dr[dc] = data;
                        }
                    }
                }
            }
            DataSet newDaS = sopH.SaveData(parametersId, inventNew.Tables[dvElement.DataviewName], dvElement.DataviewName);
        }
        #endregion
        #region protected methods
        protected DataSet _GetDataSet(string parameters)
        {
            DataSet datS = sopH.GetData(parameters);
            dicDataView.Add(hHelp.SYS_VIEWDATA_DATATABLE, sopH.SetH.DataViewName);
            dicDataView.Add(hHelp.SYS_VIEWDATA_GRIDRESULT, hHelp.SYS_RAZO_GENERICGRIDVIEWSEARCH);
            dicDataView.Add(hHelp.SYS_VIEWDATA_PARAMETERS, parameters);
            return datS;
        }

        protected void AddDicDataView(string key, string value)
        {
            if (dicDataView.ContainsKey(key))
            {
                dicDataView[key] = value;
            }
            else
            {
                dicDataView.Add(key, value);
            }
        }
        /// <summary>
        /// Obtain the information about the specific data view in the file label and write the parameters to be consulted in the viewdata["parameters"]
        /// </summary>
        /// <param name="formC">Data from formuled</param>
        /// <param name="sysFieldId">id to search in fiedls</param>
        /// <returns>DataTable with result</returns>
        protected DataTable GetDataviewWhioutUnderscore(FormCollection formC, string sysFieldId)
        {
            XElement node = sopH.SetH.GetField_AtributeValue(sysFieldId);
            string dataviewName = sopH.SetH.GetDataviewName_Parameter(node);
            Dictionary<string, string> parametersDic = sopH.GetParameters(formC, dataviewName);
            foreach (var key in formC.AllKeys)
            {
                string newkey = key.Replace("_", "");
                if (parametersDic.ContainsKey(newkey))
                {
                    parametersDic[newkey] = formC[key];
                }
            }
            string parameters = sopH.Parameters_DictionaryToString(parametersDic);
            AddDicDataView(hHelp.SYS_VIEWDATA_PARAMETERS, parameters);
            DataSet datS = sopH.GetData(parameters, dataviewName);
            return datS.Tables[dataviewName];
        }

        protected DataTable GetDataviewById(string parameterId, string sysFieldId)
        {
            XElement node = sopH.SetH.GetField_AtributeValue(sysFieldId);
            string dataviewName = sopH.SetH.GetDataviewName_Parameter(node);
            Dictionary<string, string> parametersDic = sopH.GetOnlyParameters(dataviewName);
            string id = node.Attribute(sopH.SetH.SETTING_NAME_GENERIC_ID).Value;
            if (parametersDic.ContainsKey(id))
            {
                parametersDic[id] = parameterId;
            }
            id = id.Replace("_", "");
            if (parametersDic.ContainsKey(id))
            {
                parametersDic[id] = parameterId;
            }
            string parameters = sopH.Parameters_DictionaryToString(parametersDic);
            DataSet datS = sopH.GetData(parameters, dataviewName);
            return datS.Tables[dataviewName];
        }

        protected DataviewElement GetParameteresById(string parameterId, string sysFieldId)
        {
            DataviewElement dic = new DataviewElement();
            XElement node = sopH.SetH.GetField_AtributeValue(sysFieldId);
            string dataviewName = sopH.SetH.GetDataviewName_Parameter(node);
            dic.DictionaryParamtersOnly = sopH.GetOnlyParameters(dataviewName);
            Dictionary<string, string> parametersDic = sopH.GetOnlyParameters(dataviewName);
            string id = node.Attribute(sopH.SetH.SETTING_NAME_GENERIC_ID).Value;
            if (parametersDic.ContainsKey(id))
            {
                parametersDic[id] = parameterId;
            }
            id = id.Replace("_", "");
            if (parametersDic.ContainsKey(id))
            {
                parametersDic[id] = parameterId;
            }
            dic.StringParamters = sopH.Parameters_DictionaryToString(parametersDic);
            dic.DictionaryParamters = parametersDic;
            dic.DataviewName = dataviewName;
            return dic;
        }
        #endregion
    }

    public class DataviewElement
    {
        public string DataviewName { set; get; }
        public string StringParamters { set; get; }
        public Dictionary<string, string> DictionaryParamters { set; get; }
        public Dictionary<string, string> DictionaryParamtersOnly { set; get; }
    }
}