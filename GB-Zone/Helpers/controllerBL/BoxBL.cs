﻿using GrinGlobal.Zone.Helpers.setting;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Xml.Linq;

namespace GrinGlobal.Zone.Helpers.controllerBL
{
    public class BoxBL : CoreBL
    {
        /// <summary>
        /// Initial constructor to get the parent constructor
        /// </summary>
        /// <param name="sopH">Instancia form class GrinGlobalSoapHelp, for get the setting fiel and service GirnGlobal SOAP </param>
        public BoxBL(IGrinGlobalSoapHelp sopH) : base(sopH)
        {

        }
        #region public methods
        /// <summary>
        /// Insert new item in the storage location
        /// </summary>
        /// <param name="insert"></param>
        /// <param name="parameters"></param>
        /// <param name="sysColumnInventoryValue"></param>
        /// <param name="sysColumnSaveValue"></param>
        /// <param name="splitFields"></param>
        /// <returns></returns>
        public DataSet Add(DataTable insert, string parameters, string sysColumnInventoryValue, string sysColumnSaveValue, List<string> splitFields)
        {
            string searchValue = string.Join("','", insert.AsEnumerable().Where(row => row.Field<string>(sysColumnInventoryValue) != null).Select(row => row.Field<string>(sysColumnInventoryValue)).ToList());
            Dictionary<string, string> parameterD = sopH.Parameters_StringToDictionary(parameters);
            Dictionary<string, string> parameterOrigen = sopH.Parameters_StringToDictionary(parameters);
            foreach (var key in parameterD.Keys.ToList())
            {
                parameterD[key] = "";
            }
            parameterD[sysColumnInventoryValue] = searchValue;
            string newParameters = sopH.Parameters_DictionaryToString(parameterD);
            DataSet datS = sopH.GetData(newParameters);
            DataTable model = datS.Tables[sopH.SetH.DataViewName];
            List<string> insertL = insert.AsEnumerable().Where(row => row.Field<string>(sysColumnInventoryValue) != null).Select(row => row.Field<string>(sysColumnInventoryValue)).ToList();
            List<string> modelL = model.AsEnumerable().Select(row => row.Field<string>(sysColumnInventoryValue)).ToList();
            List<string> distinc = insertL.Except(modelL).ToList();
            if (distinc.Count > 0)
            {
                string datoError = string.Join(",", distinc.ToArray());
                throw new System.ArgumentOutOfRangeException("The elements do not exist in the system |@" + datoError + "", "NO-001");
            }
            Dictionary<string, string> insertD = insert.AsEnumerable().ToDictionary<DataRow, string, string>(row => row[sysColumnInventoryValue].ToString(), row => row[sysColumnSaveValue].ToString());
            foreach (DataRow row in model.Rows)
            {
                string key = row[sysColumnInventoryValue].ToString();
                row[sysColumnSaveValue] = insertD[key];
                foreach (string splitItem in splitFields)
                {
                    row[splitItem] = parameterOrigen[splitItem];
                }
            }
            DataSet saveDS = sopH.SaveData(newParameters, model);
            return saveDS;
        }

        /// <summary>
        /// Get the all attributes the component the inventory number
        /// </summary>
        /// <returns>Valid list attributes in the configuration file</returns>
        public List<string> GetAllItemToSplit()
        {
            List<string> splitColumn = new List<string>();
            List<XElement> nodos = new List<XElement>();
            nodos.Add(sopH.SetH.GetColumn_Name(sopH.SetH.GetColumnVariable(hHelp.SYS_BOX_COLUMNNAME_STORAGELOCATION1)));
            nodos.Add(sopH.SetH.GetColumn_Name(sopH.SetH.GetColumnVariable(hHelp.SYS_BOX_COLUMNNAME_STORAGELOCATION2)));
            nodos.Add(sopH.SetH.GetColumn_Name(sopH.SetH.GetColumnVariable(hHelp.SYS_BOX_COLUMNNAME_STORAGELOCATION3)));
            nodos.Add(sopH.SetH.GetColumn_Name(sopH.SetH.GetColumnVariable(hHelp.SYS_BOX_COLUMNNAME_STORAGELOCATION4)));
            foreach (XElement nodo in nodos)
            {
                if (nodo.Attribute(hHelp.SYS_BOX_ATTRIBUTE_ALLOCATION_SPLIT) != null)
                {
                    splitColumn.Add(nodo.Value);
                }
            }
            return splitColumn;
        }

        /// <summary>
        /// Convert the parameters into a valid list of attributes in the configuration file
        /// </summary>
        /// <param name="parameter">View page parameters</param>
        /// <returns>Parameters in dictionary [name - value]</returns>
        public Dictionary<string, string> CastParameter(Dictionary<string, string> parameter)
        {
            string inventory = sopH.SetH.GetColumnVariable(hHelp.SYS_BOX_COLUMNNAME_INVENTORY_GETDATA);
            string location1 = sopH.SetH.GetColumnVariable(hHelp.SYS_BOX_COLUMNNAME_STORAGELOCATION1);
            string location2 = sopH.SetH.GetColumnVariable(hHelp.SYS_BOX_COLUMNNAME_STORAGELOCATION2);
            string location3 = sopH.SetH.GetColumnVariable(hHelp.SYS_BOX_COLUMNNAME_STORAGELOCATION3);
            if (parameter.ContainsKey(inventory))
            {
                XElement node = sopH.SetH.GetColumn_Name(inventory);
                XElement node1 = sopH.SetH.GetColumn_Name(location1);
                XElement node2 = sopH.SetH.GetColumn_Name(location2);
                XElement node3 = sopH.SetH.GetColumn_Name(location3);
                if (node.Attribute(hHelp.SYS_BOX_ATTRIBUTE_SPLIT) != null)
                {
                    char divid = (char)Int32.Parse(node.Attribute(hHelp.SYS_BOX_ATTRIBUTE_SPLIT).Value);
                    string[] parts = parameter[inventory].Split(divid);
                    if (node1.Attribute(hHelp.SYS_BOX_ATTRIBUTE_ALLOCATION_SPLIT) != null)
                    {
                        int indice = Int32.Parse(node1.Attribute(hHelp.SYS_BOX_ATTRIBUTE_ALLOCATION_SPLIT).Value);
                        parameter[location1] = parts[indice];
                    }
                    if (node2.Attribute(hHelp.SYS_BOX_ATTRIBUTE_ALLOCATION_SPLIT) != null)
                    {
                        int indice = Int32.Parse(node2.Attribute(hHelp.SYS_BOX_ATTRIBUTE_ALLOCATION_SPLIT).Value);
                        parameter[location2] = parts[indice];
                    }
                    if (node3.Attribute(hHelp.SYS_BOX_ATTRIBUTE_ALLOCATION_SPLIT) != null)
                    {
                        int indice = Int32.Parse(node3.Attribute(hHelp.SYS_BOX_ATTRIBUTE_ALLOCATION_SPLIT).Value);
                        parameter[location3] = parts[indice];
                    }
                    parameter[inventory] = "";
                }

            }
            return parameter;
        }

        /// <summary>
        /// Search and return the column to save the storage location
        /// </summary>
        /// <returns>Column to save storare location</returns>
        public string GetColumnSave()
        {
            string returnColumn = "";
            List<XElement> nodos = new List<XElement>();
            nodos.Add(sopH.SetH.GetColumn_Name(sopH.SetH.GetColumnVariable(hHelp.SYS_BOX_COLUMNNAME_STORAGELOCATION1)));
            nodos.Add(sopH.SetH.GetColumn_Name(sopH.SetH.GetColumnVariable(hHelp.SYS_BOX_COLUMNNAME_STORAGELOCATION2)));
            nodos.Add(sopH.SetH.GetColumn_Name(sopH.SetH.GetColumnVariable(hHelp.SYS_BOX_COLUMNNAME_STORAGELOCATION3)));
            nodos.Add(sopH.SetH.GetColumn_Name(sopH.SetH.GetColumnVariable(hHelp.SYS_BOX_COLUMNNAME_STORAGELOCATION4)));
            foreach (XElement nodo in nodos)
            {
                if (nodo.Attribute(hHelp.SYS_BOX_ATTRIBUTE_SAVE) != null && nodo.Attribute(hHelp.SYS_BOX_ATTRIBUTE_SAVE).Value == "1")
                {
                    returnColumn = nodo.Value;
                }
            }
            if (string.IsNullOrEmpty(returnColumn))
            {
                throw new ArgumentNullException("The sysSave attribute was not defined in the configuration");
            }
            return returnColumn;
        }
        #endregion
    }
}