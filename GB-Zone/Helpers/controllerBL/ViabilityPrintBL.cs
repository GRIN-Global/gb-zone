﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Xml.Linq;

namespace GrinGlobal.Zone.Helpers.controllerBL
{
    public class ViabilityPrintBL : CoreBL
    {
        /// <summary>
        /// Initial constructor to get the parent constructor
        /// </summary>
        /// <param name="sopH">Instancia form class GrinGlobalSoapHelp, for get the setting fiel and service GirnGlobal SOAP </param>
        public ViabilityPrintBL(IGrinGlobalSoapHelp sopH) : base(sopH)
        {

        }
        #region public methods
        /// <summary>
        /// Get all the data from the Grin Global SOAP to work viability print module
        /// </summary>
        /// <param name="parameters">String to the parameters [order_request_id]</param>
        /// <returns>Find inventories within the order</returns>
        public DataSet GetDataSet(string parameters)
        {
            DataSet datS = _GetDataSet(parameters);
            Dictionary<string, string> parameterDics = sopH.Parameters_StringToDictionary(parameters);
            FormCollection formCollection = new FormCollection();
            foreach (KeyValuePair<string, string> item in parameterDics)
            {
                formCollection.Add(item.Key, item.Value);
            }
            DataTable combo = GetDataviewWhioutUnderscore(formCollection, hHelp.SYS_VIABILITYPRINT_FIELDNAME_INVNETORYVIABILITYRULECOMBO);
            DataRow dr = combo.NewRow();
            dr["inventory_viability_rule_id"] = 0;
            dr["name"] = "### Select rule";
            combo.Rows.Add(dr);
            datS.Tables.Add(combo.Copy());
            dicDataView.Add(hHelp.SYS_VIABILITYPRINT_COMBOBOX_INVNETORYVIABILITYRULE, combo.TableName);
            DataTable headers = GetDataviewWhioutUnderscore(formCollection, hHelp.SYS_VIABILITYPRINT_FIELDNAME_NUMBERORDER);
            datS.Tables.Add(headers.Copy());
            dicDataView.Add(hHelp.SYS_VIABILITYPRINT_DATAVIEW_HEADER, headers.TableName);
            dicDataView.Add(hHelp.SYS_VIABILITYPRINT_JSON_EMPTYVIABILITYJSON, GetEmptyViabilityData(datS.Tables[sopH.SetH.DataViewName]));
            dicDataView.Add(hHelp.SYS_VIABILITYPRINT_JSON_ALLITEMJSON, gvH.DataTableToJsonWithJavaScriptSerializer(datS.Tables[sopH.SetH.DataViewName], sopH.SetH.GlobalMaxJsonLength));
            dicDataView[hHelp.SYS_VIEWDATA_PARAMETERS] = parameters;
            return datS;
        }

        /// <summary>
        /// Create inventory viability for each of the inventories within order
        /// </summary>
        /// <param name="parameters">String to the parameters [order_request_id]</param>
        /// <param name="inventoryVialityRuleId">Inventory viability rule id</param>
        public void Save(string parameters, int inventoryVialityRuleId)
        {
            DataSet datS = sopH.GetData(parameters);
            XElement node = sopH.SetH.GetField_AtributeValue(hHelp.SYS_VIABILITYPRINT_FIELDNAME_INVNETORYVIABILITY);
            string dataviewName = sopH.SetH.GetDataviewName_Parameter(node);
            DataTable inventoryViability = sopH.GetData(parameters, dataviewName).Tables[dataviewName];
            int i = -1;
            DateTime hoy = DateTime.Now;
            Dictionary<string, string> orderRequestItemId = new Dictionary<string, string>();
            DataTable inventoryViabilityData = GetDataviewById("", hHelp.SYS_VIABILITYPRINT_FIELDNAME_INVNETORYVIABILITYDATA);
            DataTable inventoryViabilityRule = GetDataviewById(inventoryVialityRuleId.ToString(), hHelp.SYS_VIABILITYPRINT_FIELDNAME_INVNETORYVIABILITYRULEID);
            int numberReplicates = Convert.ToInt32(inventoryViabilityRule.Rows[0][hHelp.SYS_VIABILITYPRINT_COLUMNNAME_NUMBEROFREPLICANTE].ToString());
            int seedReplicates = Convert.ToInt32(inventoryViabilityRule.Rows[0][hHelp.SYS_VIABILITYPRINT_COLUMNNAME_SEEDSPERREPLICATE].ToString());
            List<string> invenotryIds = new List<string>();
            foreach (DataRow dr in datS.Tables[sopH.SetH.DataViewName].Rows)
            {
                DataRow newDr = inventoryViability.NewRow();
                newDr[hHelp.SYS_VIABILITY_COLUMNNAME_INVENTORYVIABILITYID] = i--;
                newDr[hHelp.SYS_VIABILITY_COLUMNNAME_INVENTORYVIABILITYRULEID] = inventoryVialityRuleId;
                newDr[hHelp.SYS_VIABILITY_COLUMNNAME_INVENTORYID] = dr[hHelp.SYS_VIABILITY_COLUMNNAME_INVENTORYID];
                invenotryIds.Add(dr[hHelp.SYS_VIABILITY_COLUMNNAME_INVENTORYID].ToString());
                newDr[hHelp.SYS_VIABILITYPRINT_COLUMNNAME_TESTEDDATE] = hoy;
                inventoryViability.Rows.Add(newDr);
                orderRequestItemId.Add(dr[hHelp.SYS_VIABILITY_COLUMNNAME_INVENTORYID].ToString(), dr[hHelp.SYS_VIABILITYPRINT_COLUMNNAME_ORDERREQUESTITEMID].ToString());
            }
            DataSet result = sopH.SaveData(parameters, inventoryViability, dataviewName);
            SaveAction(datS.Tables[sopH.SetH.DataViewName], parameters, hHelp.SYS_DATAVIEWACTIONNAME_GET_INVENTORYACTION, null);
            DataTable inventoryViabilityNew = GetDataviewById(string.Join(",", invenotryIds.ToArray()), hHelp.SYS_VIABILITYPRINT_FIELDNAME_INVNETORYVIABILITY);
            i = -1;
            DateTime now = DateTime.Now;
            List<DataRow> rowDs = inventoryViabilityNew.Rows.Cast<DataRow>()
                       .Where(x => x.Field<int?>(hHelp.SYS_VIABILITYPRINT_COLUMNNAME_PERCENTVIABLE) == null).ToList();
            foreach (DataRow dr in rowDs)
            {
                int replicationnumber = 1;
                for (int j = 0; j < numberReplicates; j++)
                {
                    DataRow drData = inventoryViabilityData.NewRow();
                    drData[hHelp.SYS_VIABILITYPRINT_COLUMNNAME_INVENTORYVIABILITYDATAID] = i--;
                    drData[hHelp.SYS_VIABILITY_COLUMNNAME_INVENTORYID] = dr[hHelp.SYS_VIABILITY_COLUMNNAME_INVENTORYID];
                    drData[hHelp.SYS_VIABILITY_COLUMNNAME_INVENTORYVIABILITYID] = dr[hHelp.SYS_VIABILITY_COLUMNNAME_INVENTORYVIABILITYID];
                    drData[hHelp.SYS_VIABILITYPRINT_COLUMNNAME_ORDERREQUESTITEMID] = orderRequestItemId[dr[hHelp.SYS_VIABILITY_COLUMNNAME_INVENTORYID].ToString()];
                    drData[hHelp.SYS_VIABILITYPRINT_COLUMNNAME_REPLICATIONNUMBER] = replicationnumber++;
                    drData[hHelp.SYS_VIABILITYPRINT_COLUMNNAME_COUNTNUMBER] = seedReplicates;
                    drData[hHelp.SYS_VIABILITYPRINT_COLUMNNAME_COUNTDATE] = now;
                    drData[hHelp.SYS_VIABILITYPRINT_COLUMNNAME_NORMALCOUNT] = 0;
                    inventoryViabilityData.Rows.Add(drData);
                }
            }
            DataSet resultData = sopH.SaveData("", inventoryViabilityData, inventoryViabilityData.TableName);
        }
        #endregion
        #region private methods
        private string GetEmptyViabilityData(DataTable dt)
        {
            List<string> emptyViabilityDataList = new List<string>();
            var result = dt.AsEnumerable().Where(row => row.Field<DateTime?>(hHelp.SYS_VIABILITYPRINT_COLUMNNAME_TESTEDDATE) != null
            && row.Field<int?>("percent_viable") == null);
            if (result.Count() > 0)
            {
                foreach (DataRow row in result)
                {
                    emptyViabilityDataList.Add(row[hHelp.SYS_VIABILITY_COLUMNNAME_INVENTORYID].ToString());
                }
            }
            return gvH.ArrayToJsonWithJavaScriptSerializer(emptyViabilityDataList.ToArray(), sopH.SetH.GlobalMaxJsonLength);
        }
        #endregion
    }
}