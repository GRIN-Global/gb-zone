﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Xml.Linq;

namespace GrinGlobal.Zone.Helpers.controllerBL
{
    public class ViabilityDataCollectionBL : CoreBL
    {
        /// <summary>
        /// Initial constructor to get the parent constructor
        /// </summary>
        /// <param name="sopH">Instancia form class GrinGlobalSoapHelp, for get the setting fiel and service GirnGlobal SOAP </param>
        public ViabilityDataCollectionBL(IGrinGlobalSoapHelp sopH) : base(sopH)
        {

        }
        #region public function
        /// <summary>
        /// Get all the data to build the module to get data in the experiment
        /// </summary>
        /// <param name="formC">Form data from web [inventory_vaibility_id, replication_number]</param>
        /// <param name="dataJson">dataJson to concatner more elemente to conver in ViewData</param>
        /// <returns>Return information in the json dictionary parser</returns>
        public Dictionary<string, object> AddDictionaryAll(FormCollection formC, Dictionary<string, object> dataJson)
        {
            ///Load inventory viablity
            DataTable inventoryViabilityDataTable = GetDataviewWhioutUnderscore(formC, hHelp.SYS_VIABILITYDATACOLLECTION_FIELDNAME_INVENTORYVIABILITYDATA);
            formC.Add("inventory_viability_id", inventoryViabilityDataTable.Rows[0][hHelp.SYS_VIABILITY_COLUMNNAME_INVENTORYVIABILITYID].ToString());
            DataTable inventoryViabilityTable = GetDataviewWhioutUnderscore(formC, hHelp.SYS_VIABILITYDATACOLLECTION_FIELDNAME_INVENTORYVIABILITY);
            dataJson = AddDicctionary(dataJson, inventoryViabilityTable);
            string inventoryId = inventoryViabilityTable.Rows[0][hHelp.SYS_VIABILITYDATACOLLECTION_COLUMNNAME_INVENTORYID].ToString();
            string ruleId = inventoryViabilityTable.Rows[0][hHelp.SYS_VIABILITY_COLUMNNAME_INVENTORYVIABILITYRULEID].ToString();
            ///Load invenotry
            DataTable inventoryTable = GetDataviewById(inventoryId, hHelp.SYS_VIABILITYDATACOLLECTION_FIELDNAME_INVENTORY);
            dataJson = AddDicctionary(dataJson, inventoryTable);
            ///Load inventory_viability_data
            dataJson = AddDicctionary(dataJson, inventoryViabilityDataTable);
            ///Load inventory_viability_rule
            DataTable inventoryViabilityRuleTable = GetDataviewById(ruleId, hHelp.SYS_VIABILITYDATACOLLECTION_FIELDNAME_INVENTORYVIABILITYRULE);
            dataJson = AddDicctionary(dataJson, inventoryViabilityRuleTable);
            ///Load accession
            DataTable accession = GetDataviewById(inventoryId, hHelp.SYS_VIABILITYDATACOLLECTION_FIELDNAME_ACCESSION);
            dataJson = AddDicctionary(dataJson, accession);
            return dataJson;
        }

        /// <summary>
        /// Save all information on the inventory viability data page
        /// </summary>
        /// <param name="formC">Form data from web [inventory_vaibility_id, replication_number] </param>
        public void Save(FormCollection formC)
        {
            Dictionary<string, string> parametersDic = sopH.GetParameters(formC);
            foreach (var key in formC.AllKeys)
            {
                string newkey = key.Replace("_", "");
                if (parametersDic.ContainsKey(newkey))
                {
                    parametersDic[newkey] = formC[key];
                }
            }
            string parameters = sopH.Parameters_DictionaryToString(parametersDic);
            string parametersClose = parameters;
            DataSet datS = sopH.GetData(parameters);
            string keyColumn = datS.Tables[sopH.SetH.DataViewName].PrimaryKey[0].ColumnName;
            DataRow results = datS.Tables[sopH.SetH.DataViewName].Rows.Cast<DataRow>()
                       .FirstOrDefault(x => x.Field<int>("replication_number") == Convert.ToInt32(formC["replication_number"]));
            if (results != null)
            {
                results = SetInvenotryViabilityData(results, formC);
            }
            else
            {
                DataRow dr = datS.Tables[sopH.SetH.DataViewName].NewRow();
                dr[keyColumn] = -1;
                dr = SetInvenotryViabilityData(dr, formC);
                datS.Tables[sopH.SetH.DataViewName].Rows.Add(dr);
            }
            sopH.SaveData(parameters, datS.Tables[sopH.SetH.DataViewName]);
            ///Load inventory viability data
            DataTable inventoryVaibilityDataTable = sopH.GetData(parameters).Tables[sopH.SetH.DataViewName];
            ///Load inventory viablity
            DataTable inventoryViabilityTable = GetDataviewWhioutUnderscore(formC, hHelp.SYS_VIABILITYDATACOLLECTION_FIELDNAME_INVENTORYVIABILITY);
            string inventoryId = inventoryViabilityTable.Rows[0][hHelp.SYS_VIABILITYDATACOLLECTION_COLUMNNAME_INVENTORYID].ToString();
            string ruleId = inventoryViabilityTable.Rows[0][hHelp.SYS_VIABILITY_COLUMNNAME_INVENTORYVIABILITYRULEID].ToString();
            //Load inventory viability rule            
            DataTable inventoryViabilityRuleTable = GetDataviewById(ruleId, hHelp.SYS_VIABILITYDATACOLLECTION_FIELDNAME_INVENTORYVIABILITYRULE);
            int seedsPerReplicate = Convert.ToInt32(inventoryViabilityRuleTable.Rows[0]["seeds_per_replicate"].ToString());
            int numberOfReplicantes = Convert.ToInt32(inventoryViabilityRuleTable.Rows[0]["number_of_replicates"].ToString());
            string cooperador_id = formC["counter_cooperator_id"].ToString();
            DateTime hoy = DateTime.Now;
            inventoryViabilityTable.Rows[0]["tested_date"] = formC["tested_date"] + " " + hoy.Hour + ":" + hoy.Minute + ":" + hoy.Second;
            dataCollectionAction = new Dictionary<string, object>();
            dataCollectionAction.Add("inventory_id", inventoryId);
            //dataCollectionAction.Add("inventory_viability_rule_id", ruleId);
            if (!string.IsNullOrEmpty(cooperador_id))
            {
                dataCollectionAction.Add("cooperator_id", cooperador_id);
            }
            inventoryViabilityTable = CloseInvenotryViabilityData(inventoryVaibilityDataTable, numberOfReplicantes, seedsPerReplicate
                , inventoryViabilityTable);
            sopH.SaveData(parameters, inventoryViabilityTable);
        }
        /// <summary>
        /// The function to validate if the cooperator's identification exists
        /// </summary>
        /// <param name="formC">cooperator_id</param>
        /// <returns>Row whit information</returns>
        public DataRow ValidateCooperatorId(FormCollection formC)
        {
            DataRow regreso = null;
            DataTable cooperadorTable = GetDataviewWhioutUnderscore(formC, "sysCooperatorId");
            if (cooperadorTable.Rows.Count == 1)
            {
                regreso = cooperadorTable.Rows[0];
            }
            return regreso;
        }

        /// <summary>
        /// Obtain the history of the viability of the inventory from the inventory id.
        /// </summary>
        /// <param name="formC">Form data from web [inventory_id]</param>
        /// <returns>Result of serach</returns>
        public DataSet GridViewInventoryViability(FormCollection formC)
        {
            DataSet datS = new DataSet();
            DataTable dt = GetDataviewWhioutUnderscore(formC, hHelp.SYS_VIABILITYDATACOLLECTION_FIELDNAME_INVENTORYVIABILITY);
            AddDicDataView(hHelp.SYS_VIEWDATA_DATATABLE, dt.TableName);
            AddDicDataView(hHelp.SYS_VIEWDATA_CONTROLLER, "ViabilityDataCollection");
            AddDicDataView(hHelp.SYS_VIEWDATA_CONTROLLERACTION, "GridViewInventoryViability");
            datS.Tables.Add(dt.Copy());
            return datS;
        }

        /// <summary>
        /// Obtain the other replica of the same experiment by  inventory viability id.
        /// </summary>
        /// <param name="formC">Form data from web [inventory_viability_id]</param>
        /// <returns>Result of serach</returns>
        public DataSet GridViewInventoryViabilityData(FormCollection formC)
        {
            DataSet datS = new DataSet();
            DataTable dt = GetDataviewWhioutUnderscore(formC, hHelp.SYS_VIABILITYDATACOLLECTION_FIELDNAME_INVENTORYVIABILITYDATA);
            AddDicDataView(hHelp.SYS_VIEWDATA_DATATABLE, dt.TableName);
            AddDicDataView(hHelp.SYS_VIEWDATA_CONTROLLER, "ViabilityDataCollection");
            AddDicDataView(hHelp.SYS_VIEWDATA_CONTROLLERACTION, "GridViewInventoryViabilityData");
            datS.Tables.Add(dt.Copy());
            return datS;
        }
        #endregion

        #region private function
        private DataRow SetInvenotryViabilityData(DataRow dr, FormCollection formC)
        {
            DateTime now = DateTime.Now;
            dr["inventory_viability_id"] = formC["inventory_viability_id"];
            dr["replication_number"] = formC["replication_number"];
            dr["count_number"] = formC["count_number"];
            dr["count_date"] = now;
            dr = ExisteFormCollection(dr, formC, "normal_count", "int");
            dr = ExisteFormCollection(dr, formC, "abnormal_count", "int");
            dr = ExisteFormCollection(dr, formC, "dormant_count", "int");
            dr = ExisteFormCollection(dr, formC, "hard_count", "int");
            dr = ExisteFormCollection(dr, formC, "empty_count", "int");
            dr = ExisteFormCollection(dr, formC, "infested_count", "int");
            dr = ExisteFormCollection(dr, formC, "dead_count", "int");
            dr = ExisteFormCollection(dr, formC, "unknown_count", "int");
            dr = ExisteFormCollection(dr, formC, "note");
            dr = ExisteFormCollection(dr, formC, "counter_cooperator_id");
            return dr;
        }

        private DataTable CloseInvenotryViabilityData(DataTable inventoryViabilityDataTable, int numberOfReplicates
            , int seedsPerReplicate, DataTable inventoryViabilityTable)
        {
            List<DataRow> results = inventoryViabilityDataTable.Rows.Cast<DataRow>()
                       .Where(x => x.Field<int?>("normal_count") == 0 && x.Field<string>("note") == null
                       && x.Field<int?>("abnormal_count") == null && x.Field<int?>("dormant_count") == null
                       && x.Field<int?>("hard_count") == null && x.Field<int?>("empty_count") == null
                       && x.Field<int?>("infested_count") == null && x.Field<int?>("dead_count") == null
                       && x.Field<int?>("unknown_count") == null
                       ).ToList();
            int totalSeedCount = 0;
            if (results.Count == 0 && inventoryViabilityDataTable.Rows.Count == numberOfReplicates)
            {
                List<DataRow> resultsValid = inventoryViabilityDataTable.Rows.Cast<DataRow>()
                       .Where(x => x.Field<int?>("normal_count") > 0 || (
                       x.Field<int?>("abnormal_count") != null
                       || x.Field<int?>("dormant_count") != null || x.Field<int?>("hard_count") != null
                       || x.Field<int?>("empty_count") != null || x.Field<int?>("infested_count") != null
                       || x.Field<int?>("dead_count") != null || x.Field<int?>("unknown_count") != null)
                       ).ToList();
                int totalSeed = resultsValid.Count * seedsPerReplicate;
                int seedEmpty = 0;
                int seedNormal = 0;
                foreach (DataColumn col in inventoryViabilityDataTable.Columns)
                {
                    if (col.ColumnName.Contains("_count"))
                    {
                        int total = 0;
                        foreach (DataRow dr in inventoryViabilityDataTable.Rows)
                        {
                            if (!string.IsNullOrEmpty(dr[col].ToString()))
                            {
                                if (col.ColumnName.Contains("empty_count"))
                                {
                                    seedEmpty += Convert.ToInt32(dr[col].ToString());
                                }
                                if (col.ColumnName.Contains("normal_count"))
                                {
                                    seedNormal += Convert.ToInt32(dr[col].ToString());
                                }
                                total += Convert.ToInt32(dr[col].ToString());
                                totalSeedCount += Convert.ToInt32(dr[col].ToString());
                            }
                        }
                        if (total != 0)
                        {
                            string newColumnName = "percent_" + col.ColumnName.Replace("_count", "");
                            double dd = ((double)total / (double)totalSeed) * 100;
                            int percentaje = (int)dd;
                            inventoryViabilityTable.Rows[0][newColumnName] = percentaje;
                        }
                    }
                }
                string note = "";
                string percentNormal = inventoryViabilityTable.Rows[0]["percent_normal"].ToString() == "" ? "0" : inventoryViabilityTable.Rows[0]["percent_normal"].ToString();
                if (seedEmpty > 0)
                {
                    int percentageViable = (seedNormal / (totalSeedCount - seedEmpty)) * 100;
                    totalSeedCount -= seedEmpty;
                    percentNormal = Convert.ToString((int)percentageViable);
                    note += " Remove " + seedEmpty + " empty  by percentage viable ";
                }
                inventoryViabilityTable.Rows[0]["percent_viable"] = percentNormal;
                inventoryViabilityTable.Rows[0]["total_tested_count"] = totalSeedCount;
                inventoryViabilityTable.Rows[0]["replication_count"] = resultsValid.Count;
                if (numberOfReplicates > resultsValid.Count)
                {
                    int repetitionCanceled = numberOfReplicates - resultsValid.Count;
                    note += " " + repetitionCanceled + " repetitions were canceled ";
                }
                if (!string.IsNullOrEmpty(note))
                {
                    inventoryViabilityTable.Rows[0]["note"] = "NCA: " + note;
                }
                dataCollectionAction.Add("percent_viable", percentNormal);
                if (Convert.ToDecimal(percentNormal) <= 85)
                {
                    int inventory_id = Int32.Parse(inventoryViabilityTable.Rows[0]["inventory_id"].ToString());
                    DataTable inventoryTable = GetDataviewById(inventory_id.ToString(), hHelp.SYS_VIABILITYDATACOLLECTION_FIELDNAME_INVENTORY);
                    inventoryTable.Rows[0]["availability_status_code"] = "LOWGERM";
                    sopH.SaveData("", inventoryTable);
                }
                SaveActionClose();
            }
            return inventoryViabilityTable;
        }

        private void SaveActionClose()
        {
            DataTable inventory = GetDataviewById(dataCollectionAction["inventory_id"].ToString(), "sysInventory");
            DataTable inventoryAction = GetDataviewById(dataCollectionAction["inventory_id"].ToString(), "sysActionUpdate");
            DataviewElement inventoryAccionDVE = GetParameteresById(dataCollectionAction["inventory_id"].ToString(), "sysActionUpdate");
            XElement nodeAction = sopH.SetH.GetNodeAction(sopH.SetH.SYSTEM_ACTION_DATAVIEW, hHelp.SYS_DATAVIEWACTIONNAME_FINISHVIABILITY_INVENTORYACTION);
            XElement col = (from e in nodeAction.Descendants(sopH.SetH.SETTING_NAME_ACTIONVALUE) where e.Attribute(sopH.SetH.SETTING_NAME_NAME).Value.ToUpper() == "action_name_code".ToUpper() select e).DefaultIfEmpty(null).FirstOrDefault();
            string actionCondGroupValue = "";
            if (col != null)
            {
                actionCondGroupValue = col.Attribute(sopH.SetH.SETTING_NAME_VALUE).Value;
            }
            if (!string.IsNullOrEmpty(actionCondGroupValue))
            {
                string inventoryActionPrimaryKey = inventoryAction.PrimaryKey[0].ColumnName;
                DataRow inventoryActionRow = inventoryAction.Select("action_name_code ='" + actionCondGroupValue + "' AND completed_date is null", "started_date DESC").DefaultIfEmpty(null).FirstOrDefault();
                if (inventoryActionRow != null)
                {
                    string paramtersId = inventoryActionRow[inventoryActionPrimaryKey].ToString();
                    Dictionary<string, string> dic = inventoryAccionDVE.DictionaryParamtersOnly;
                    if (dic.ContainsKey(inventoryActionPrimaryKey.Replace("_", "")))
                    {
                        dic[inventoryActionPrimaryKey.Replace("_", "")] = paramtersId;
                    }
                    UpdateAction(sopH.Parameters_DictionaryToString(dic), hHelp.SYS_DATAVIEWACTIONNAME_FINISHVIABILITY_INVENTORYACTION, inventoryAccionDVE);
                }
            }
        }

        #endregion
        #region protected funcion
        protected DataRow ExisteFormCollection(DataRow dr, FormCollection formC, string id, string type = "")
        {
            if (formC[id] != null && !string.IsNullOrEmpty(formC[id].ToString()))
            {
                string data = formC[id];
                switch (type)
                {
                    case "int":
                        dr[id] = Convert.ToInt32(data);
                        break;
                    default:
                        dr[id] = data;
                        break;
                }
            }
            return dr;
        }

        protected Dictionary<string, object> AddDicctionary(Dictionary<string, object> dataJson, DataTable dt)
        {
            foreach (KeyValuePair<string, object> entry in CastDataTableToDictionary(dt))
            {
                dataJson.Add(entry.Key, entry.Value);
            }
            return dataJson;
        }

        protected Dictionary<string, object> CastDataTableToDictionary(DataTable dt)
        {
            Dictionary<string, object> tab = new Dictionary<string, object>();
            List<object> rows = new List<object>();
            foreach (DataRow dr in dt.Rows)
            {
                Dictionary<string, object> cels = new Dictionary<string, object>();
                foreach (DataColumn dc in dt.Columns)
                {
                    cels.Add(dc.ColumnName, dr[dc].ToString());
                }
                rows.Add(cels);
            }
            tab.Add(dt.TableName, rows);
            return tab;
        }
        #endregion
    }
}