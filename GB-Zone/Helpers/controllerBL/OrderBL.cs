﻿using GrinGlobal.Zone.Helpers.setting;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Xml.Linq;

namespace GrinGlobal.Zone.Helpers.controllerBL
{
    public class OrderBL
    {
        private IGrinGlobalSoapHelp sopH;
        private HtmlHelp hHelp;
        private int remainigElementToCheckCount = 0;

        /// <summary>
        /// Initial constructor to get the parent constructor
        /// </summary>
        /// <param name="sopH">Instancia form class GrinGlobalSoapHelp, for get the setting fiel and service GirnGlobal SOAP </param>
        public OrderBL(IGrinGlobalSoapHelp sopH)
        {
            this.sopH = sopH;
            hHelp = new HtmlHelp();
        }
        #region public methods
        /// <summary>
        /// Add the history action to the dataset
        /// </summary>
        /// <param name="datS">DataSet originate to add history</param>
        /// <returns>Dataset with history</returns>
        public DataSet AddHistoryAction(DataSet datS)
        {
            remainigElementToCheckCount = 0;
            string keyColumn = datS.Tables[sopH.SetH.DataViewName].PrimaryKey[0].ColumnName;
            XElement nodeAction = sopH.SetH.GetNodeAction(sopH.SetH.SYSTEM_ACTION_DATAVIEW, hHelp.SYS_ORDER_DATAVIEWACTIONNAME_ORDER_REQUEST_ITEM_ACTION);
            string idAction = nodeAction.Attribute(sopH.SetH.SETTING_NAME_GENERIC_ID).Value != null ? nodeAction.Attribute(sopH.SetH.SETTING_NAME_GENERIC_ID).Value.ToString() : "";
            string dataViewName = nodeAction.Element(sopH.SetH.SETTING_NAME_PARAMETERS).Element(sopH.SetH.SETTING_NAME_DATAVIEW).Value != null ? nodeAction.Element(sopH.SetH.SETTING_NAME_PARAMETERS).Element(sopH.SetH.SETTING_NAME_DATAVIEW).Value.ToString() : "";
            XElement dataviewActionvalue = sopH.SetH.GetDataviewValueVariable(hHelp.SYS_ORDER_DATAVIEWACTIONVALUE_CHECK_HISTORY_ACTION);
            string field = dataviewActionvalue.Attribute(sopH.SetH.SETTING_NAME_NAME).Value.ToString(); //action_name_code
            string value = dataviewActionvalue.Attribute(sopH.SetH.SETTING_NAME_VALUE).Value.ToString();
            var query = from order in datS.Tables[dataViewName].AsEnumerable()
                        where order.Field<string>(field) == value
                        select order;
            int max = 1;
            if (query.Count() > 0)
            {
                for (int i = 0; i < datS.Tables[sopH.SetH.DataViewName].Rows.Count; i++)
                {
                    int orderRequesItemId = Int32.Parse(datS.Tables[sopH.SetH.DataViewName].Rows[i][keyColumn].ToString());
                    var cell = (from t in query.AsEnumerable() where t.Field<int>(keyColumn) == orderRequesItemId && t.Field<string>(field) == value select t);
                    if (cell.Count() != 0)
                    {
                        datS.Tables[sopH.SetH.DataViewName].Rows[i][sopH.SetH.GetColumnVariable(hHelp.SYS_ORDER_COLUMNNAME_CHECK_BEFORE)] = true;
                        remainigElementToCheckCount++;
                        max++;
                    }
                }
            }

            return datS;
        }

        /// <summary>
        /// Get the dataset with the specific action
        /// </summary>
        /// <param name="parameter">Parameters to search in SOAP Grin Global</param>
        /// <returns>Dataset with the spefific action </returns>
        public DataSet GetDataSetAction(string parameter)
        {
            DataSet datS = sopH.GetData(parameter);
            XElement nodeAction = sopH.SetH.GetNodeAction(sopH.SetH.SYSTEM_ACTION_DATAVIEW, hHelp.SYS_ORDER_DATAVIEWACTIONNAME_ORDER_REQUEST_ITEM_ACTION);
            string idAction = nodeAction.Attribute(sopH.SetH.SETTING_NAME_GENERIC_ID).Value != null ? nodeAction.Attribute(sopH.SetH.SETTING_NAME_GENERIC_ID).Value.ToString() : "";
            string dataViewName = nodeAction.Element(sopH.SetH.SETTING_NAME_PARAMETERS).Element(sopH.SetH.SETTING_NAME_DATAVIEW).Value != null ? nodeAction.Element(sopH.SetH.SETTING_NAME_PARAMETERS).Element(sopH.SetH.SETTING_NAME_DATAVIEW).Value.ToString() : "";
            if (datS.Tables.Contains(dataViewName))
            {
                datS.Tables.Remove(dataViewName);
            }
            datS.Tables.Add(sopH.GetDataActionOne(parameter, new Dictionary<string, string>(), idAction));
            datS = AddHistoryAction(datS);
            return datS;
        }
        #endregion
    }
}