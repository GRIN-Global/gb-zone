﻿using GrinGlobal.Zone.Helpers.setting;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Xml.Linq;

namespace GrinGlobal.Zone.Controllers
{
    public class ViabilityController : CoreController
    {
        private string sysFieldIncrementYear;
        private string sysFieldPupulationType;
        private string sysFieldReproductiveUniform;
        private string sysFieldInventoryNumber3;
        private string sysFieldInventoryId;
        private string sysFieldTotal;
        private string sysFieldPreload;
        public ViabilityController() : base()
        {

        }
        // GET: Viability
        public ActionResult Index(string moduleId, string formId)
        {
            Init(moduleId, formId);
            DataSet ds = DataInit(moduleId, formId, new DataSet());

            return View(ds);
        }
        [HttpPost]
        public JsonResult JsonData(FormCollection formCollection)
        {
            string jsonResult = "";
            string serverId = Session[hHelp.SYS_SESSION_SERVER].ToString();
            string moduleId = TempData[hHelp.SYS_VIEWDATA_MODULEID].ToString();
            string formId = TempData[hHelp.SYS_VIEWDATA_FORMID].ToString();
            Init();
            setH = new SettingsHelp(serverId, moduleId, formId, pathSetting);
            GetNameField();
            XElement fieldNode = null;
            if (formCollection[sysFieldIncrementYear] != null)
            {
                fieldNode = setH.GetField_AtributeValue(hHelp.SYS_VIABILITY_FIELDNAME_INCREMENTYEAR);
            }
            if (fieldNode != null)
            {
                string dataviewName = setH.GetDataviewName_Parameter(fieldNode);
                Dictionary<string, string> parameter = sopH.GetParameters(formCollection, dataviewName, setH.GetDefaultFields_XElemnt(fieldNode));
                DataSet dS = sopH.GetData(sopH.Parameters_DictionaryToString(parameter), dataviewName);

                //sopH.SetH.GetNodeViabilityRule(serverId, "Advanced/Improved cultivar", "Pure line", "ACTIVE");
                jsonResult = gvH.DataTableToJsonWithJavaScriptSerializer(dS.Tables[dataviewName], setH.GlobalMaxJsonLength);
            }
            else
            {
                throw new ArgumentNullException("The search parameter does not exist in the Settings.xml file");
            }
            var objJson = Json(jsonResult, JsonRequestBehavior.AllowGet);
            objJson.MaxJsonLength = setH.GlobalMaxJsonLength;
            return objJson;
        }
        [HttpPost]
        public JsonResult JsonDataPreload(FormCollection formCollection)
        {
            string jsonResult = "";
            Init();
            setH = new SettingsHelp(serverId, moduleId, formId, pathSetting);
            GetNameField();
            XElement fieldNode = null;
            fieldNode = setH.GetField_AtributeValue(hHelp.SYS_VIABILITY_FIELDNAME_PRELOAD);
            if (fieldNode != null)
            {
                string dataviewName = setH.GetDataviewName_Parameter(fieldNode);
                Dictionary<string, string> parameter = sopH.GetParameters(formCollection, dataviewName, setH.GetDefaultFields_XElemnt(fieldNode));
                DataSet dS = sopH.GetData(sopH.Parameters_DictionaryToString(parameter), dataviewName);
                jsonResult = gvH.DataTableToJsonWithJavaScriptSerializer(dS.Tables[dataviewName], setH.GlobalMaxJsonLength);
            }
            else
            {
                throw new ArgumentNullException("The search parameter does not exist in the Settings.xml file");
            }
            var objJson = Json(jsonResult, JsonRequestBehavior.AllowGet);
            objJson.MaxJsonLength = setH.GlobalMaxJsonLength;
            return objJson;
        }

        [HttpPost]
        public ActionResult Index(FormCollection formCollection)
        {
            Init();
            GetNameField();
            DataSet datS = new DataSet();
            try
            {
                Dictionary<string, string> parameters = sopH.GetParameters(formCollection);
                string parameterss = sopH.Parameters_DictionaryToString(parameters);
                datS = sopH.GetData(parameterss);
                ViewData[hHelp.SYS_VIEWDATA_DATATABLE] = sopH.SetH.DataViewName;
                ViewData[hHelp.SYS_VIEWDATA_GRIDRESULT] = hHelp.SYS_RAZO_GENERICGRIDVIEWSEARCH;
                ViewData[hHelp.SYS_VIEWDATA_PARAMETERS] = parameterss;
            }
            catch (Exception ex)
            {
                ViewData[hHelp.SYS_VIEWDATA_MESSAGEERROR] = ex.Message;
            }
            datS = DataInit(moduleId, formId, datS);
            return View(datS);

        }

        public ActionResult GridView(string parameters)
        {
            Init();
            DataSet datS = new DataSet();
            try
            {
                datS = sopH.GetData(parameters);
                ViewData[hHelp.SYS_VIEWDATA_DATATABLE] = sopH.SetH.DataViewName;
                ViewData[hHelp.SYS_VIEWDATA_GRIDRESULT] = hHelp.SYS_RAZO_GENERICGRIDVIEWSEARCH;
                ViewData[hHelp.SYS_VIEWDATA_PARAMETERS] = parameters;
            }
            catch (Exception ex)
            {
                ViewData[hHelp.SYS_VIEWDATA_MESSAGEERROR] = ex.Message;
            }
            datS = DataInit(moduleId, formId, datS);
            ViewData[hHelp.SYS_VIEWDATA_FORMURL] = "Order";
            return View(datS);
        }



        protected override DataSet DataInit(string moduleId, string formId, DataSet datS)
        {
            setH = new SettingsHelp(Session[hHelp.SYS_SESSION_SERVER].ToString(), moduleId, formId, pathSetting);
            ViewBag.MenuTitle = "Viability";
            ViewBag.MenuTitleInfo = "Create viability orders";
            ViewBag.SearchTitle = "Filter the search";
            ViewData[hHelp.SYS_VIABILITY_FIELDNAME_INCREMENTYEAR] = sysFieldIncrementYear;
            ViewData[hHelp.SYS_VIABILITY_FIELDNAME_IMPROVEMENTSTATUS] = sysFieldPupulationType;
            ViewData[hHelp.SYS_VIABILITY_FIELDNAME_REPRODUCTIVEUNIFORM] = sysFieldReproductiveUniform;
            ViewData[hHelp.SYS_VIABILITY_FIELDNAME_INVENTORYID] = sysFieldInventoryId;
            ViewData[hHelp.SYS_VIABILITY_FIELDNAME_TOTAL] = sysFieldTotal;
            ViewData[hHelp.SYS_VIABILITY_FIELDNAME_INVENTORYNUMBERPART3] = sysFieldInventoryNumber3;
            ViewData[hHelp.SYS_VIEWDATA_MODULEID] = moduleId;
            ViewData[hHelp.SYS_VIEWDATA_FORMURL] = "Index";
            ViewData[hHelp.SYS_VIEWDATA_FORMCONTROLLER] = "Viability";
            ViewData[hHelp.SYS_VIEWDATA_FORMIDHTML] = "formFilter";
            ViewData["success"] = "100";
            DataTable dt = setH.GetInputs();
            datS.Tables.Add(dt);
            return datS;
        }

        private void GetNameField()
        {
            try
            {
                sysFieldIncrementYear = setH.GetField_AtributeValue(hHelp.SYS_VIABILITY_FIELDNAME_INCREMENTYEAR).Attribute(setH.SETTING_NAME_GENERIC_ID).Value;
                sysFieldInventoryId = setH.GetField_AtributeValue(hHelp.SYS_VIABILITY_FIELDNAME_INVENTORYID).Attribute(setH.SETTING_NAME_GENERIC_ID).Value;
                sysFieldPupulationType = setH.GetField_AtributeValue(hHelp.SYS_VIABILITY_FIELDNAME_IMPROVEMENTSTATUS).Attribute(setH.SETTING_NAME_GENERIC_ID).Value;
                sysFieldReproductiveUniform = setH.GetField_AtributeValue(hHelp.SYS_VIABILITY_FIELDNAME_REPRODUCTIVEUNIFORM).Attribute(setH.SETTING_NAME_GENERIC_ID).Value;
                sysFieldInventoryNumber3 = setH.GetField_AtributeValue(hHelp.SYS_VIABILITY_FIELDNAME_INVENTORYNUMBERPART3).Attribute(setH.SETTING_NAME_GENERIC_ID).Value;
                sysFieldTotal = setH.GetField_AtributeValue(hHelp.SYS_VIABILITY_FIELDNAME_TOTAL).Attribute(setH.SETTING_NAME_GENERIC_ID).Value;
                sysFieldPreload = setH.GetField_AtributeValue(hHelp.SYS_VIABILITY_FIELDNAME_PRELOAD).Attribute(setH.SETTING_NAME_GENERIC_ID).Value;
            }
            catch (Exception)
            {
                throw new System.ArgumentNullException("The configuration file setting.xls does not have the configuration of the fields");
            }

        }

    }
}