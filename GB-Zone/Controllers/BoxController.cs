﻿using DevExpress.Web.Mvc;
using GrinGlobal.Zone.Classes;
using GrinGlobal.Zone.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data;
using GrinGlobal.Zone.Helpers;
using GrinGlobal.Zone.Helpers.setting;
using System.Xml.Linq;
using GrinGlobal.Zone.Helpers.controllerBL;

namespace GrinGlobal.Zone.Controllers
{
    public class BoxController : CoreController
    {
        #region public attribute

        #endregion
        #region private attribute
        private readonly string SYS_DATAVIEW_RESPONSE = "_GridViewSearch";

        #endregion

        public BoxController()
        {

        }
        /// <summary>
        /// First access to the Box controller 
        /// </summary>
        /// <param name="moduleId">Id module from settings.xml</param>
        /// <param name="formId">Id form from settings.xml</param>
        /// <returns></returns>
        public ActionResult Index(string moduleId, string formId)
        {
            DataSet ds = DataInit(moduleId, formId, new DataSet());
            Init(moduleId, formId);
            ViewData[hHelp.SYS_VIEWDATA_SUCCESS] = "100";
            return View(ds);
        }

        /// <summary>
        /// Send paramterter to find the box (storage location)
        /// </summary>
        /// <param name="formdata">Form data from web</param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult Index(FormCollection formdata)
        {
            Init();
            ViewData[hHelp.SYS_VIEWDATA_SUCCESS] = "100";


            DataSet datS = new DataSet();
            try
            {
                BoxBL bl = new BoxBL(sopH);
                Dictionary<string, string> parameter = sopH.GetParameters(formdata);
                string inventory = sopH.SetH.GetColumnVariable(hHelp.SYS_BOX_COLUMNNAME_INVENTORY_GETDATA);
                ViewData[hHelp.SYS_BOX_DATAVIEW_STORAGELOCATION] = parameter[inventory];
                ViewData[hHelp.SYS_BOX_COLUMNNAME_INVENTORY_GETDATA] = sopH.SetH.GetColumnVariable(hHelp.SYS_BOX_COLUMNNAME_INVENTORY_GETDATA);
                ViewData[hHelp.SYS_BOX_COLUMNNAME_STORAGELOCATION4] = sopH.SetH.GetColumnVariable(hHelp.SYS_BOX_COLUMNNAME_STORAGELOCATION4);
                parameter = bl.CastParameter(parameter);
                ViewData[hHelp.SYS_VIEWDATA_DATATABLE] = sopH.SetH.DataViewName;
                ViewData[hHelp.SYS_VIEWDATA_PARAMETERS] = sopH.Parameters_DictionaryToString(parameter);
                datS = sopH.GetData(ViewData[hHelp.SYS_VIEWDATA_PARAMETERS].ToString());
                ViewData[hHelp.SYS_BOX_JSON_ITEMINBOX] = gvH.DataTableToJsonWithJavaScriptSerializer(datS.Tables[sopH.SetH.DataViewName], sopH.SetH.GlobalMaxJsonLength);
            }
            catch (Exception ex)
            {
                ViewData[hHelp.SYS_VIEWDATA_MESSAGEERROR] = ex.Message;
            }
            datS = DataInit(moduleId, formId, datS);
            return View(datS);
        }
        /// <summary>
        /// To update the grid by order or group
        /// </summary>
        /// <param name="parameter">parameters to search in text string format</param>
        /// <returns></returns>
        public ActionResult GridView(string parameter)
        {
            Init();
            ViewData[hHelp.SYS_VIEWDATA_SUCCESS] = "100";
            DataSet datS = new DataSet();
            try
            {
                ViewData[hHelp.SYS_BOX_COLUMNNAME_INVENTORY_GETDATA] = sopH.SetH.GetColumnVariable(hHelp.SYS_BOX_COLUMNNAME_INVENTORY_GETDATA);
                ViewData[hHelp.SYS_BOX_COLUMNNAME_STORAGELOCATION4] = sopH.SetH.GetColumnVariable(hHelp.SYS_BOX_COLUMNNAME_STORAGELOCATION4);
                ViewData[hHelp.SYS_VIEWDATA_DATATABLE] = sopH.SetH.DataViewName;
                ViewData[hHelp.SYS_VIEWDATA_PARAMETERS] = parameter;
                datS = sopH.GetData(parameter);
            }
            catch (Exception ex)
            {
                ViewData[hHelp.SYS_VIEWDATA_MESSAGEERROR] = ex.Message;
            }
            datS = DataInit(moduleId, formId, datS);
            return PartialView(SYS_DATAVIEW_RESPONSE, datS);
        }
        /// <summary>
        /// Batching update for the grid
        /// </summary>
        /// <param name="parameters">parameters to search in text string format</param>
        /// <returns></returns>
        public ActionResult BatchUpdateAction(string parameters)
        {
            Init();
            ViewData[hHelp.SYS_VIEWDATA_PARAMETERS] = parameters;
            DataTable originDT = new DataTable();
            DataTable dt = new DataTable();
            DataTable updateDT = new DataTable();
            DataTable insertDT = new DataTable();
            DataSet saveDS = new DataSet();
            try
            {
                originDT = sopH.GetData(parameters).Tables[sopH.SetH.DataViewName];
                dt = originDT.Copy();
                ViewData[hHelp.SYS_BOX_COLUMNNAME_INVENTORY_GETDATA] = sopH.SetH.GetColumnVariable(hHelp.SYS_BOX_COLUMNNAME_INVENTORY_GETDATA);
                ViewData[hHelp.SYS_BOX_COLUMNNAME_STORAGELOCATION4] = sopH.SetH.GetColumnVariable(hHelp.SYS_BOX_COLUMNNAME_STORAGELOCATION4);
                ViewData[hHelp.SYS_VIEWDATA_DATATABLE] = sopH.SetH.DataViewName;
                insertDT = GetOnlyInsertData(dt.Copy());
                updateDT = GetOnlyUpdateData(dt.Copy());
                dt = GetNewAndUpdateData(dt);

                BoxBL bl = new BoxBL(sopH);
                saveDS = bl.Add(dt
                    , parameters
                    , sopH.SetH.GetColumnVariable(hHelp.SYS_BOX_COLUMNNAME_INVENTORY_GETDATA)
                    , bl.GetColumnSave()
                    , bl.GetAllItemToSplit()
                );
                originDT = sopH.GetData(parameters).Tables[sopH.SetH.DataViewName];
                bl.SaveAction(originDT, parameters, hHelp.SYS_DATAVIEWACTIONNAME_GET_INVENTORYACTION);
                ViewData[hHelp.SYS_VIEWDATA_SUCCESS] = "200";
            }
            catch (Exception e)
            {
                ProcessError(e, insertDT, updateDT);
            }
            if (saveDS.Tables.Contains(sopH.SetH.DataViewName))
            {
                saveDS.Tables.Remove(sopH.SetH.DataViewName);
            }
            saveDS.Tables.Add(originDT.Copy());
            saveDS = DataInit(moduleId, formId, saveDS);
            ViewData[hHelp.SYS_BOX_JSON_ITEMINBOX] = gvH.DataTableToJsonWithJavaScriptSerializer(saveDS.Tables[sopH.SetH.DataViewName], sopH.SetH.GlobalMaxJsonLength);
            return PartialView(SYS_DATAVIEW_RESPONSE, saveDS);
        }

        private void ProcessError(Exception e, DataTable insertDT, DataTable updateDT)
        {
            ViewData[hHelp.SYS_VIEWDATA_SUCCESS] = 501;
            Guid d = Guid.NewGuid();
            log.Fatal(Guid.NewGuid(), e);
            string[] spearator = { ":line", "--->" };
            string[] lineas = e.Message.Split(spearator, StringSplitOptions.RemoveEmptyEntries);
            string error = "";
            if (lineas.Count() > 1)
            {
                error = lineas[1];
            }
            if (string.IsNullOrEmpty(error))
            {
                error = e.Message;
                spearator = new string[] { "|@" };
                lineas = e.Message.Split(spearator, StringSplitOptions.RemoveEmptyEntries);
                if (lineas.Count() == 2)
                {
                    string[] errorData = lineas[1].Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries);
                    ViewData[hHelp.SYS_JSON_ERRORDATA] = gvH.ArrayToJsonWithJavaScriptSerializer(errorData, sopH.SetH.GlobalMaxJsonLength);
                    error = lineas[0] + " : " + lineas[1];
                }
            }
            ViewData[hHelp.SYS_JSON_INSERTDATA] = gvH.DataTableToJsonWithJavaScriptSerializer(insertDT, sopH.SetH.GlobalMaxJsonLength);
            ViewData[hHelp.SYS_JSON_UPDATEDATA] = gvH.DataTableToJsonWithJavaScriptSerializer(updateDT, sopH.SetH.GlobalMaxJsonLength);
            ViewData[hHelp.SYS_VIEWDATA_MESSAGEERROR] = error;
        }

        protected override DataSet DataInit(string moduleId, string formId, DataSet datS)
        {
            ViewData[hHelp.SYS_VIEWDATA_MODULEID] = moduleId;
            ViewData[hHelp.SYS_VIEWDATA_FORMID] = formId;
            ViewData[hHelp.SYS_VIEWDATA_FORMURL] = "Index";
            ViewData[hHelp.SYS_VIEWDATA_FORMCONTROLLER] = "Box";
            ViewData[hHelp.SYS_VIEWDATA_FORMIDHTML] = "formFilter";
            ViewData[hHelp.SYS_VIEWDATA_TITLEMENU] = "Boxes";
            ViewBag.MenuTitle = "STORAGE";
            ViewBag.MenuTitleInfo = "Move and reorder the item";
            ViewBag.searchTitle = "Search";
            setH = new SettingsHelp(Session[hHelp.SYS_SESSION_SERVER].ToString(), moduleId, formId, pathSetting);
            datS.Tables.Add(setH.GetInputs());
            return datS;
        }
    }
}