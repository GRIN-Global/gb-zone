﻿using GrinGlobal.Zone.Helpers.controllerBL;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Xml.Linq;

namespace GrinGlobal.Zone.Controllers
{
    public class ViabilityDataCollectionController : CoreController
    {
        private ViabilityDataCollectionBL bl;

        /// <summary>
        /// Constructor to load parent constructor
        /// </summary>
        public ViabilityDataCollectionController() : base()
        {

        }
        #region public function
        /// <summary>
        /// First step to create the module for Viability Data Collection
        /// </summary>
        /// <param name="moduleId">Id module from settings.xml</param>
        /// <param name="formId">Id form from settings.xml</param>
        /// <returns>Page viability Data Collection</returns>
        // GET: ViabilityDataCollection
        public ActionResult Index(string moduleId, string formId)
        {
            Init(moduleId, formId);
            DataSet ds = DataInit(moduleId, formId, new DataSet());
            return View(ds);
        }

        /// <summary>
        /// Serach from Inventory Viability, form create or update inventory viability data
        /// </summary>
        /// <param name="formC">Form data from web</param>
        /// <returns>Json with the information from experiment</returns>
        [HttpPost]
        public JsonResult SearchInventory(FormCollection formC)
        {
            Init();
            DataSet datS = new DataSet();
            bl = new ViabilityDataCollectionBL(sopH);
            try
            {
                jResponse.DataJson = bl.AddDictionaryAll(formC, new Dictionary<string, object>());
                jResponse.Success = 200;
            }
            catch (Exception ex)
            {
                jResponse.MenssageError = ex.Message;
            }
            return jResponse.GetJsonResult(sopH.SetH.GlobalMaxJsonLength);
        }

        /// <summary>
        /// Save the data, form invneotry viability data and testd_date of inventory viability
        /// </summary>
        /// <param name="formC">Form data from web</param>
        /// <returns></returns>
        [HttpPost]
        public JsonResult Save(FormCollection formC)
        {
            Init();
            DataSet datS = new DataSet();
            bl = new ViabilityDataCollectionBL(sopH);
            try
            {
                bl.Save(formC);
                jResponse.Success = 200;
            }
            catch (Exception ex)
            {
                jResponse.Success = 500;
                jResponse.MenssageError = ex.Message;
            }
            return jResponse.GetJsonResult(sopH.SetH.GlobalMaxJsonLength);
        }
        /// <summary>
        /// Obtain the grid with information on all the viability inventory data  from the inventory identifier of the experiment.
        /// </summary>
        /// <param name="formC">Form data from web</param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult GridViewInventoryViabilityData(FormCollection formC)
        {
            Init();
            DataSet datS = new DataSet();
            bl = new ViabilityDataCollectionBL(sopH);
            try
            {
                datS = bl.GridViewInventoryViabilityData(formC);
            }
            catch (Exception ex)
            {
                ViewData[hHelp.SYS_VIEWDATA_EDIT_ERROR] = ex.Message;
            }
            SetViewData(bl.DicDataView);
            return PartialView("_GridReadOnlyPartitial", datS);
        }

        /// <summary>
        /// Obtain the grid with information on all hisotry from the viability inventory.
        /// </summary>
        /// <param name="formC">Form data from web</param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult GridViewInventoryViability(FormCollection formC)
        {
            Init();
            DataSet datS = new DataSet();
            bl = new ViabilityDataCollectionBL(sopH);
            try
            {
                datS = bl.GridViewInventoryViability(formC);
            }
            catch (Exception ex)
            {
                ViewData[hHelp.SYS_VIEWDATA_EDIT_ERROR] = ex.Message;
            }
            SetViewData(bl.DicDataView);
            return PartialView("_GridReadOnlyPartitial", datS);
        }
        [HttpPost]
        public JsonResult ValidateCooperateId(FormCollection formC)
        {
            Init();
            DataSet datS = new DataSet();
            bl = new ViabilityDataCollectionBL(sopH);
            try
            {
                DataRow cooperatorRow = bl.ValidateCooperatorId(formC);
                if (cooperatorRow != null)
                {
                    jResponse.DataJson = new Dictionary<string, object>()
                    {
                        {"cooperator", cooperatorRow },
                    };
                    jResponse.Success = 200;
                }
                else
                {
                    jResponse.Success = 400;
                }
            }
            catch (Exception ex)
            {
                jResponse.Success = 500;
                jResponse.MenssageError = ex.Message;
            }
            return jResponse.GetJsonResult(sopH.SetH.GlobalMaxJsonLength);
        }
        #endregion

        #region protected function
        protected override DataSet DataInit(string moduleId, string formId, DataSet datS)
        {
            base.DataInit(moduleId, formId, datS);
            XElement node = sopH.SetH.GetField_AtributeValue(hHelp.SYS_VIABILITYDATACOLLECTION_FIELDNAME_SCANDQR);
            ViewData[hHelp.SYS_VIABILITYDATACOLLECTION_FIELDNAME_SCANDQR] = node.Attribute(sopH.SetH.SETTING_NAME_GENERIC_ID).Value;
            ViewBag.MenuTitle = "Viability";
            ViewBag.MenuTitleInfo = "Viability data collection";
            ViewBag.SearchTitle = "Scan inventory_viability_data_id to set the test information";
            return datS;
        }
        #endregion
    }
}