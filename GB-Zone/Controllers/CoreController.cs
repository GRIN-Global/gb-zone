﻿using DevExpress.Web.Mvc;
using GrinGlobal.Zone.Helpers;
using GrinGlobal.Zone.Helpers.setting;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace GrinGlobal.Zone.Controllers
{
    public class CoreController : Controller
    {
        protected HtmlHelp hHelp;
        protected static readonly log4net.ILog log = log4net.LogManager.GetLogger(typeof(SearchController));
        protected GridViewHelp gvH;
        protected string pathSetting = "";
        protected IGrinGlobalSoapHelp sopH;
        protected SettingsHelp setH;
        protected string serverId;
        protected string moduleId;
        protected string formId;
        protected JsonResponse jResponse;

        /// <summary>
        /// Constructor that initializes all controllers and instances the HtmlHelp GridviewHelper and Jsonresponse Class
        /// </summary>
        public CoreController()
        {
            hHelp = new HtmlHelp();
            gvH = new GridViewHelp();
            jResponse = new JsonResponse();
        }
        #region private function
        private void InitGrinGlobalSoap()
        {
            if (sopH == null)
            {
                sopH = new GrinGlobalSoapHelp(serverId, moduleId, formId, pathSetting);
            }
        }

        private void InitialSteupModuleFormServer(string _moduleId = "", string _formId = "")
        {
            serverId = Session[hHelp.SYS_SESSION_SERVER].ToString();
            ViewData["success"] = "100";
            if (!string.IsNullOrEmpty(_moduleId) && !string.IsNullOrEmpty(_formId))
            {
                TempData[hHelp.SYS_VIEWDATA_MODULEID] = _moduleId;
                TempData[hHelp.SYS_VIEWDATA_FORMID] = _formId;
                moduleId = _moduleId;
                formId = _formId;
            }
            else
            {
                moduleId = TempData[hHelp.SYS_VIEWDATA_MODULEID].ToString();
                formId = TempData[hHelp.SYS_VIEWDATA_FORMID].ToString();
                TempData[hHelp.SYS_VIEWDATA_MODULEID] = moduleId;
                TempData[hHelp.SYS_VIEWDATA_FORMID] = formId;
            }
        }
        #endregion
        #region protected function
        protected void Init(string _moduleId, string _formId)
        {
            InitialSteupModuleFormServer(_moduleId, _formId);
            InitGrinGlobalSoap();
        }

        protected void Init()
        {
            InitialSteupModuleFormServer();
            InitGrinGlobalSoap();
        }

        protected DataTable GetNewAndUpdateData(DataTable dt)
        {
            string columnKey = dt.PrimaryKey[0].ColumnName;
            foreach (DataColumn column in dt.Columns)
            {
                if (column.ReadOnly == false)
                {
                    var newValues = GridViewExtension.GetBatchUpdateValues<string, string>(column.ColumnName); // S is key field type, T is the column type
                    if (newValues != null && columnKey != column.ColumnName)
                    {
                        foreach (var item in newValues)
                        {
                            dt = gvH.AddRow(column.ColumnName, columnKey, item.Key, item.Value, dt);
                        }
                    }
                    List<string> insertValues = GridViewExtension.GetBatchInsertValues<string>(column.ColumnName);
                    if (insertValues != null && insertValues.Count > 0)
                    {
                        int index = -1;
                        foreach (string insertV in insertValues.Where(s => !string.IsNullOrEmpty(s)).ToList())
                        {
                            dt = gvH.AddRow(column.ColumnName, columnKey, index.ToString(), insertV, dt);
                            index--;
                        }
                    }
                }
            }
            return dt;
        }

        protected DataTable GetOnlyInsertData(DataTable dt)
        {
            dt.Clear();
            string columnKey = dt.PrimaryKey[0].ColumnName;
            foreach (DataColumn column in dt.Columns)
            {
                if (column.ReadOnly == false)
                {
                    List<string> insertValues = GridViewExtension.GetBatchInsertValues<string>(column.ColumnName);
                    if (insertValues != null && insertValues.Count > 0)
                    {
                        int index = -1;
                        foreach (string insertV in insertValues.Where(s => !string.IsNullOrEmpty(s)).ToList())
                        {
                            dt = gvH.AddRow(column.ColumnName, columnKey, index.ToString(), insertV, dt);
                            index--;
                        }
                    }
                }
            }
            return dt;
        }

        protected DataTable GetOnlyUpdateData(DataTable dt)
        {
            dt.Clear();
            string columnKey = dt.PrimaryKey[0].ColumnName;
            foreach (DataColumn column in dt.Columns)
            {
                if (column.ReadOnly == false)
                {
                    var newValues = GridViewExtension.GetBatchUpdateValues<string, string>(column.ColumnName); // S is key field type, T is the column type
                    if (newValues != null && columnKey != column.ColumnName)
                    {
                        foreach (var item in newValues)
                        {
                            dt = gvH.AddRow(column.ColumnName, columnKey, item.Key, item.Value, dt);
                        }
                    }
                }
            }
            return dt;
        }

        protected virtual DataSet DataInit(string moduleId, string formId, DataSet datS)
        {
            setH = new SettingsHelp(Session[hHelp.SYS_SESSION_SERVER].ToString(), moduleId, formId, pathSetting);
            ViewBag.MenuTitle = "Titulo";
            ViewBag.MenuTitleInfo = "Menu title info";
            ViewBag.SearchTitle = "Search title";
            ViewData[hHelp.SYS_VIEWDATA_MODULEID] = moduleId;
            ViewData[hHelp.SYS_VIEWDATA_FORMURL] = "FORM_URL";
            ViewData[hHelp.SYS_VIEWDATA_FORMCONTROLLER] = "Controller";
            ViewData[hHelp.SYS_VIEWDATA_FORMIDHTML] = "idFormGeneral";
            DataTable dt = setH.GetInputs();
            datS.Tables.Add(dt);
            return datS;
        }

        protected void SetViewData(Dictionary<string, string> dic, JsonResponse json = null)
        {
            if (json != null && json.Success != 0)
            {
                ViewData[hHelp.SYS_VIEWDATA_SUCCESS] = json.Success;
                ViewData[hHelp.SYS_VIEWDATA_EDIT_ERROR] = json.MenssageError;
            }
            foreach (var item in dic)
            {
                ViewData[item.Key] = item.Value;
            }
        }
        #endregion
        #region test
        public void InjectDependencyTest(string pathSetting)
        {
            this.pathSetting = pathSetting;
        }

        public void InjectDependencyTest(IGrinGlobalSoapHelp ggSoapH)
        {
            this.sopH = ggSoapH;
        }
        #endregion
    }

    /// <summary>
    /// Save and use the basic data for json to return from the controller
    /// </summary>
    public class JsonResponse
    {
        private Dictionary<string, object> dataJson;
        private int success;
        private string menssageError;

        /// <summary>
        /// The all response data in json
        /// </summary>
        public Dictionary<string, object> DataJson
        {
            set { dataJson = value; }
            get { return dataJson; }
        }

        /// <summary>
        /// Success from response
        /// </summary>
        public int Success
        {
            set { success = value; }
            get { return success; }
        }

        /// <summary>
        /// Message the error from aplication
        /// </summary>
        public string MenssageError
        {
            set { menssageError = value; }
            get { return menssageError; }
        }

        /// <summary>
        /// Initialize success at 100 and empty error message
        /// </summary>
        public JsonResponse()
        {
            success = 100;
            menssageError = "";
            dataJson = new Dictionary<string, object>();
        }

        /// <summary>
        /// Return the json response to the process by Microsoft C # controller
        /// </summary>
        /// <param name="maxLength">Max the caracter to convert in json</param>
        /// <returns>The json to convert in response</returns>
        public JsonResult GetJsonResult(int maxLength)
        {
            Dictionary<string, object> json = new Dictionary<string, object>();
            json.Add("Success", success);
            json.Add("MenssageError", menssageError);
            json.Add("Data", dataJson);
            string jsonString = JsonConvert.SerializeObject(json);
            JsonResult objJson = new JsonResult() { Data = jsonString };
            objJson.MaxJsonLength = maxLength;
            return objJson;
        }
    }
}