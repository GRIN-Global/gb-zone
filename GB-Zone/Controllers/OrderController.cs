﻿using DevExpress.Web.Mvc;
using GrinGlobal.Zone.Classes;
using GrinGlobal.Zone.Helpers;
using GrinGlobal.Zone.Helpers.controllerBL;
using GrinGlobal.Zone.Helpers.setting;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Xml.Linq;

namespace GrinGlobal.Zone.Controllers
{
    public class OrderController : CoreController
    {

        /// <summary>
        /// Create the first page for Order Check module
        /// </summary>
        /// <param name="moduleId">Id module from settings.xml</param>
        /// <param name="formId">Id form from settings.xml</param>
        /// <returns></returns>
        public ActionResult Index(string moduleId, string formId)
        {
            Init(moduleId, formId);
            ViewData[hHelp.SYS_VIEWDATA_DATATABLE] = sopH.SetH.DataViewName;
            DataSet ds = DataInit(moduleId, formId, new DataSet());
            return View(ds);
        }
        #region public function
        /// <summary>
        /// Find the order and create the page to work it
        /// </summary>
        /// <param name="formdata">Form data from web [order_request_id]</param>
        /// <returns>The pages to work the order</returns>
        [HttpPost]
        public ActionResult Index(FormCollection formdata)
        {
            Init();
            OrderBL bl = new OrderBL(sopH);
            Dictionary<string, string> parameter = sopH.GetParameters(formdata);
            ViewData[hHelp.SYS_SESSION_SERVER] = serverId;
            ViewData[hHelp.SYS_VIEWDATA_MODULEID] = moduleId;
            ViewData[hHelp.SYS_VIEWDATA_DATATABLE] = sopH.SetH.DataViewName;
            ViewData[hHelp.SYS_VIEWDATA_PARAMETERS] = sopH.Parameters_DictionaryToString(parameter);
            ViewData[hHelp.SYS_ORDER_COLUMNNAME_CHECK_LIST] = sopH.SetH.GetColumnVariable(hHelp.SYS_ORDER_COLUMNNAME_CHECK_LIST);
            ViewData[hHelp.SYS_ORDER_COLUMNNAME_CHECK_TO_SAVE_DATAVIEW] = sopH.SetH.GetColumnVariable(hHelp.SYS_ORDER_COLUMNNAME_CHECK_TO_SAVE_DATAVIEW);
            ViewData[hHelp.SYS_ORDER_COLUMNNAME_CHECK_BEFORE] = sopH.SetH.GetColumnVariable(hHelp.SYS_ORDER_COLUMNNAME_CHECK_BEFORE);
            DataSet datS = bl.GetDataSetAction(ViewData[hHelp.SYS_VIEWDATA_PARAMETERS].ToString());
            datS = DataInit(moduleId, formId, datS);
            ViewData[hHelp.SYS_ORDER_JSON_CHECK_SECCION_ITEM] = gvH.DataTableToJsonWithJavaScriptSerializer(datS.Tables[sopH.SetH.DataViewName], sopH.SetH.GlobalMaxJsonLength);
            return View(datS);
        }

        /// <summary>
        /// Get the gridview to get the inventories of an order
        /// </summary>
        /// <param name="parameters">String with parameters [order_request_id]</param>
        /// <returns>Grid with the inventory in the order</returns>
        public ActionResult GridView(string parameters)
        {
            Init();
            OrderBL bl = new OrderBL(sopH);
            ViewData[hHelp.SYS_VIEWDATA_MODULEID] = moduleId;
            ViewData[hHelp.SYS_VIEWDATA_PARAMETERS] = parameters;
            ViewData[hHelp.SYS_VIEWDATA_DATATABLE] = sopH.SetH.DataViewName;
            ViewData[hHelp.SYS_ORDER_COLUMNNAME_CHECK_LIST] = sopH.SetH.GetColumnVariable(hHelp.SYS_ORDER_COLUMNNAME_CHECK_LIST);
            ViewData[hHelp.SYS_ORDER_COLUMNNAME_CHECK_TO_SAVE_DATAVIEW] = sopH.SetH.GetColumnVariable(hHelp.SYS_ORDER_COLUMNNAME_CHECK_TO_SAVE_DATAVIEW);
            ViewData[hHelp.SYS_ORDER_COLUMNNAME_CHECK_BEFORE] = sopH.SetH.GetColumnVariable(hHelp.SYS_ORDER_COLUMNNAME_CHECK_BEFORE);
            DataSet datS = bl.GetDataSetAction(ViewData[hHelp.SYS_VIEWDATA_PARAMETERS].ToString());
            datS = DataInit(moduleId, formId, datS);
            ViewData[hHelp.SYS_ORDER_JSON_CHECK_SECCION_ITEM] = gvH.DataTableToJsonWithJavaScriptSerializer(datS.Tables[sopH.SetH.DataViewName], sopH.SetH.GlobalMaxJsonLength);
            return PartialView(hHelp.SYS_RAZO_GENERICGRIDVIEWSEARCH, datS);
        }

        /// <summary>
        /// Save the order to generate the action
        /// </summary>
        /// <param name="parameters">String with parameters [order_request_id]</param>
        /// <returns>Succes or error</returns>
        public ActionResult BatchUpdateAction(string parameters)
        {
            Init();
            OrderBL bl = new OrderBL(sopH);
            ViewData[hHelp.SYS_VIEWDATA_PARAMETERS] = parameters;
            ViewData[hHelp.SYS_VIEWDATA_DATATABLE] = sopH.SetH.DataViewName;
            ViewData[hHelp.SYS_ORDER_COLUMNNAME_CHECK_LIST] = sopH.SetH.GetColumnVariable(hHelp.SYS_ORDER_COLUMNNAME_CHECK_LIST);
            ViewData[hHelp.SYS_ORDER_COLUMNNAME_CHECK_TO_SAVE_DATAVIEW] = sopH.SetH.GetColumnVariable(hHelp.SYS_ORDER_COLUMNNAME_CHECK_TO_SAVE_DATAVIEW);
            ViewData[hHelp.SYS_ORDER_COLUMNNAME_CHECK_BEFORE] = sopH.SetH.GetColumnVariable(hHelp.SYS_ORDER_COLUMNNAME_CHECK_BEFORE);
            DataSet datS = bl.GetDataSetAction(ViewData[hHelp.SYS_VIEWDATA_PARAMETERS].ToString());
            DataTable dt = datS.Tables[sopH.SetH.DataViewName];
            try
            {
                dt = GetNewAndUpdateData(dt);
                XElement nodeAction = sopH.SetH.GetNodeAction(sopH.SetH.SYSTEM_ACTION_DATAVIEW, hHelp.SYS_ORDER_DATAVIEWACTIONNAME_ORDER_REQUEST_ITEM_ACTION);
                string idAction = nodeAction.Attribute(sopH.SetH.SETTING_NAME_GENERIC_ID).Value != null ? nodeAction.Attribute(sopH.SetH.SETTING_NAME_GENERIC_ID).Value.ToString() : "";
                string dataViewName = nodeAction.Element(sopH.SetH.SETTING_NAME_PARAMETERS).Element(sopH.SetH.SETTING_NAME_DATAVIEW).Value != null ? nodeAction.Element(sopH.SetH.SETTING_NAME_PARAMETERS).Element(sopH.SetH.SETTING_NAME_DATAVIEW).Value.ToString() : "";
                if (!string.IsNullOrEmpty(dataViewName))
                {
                    int ii = -1;
                    DataTable dtAct = datS.Tables[dataViewName];
                    string keyColumnAct = dtAct.PrimaryKey[0] != null ? dtAct.PrimaryKey[0].ColumnName : "";
                    string keyColum = dt.PrimaryKey[0] != null ? dt.PrimaryKey[0].ColumnName : "";
                    var query = from order in dt.AsEnumerable()
                                where order.Field<bool>(sopH.SetH.GetColumnVariable(hHelp.SYS_ORDER_COLUMNNAME_CHECK_TO_SAVE_DATAVIEW)) == true
                                select order;
                    if (query.Count() > 0)
                    {
                        foreach (DataRow dtF in query)
                        {
                            DataRow dr = dtAct.NewRow();
                            dr[keyColumnAct] = ii--;
                            dr[keyColum] = dtF[keyColum].ToString();
                            dr = sopH.SetH.GetRowAction(dr, idAction, new Dictionary<string, object>());
                            dtAct.Rows.Add(dr);
                        }
                    }
                    DataSet newDaS = sopH.SaveDataAction(parameters, new Dictionary<string, string>(), idAction, dtAct);
                }
            }
            catch (Exception e)
            {
                Guid d = Guid.NewGuid();
                log.Fatal(Guid.NewGuid(), e);
                ViewData[hHelp.SYS_VIEWDATA_EDIT_ERROR] = String.Format(e.Message);
            }
            DataSet newDatS = bl.GetDataSetAction(ViewData[hHelp.SYS_VIEWDATA_PARAMETERS].ToString());
            newDatS = DataInit(moduleId, formId, newDatS);
            ViewData[hHelp.SYS_ORDER_JSON_CHECK_SECCION_ITEM] = gvH.DataTableToJsonWithJavaScriptSerializer(newDatS.Tables[sopH.SetH.DataViewName], sopH.SetH.GlobalMaxJsonLength);
            return PartialView(hHelp.SYS_RAZO_GENERICGRIDVIEWSEARCH, newDatS);
        }
        #endregion
        #region protected function
        protected override DataSet DataInit(string moduleId, string formId, DataSet datS)
        {
            ViewData[hHelp.SYS_VIEWDATA_MODULEID] = moduleId;
            ViewData[hHelp.SYS_VIEWDATA_FORMID] = formId;
            ViewData[hHelp.SYS_VIEWDATA_FORMURL] = "Index";
            ViewData[hHelp.SYS_VIEWDATA_FORMCONTROLLER] = "Order";
            ViewData[hHelp.SYS_VIEWDATA_FORMIDHTML] = "formFilter";
            ViewData[hHelp.SYS_VIEWDATA_TITLEMENU] = "Order";
            ViewBag.MenuTitle = "ORDER";
            ViewBag.MenuTitleInfo = "Search order in storage location";
            ViewBag.searchTitle = "Scan the order to verify its content";
            setH = new SettingsHelp(Session[hHelp.SYS_SESSION_SERVER].ToString(), moduleId, formId, pathSetting);
            datS.Tables.Add(setH.GetInputs());
            return datS;
        }
        #endregion
    }
}