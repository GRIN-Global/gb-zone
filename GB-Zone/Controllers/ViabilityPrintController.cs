﻿using GrinGlobal.Zone.Helpers.controllerBL;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Xml.Linq;

namespace GrinGlobal.Zone.Controllers
{
    public class ViabilityPrintController : CoreController
    {
        private ViabilityPrintBL bl;

        /// <summary>
        /// Create the page for Viability print module
        /// </summary>
        /// <param name="moduleId">Id module from settings.xml</param>
        /// <param name="formId">Id form from settings.xml</param>
        /// <returns>Page Vaibility Print </returns>
        // GET: ViabilityPrint
        public ActionResult Index(string moduleId, string formId)
        {
            Init(moduleId, formId);
            DataSet ds = DataInit(moduleId, formId, new DataSet());
            return View(ds);
        }

        /// <summary>
        /// Search in the specific order to assign the feasibility data rule
        /// </summary>
        /// <param name="formCollection">Form data from web [order_request_id]</param>
        /// <returns>The page to assign the inventory visibility rule</returns>
        [HttpPost]
        public ActionResult Index(FormCollection formCollection)
        {
            Init();
            ViewData[hHelp.SYS_VIEWDATA_SUCCESS] = "100";
            DataSet datS = new DataSet();
            try
            {
                bl = new ViabilityPrintBL(sopH);
                Dictionary<string, string> parameters = sopH.GetParameters(formCollection);
                ViewData[hHelp.SYS_VIABILITYPRINT_JSON_ORDERID] = parameters[sopH.SetH.GetField_AtributeValue(hHelp.SYS_VIABILITYPRINT_FIELDNAME_NUMBERORDER).Attribute(sopH.SetH.SETTING_NAME_GENERIC_ID).Value];
                string parameterss = sopH.Parameters_DictionaryToString(parameters);
                datS = bl.GetDataSet(parameterss);
            }
            catch (Exception ex)
            {
                ViewData[hHelp.SYS_VIEWDATA_MESSAGEERROR] = ex.Message;
            }
            datS = DataInit(moduleId, formId, datS);
            SetViewData(bl.DicDataView);
            return View(datS);
        }

        /// <summary>
        /// Get the data view to match the order request ID
        /// </summary>
        /// <param name="parameters">String to the parameters [order_request_id]</param>
        /// <returns>Grid view with data filter by order request id</returns>
        public ActionResult GridView(string parameters)
        {
            Init();
            ViewData[hHelp.SYS_VIEWDATA_SUCCESS] = "100";
            Dictionary<string, string> dic = sopH.Parameters_StringToDictionary(parameters);
            ViewData[hHelp.SYS_VIABILITYPRINT_JSON_ORDERID] = dic[sopH.SetH.GetField_AtributeValue(hHelp.SYS_VIABILITYPRINT_FIELDNAME_NUMBERORDER).Attribute(sopH.SetH.SETTING_NAME_GENERIC_ID).Value];
            DataSet datS = new DataSet();
            try
            {
                bl = new ViabilityPrintBL(sopH);
                datS = bl.GetDataSet(parameters);
                SetViewData(bl.DicDataView);
            }
            catch (Exception ex)
            {
                ViewData[hHelp.SYS_VIEWDATA_MESSAGEERROR] = ex.Message;
            }
            datS = DataInit(moduleId, formId, datS);
            return PartialView("MasterPartitial", datS);
        }

        /// <summary>
        /// Get the detail to the grid view the order reques id
        /// </summary>
        /// <param name="parameters">String to the parameters [inventory_id]</param>
        /// <returns>Grid view with data filter by inventory id </returns>
        public ActionResult DetailPartitial(string invenotryId)
        {
            ViewData["invenotryId"] = invenotryId;
            Init();
            DataSet datS = new DataSet();
            try
            {
                XElement node = sopH.SetH.GetField_AtributeValue("inventoryId");
                string dataviewName = sopH.SetH.GetDataviewName_Parameter(node);
                ViewData[hHelp.SYS_VIEWDATA_DATATABLE] = dataviewName;
                Dictionary<string, string> dic = sopH.GetOnlyParameters(dataviewName);
                string param = node.Attribute("id").Value;
                if (dic.ContainsKey(param))
                {
                    dic[param] = invenotryId;
                }
                string parameters = sopH.Parameters_DictionaryToString(dic);
                datS = sopH.GetData(parameters, dataviewName);
            }
            catch (Exception ex)
            {
                ViewData[hHelp.SYS_VIEWDATA_MESSAGEERROR] = ex.Message;
            }

            return PartialView("DetailPartitial", datS);
        }

        /// <summary>
        /// Create the inventory viability for each inventory in the order
        /// </summary>
        /// <param name="formCollection">Form data from web [order_request_id, inventory_vaibility_rule_id]</param>
        /// <returns>Redirect to the first page</returns>
        [HttpPost]
        public JsonResult SaveInventoryViability(FormCollection formCollection)
        {
            Init();
            string menssageError = "";
            int success = 100;
            string parameters = formCollection[sopH.SetH.SETTING_NAME_PARAMETERS];
            int inventoryVialityRuleId = Convert.ToInt32(formCollection[hHelp.SYS_VIABILITYPRINT_FIELDNAME_INVNETORYVIABILITYRULEID]);
            ViabilityPrintBL bl = new ViabilityPrintBL(sopH);
            try
            {
                bl.Save(parameters, inventoryVialityRuleId);
                success = 200;
            }
            catch (Exception ex)
            {
                menssageError = ex.Message;
                success = 500;
            }
            Dictionary<string, object> json = new Dictionary<string, object>();
            json.Add("Success", success);
            json.Add("MenssageError", menssageError);
            json.Add("Data", null);
            var objJson = Json(json, JsonRequestBehavior.AllowGet);
            objJson.MaxJsonLength = sopH.SetH.GlobalMaxJsonLength;
            return objJson;
        }

        protected override DataSet DataInit(string moduleId, string formId, DataSet datS)
        {
            datS = base.DataInit(moduleId, formId, datS);
            ViewBag.MenuTitle = "Viability";
            ViewBag.MenuTitleInfo = "Viability generate";
            ViewBag.SearchTitle = "Scan an order to generate inventory viability data";
            ViewData[hHelp.SYS_VIEWDATA_MODULEID] = moduleId;
            ViewData[hHelp.SYS_VIEWDATA_FORMURL] = "index";
            ViewData[hHelp.SYS_VIEWDATA_FORMCONTROLLER] = "ViabilityPrint";
            return datS;
        }


    }
}