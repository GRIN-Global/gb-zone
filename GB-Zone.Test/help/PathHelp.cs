﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GB_Zone.Test.help
{
    class PathHelp
    {
        public string GetSettingFile()
        {
            string pathSettingfile = string.Empty;
            string startupPath = System.IO.Directory.GetCurrentDirectory();
            string[] stringSeparators = new string[] { ".Test" };
            string[] parts = startupPath.Split(stringSeparators, StringSplitOptions.RemoveEmptyEntries);
            if (parts.Count() == 2)
            {
                string temp = parts[0];
                temp = Path.Combine(temp, "Setting.xml");
                if (System.IO.File.Exists(temp))
                {
                    pathSettingfile = temp;
                }
            }
            return pathSettingfile;
        }
    }
}
