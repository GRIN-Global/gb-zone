﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GB_Zone.Test.help
{
    class DataTableHelp
    {
        public DataTable CreateInventoryVaibilityTable(string tableName = "")
        {
            if (tableName == "")
            {
                tableName = "get_inventory_viability";
            }
            DataTable viab = new DataTable() { TableName = tableName };
            DataRow dr = viab.NewRow();
            DataColumn viabId = new DataColumn() { ColumnName = "inventory_viability_id", DataType = System.Type.GetType("System.Int32") };
            viab.Columns.Add(viabId);
            viab.PrimaryKey = new DataColumn[] { viabId };
            viab.Columns.Add(new DataColumn() { ColumnName = "inventory_id", DataType = System.Type.GetType("System.Int32") });
            viab.Columns.Add(new DataColumn() { ColumnName = "inventory_viability_rule_id", DataType = System.Type.GetType("System.Int32") });
            viab.Columns.Add(new DataColumn() { ColumnName = "tested_date", DataType = System.Type.GetType("System.DateTime") });
            viab.Columns.Add(new DataColumn() { ColumnName = "percent_viable", DataType = System.Type.GetType("System.Int32") });
            viab.Columns.Add(new DataColumn() { ColumnName = "percent_normal", DataType = System.Type.GetType("System.Int32") });
            viab.Columns.Add(new DataColumn() { ColumnName = "total_tested_count", DataType = System.Type.GetType("System.Int32") });
            viab.Columns.Add(new DataColumn() { ColumnName = "replication_count", DataType = System.Type.GetType("System.Int32") });
            viab.Columns.Add(new DataColumn() { ColumnName = "note", DataType = System.Type.GetType("System.String") });
            dr = viab.NewRow();
            dr["inventory_id"] = 1;
            dr["inventory_viability_rule_id"] = 1;
            dr["inventory_viability_id"] = 1;
            viab.Rows.Add(dr);
            return viab;
        }

        public DataTable CreateInventoryViabilityDataTable(string tableName = "")
        {
            if (tableName == "")
            {
                tableName = "get_inventory_viability_data";
            }
            DataTable dt = new DataTable() { TableName = tableName };
            DataColumn dcId = new DataColumn() { ColumnName = "inventory_viability_data_id" };
            dt.Columns.Add(dcId);
            dt.PrimaryKey = new DataColumn[] { dcId };
            dt.Columns.Add(new DataColumn() { ColumnName = "tested_date", DataType = System.Type.GetType("System.DateTime") });
            dt.Columns.Add(new DataColumn() { ColumnName = "inventory_viability_id", DataType = System.Type.GetType("System.Int32") });
            dt.Columns.Add(new DataColumn() { ColumnName = "percent_viable", DataType = System.Type.GetType("System.Int32") });
            dt.Columns.Add(new DataColumn() { ColumnName = "inventory_id", DataType = System.Type.GetType("System.Int32") });
            dt.Columns.Add(new DataColumn() { ColumnName = "inventory_viability_rule_id", DataType = System.Type.GetType("System.Int32") });
            dt.Columns.Add(new DataColumn() { ColumnName = "replication_number", DataType = System.Type.GetType("System.Int32") });
            dt.Columns.Add(new DataColumn() { ColumnName = "count_number", DataType = System.Type.GetType("System.Int32") });
            dt.Columns.Add(new DataColumn() { ColumnName = "count_date", DataType = System.Type.GetType("System.DateTime") });
            dt.Columns.Add(new DataColumn() { ColumnName = "normal_count", DataType = System.Type.GetType("System.Int32") });
            dt.Columns.Add(new DataColumn() { ColumnName = "abnormal_count", DataType = System.Type.GetType("System.Int32") });
            dt.Columns.Add(new DataColumn() { ColumnName = "dormant_count", DataType = System.Type.GetType("System.Int32") });
            dt.Columns.Add(new DataColumn() { ColumnName = "hard_count", DataType = System.Type.GetType("System.Int32") });
            dt.Columns.Add(new DataColumn() { ColumnName = "empty_count", DataType = System.Type.GetType("System.Int32") });
            dt.Columns.Add(new DataColumn() { ColumnName = "infested_count", DataType = System.Type.GetType("System.Int32") });
            dt.Columns.Add(new DataColumn() { ColumnName = "dead_count", DataType = System.Type.GetType("System.Int32") });
            dt.Columns.Add(new DataColumn() { ColumnName = "unknown_count", DataType = System.Type.GetType("System.Int32") });
            dt.Columns.Add(new DataColumn() { ColumnName = "note", DataType = System.Type.GetType("System.String") });
            dt.Columns.Add(new DataColumn() { ColumnName = "counter_cooperator_id", DataType = System.Type.GetType("System.Int32") });
            DataRow dr = dt.NewRow();
            dr["inventory_id"] = 1;
            dr["inventory_viability_rule_id"] = 1;
            dr["inventory_viability_data_id"] = 1;
            dr["replication_number"] = 1;
            dt.Rows.Add(dr);
            return dt;
        }

        public DataTable CreateInventoryViabilityRuleTable(string tableName = "")
        {
            if (tableName == "")
            {
                tableName = "get_inventory_viability_rule";
            }
            DataTable dt = new DataTable() { TableName = tableName };
            DataColumn dCId = new DataColumn() { ColumnName = "inventory_viability_rule_id" };
            dt.Columns.Add(dCId);
            dt.PrimaryKey = new DataColumn[] { dCId };
            dt.Columns.Add(new DataColumn() { ColumnName = "name", DataType = System.Type.GetType("System.String") });
            dt.Columns.Add(new DataColumn() { ColumnName = "seeds_per_replicate", DataType = System.Type.GetType("System.Int32") });
            dt.Columns.Add(new DataColumn() { ColumnName = "number_of_replicates", DataType = System.Type.GetType("System.Int32") });
            dt.Columns.Add(new DataColumn() { ColumnName = "tested_date", DataType = System.Type.GetType("System.DateTime") });
            DataRow dr = dt.NewRow();
            dr["inventory_viability_rule_id"] = 1;
            dr["name"] = "nombre";
            dr["seeds_per_replicate"] = 1;
            dr["number_of_replicates"] = 1;
            dr["tested_date"] = DateTime.Now;
            dt.Rows.Add(dr);
            return dt;
        }

        public DataTable CreateInventoryTable(string tableName = "")
        {
            if (tableName == "")
            {
                tableName = "get_inventory";
            }
            DataTable dt = new DataTable() { TableName = tableName };
            DataColumn dCId = new DataColumn() { ColumnName = "inventory_id" };
            dt.Columns.Add(dCId);
            dt.PrimaryKey = new DataColumn[] { dCId };
            dt.Columns.Add(new DataColumn() { ColumnName = "inventory_number_part1", DataType = System.Type.GetType("System.String") });
            dt.Columns.Add(new DataColumn() { ColumnName = "inventory_number_part2", DataType = System.Type.GetType("System.Int32") });
            dt.Columns.Add(new DataColumn() { ColumnName = "inventory_number_part3", DataType = System.Type.GetType("System.String") });
            dt.Columns.Add(new DataColumn() { ColumnName = "availability_status_code", DataType = System.Type.GetType("System.String") });

            DataRow dr = dt.NewRow();
            dr["inventory_id"] = 1;
            dr["inventory_number_part1"] = "AL01";
            dr["inventory_number_part2"] = 1;
            dr["inventory_number_part3"] = "SD";
            dt.Rows.Add(dr);
            return dt;
        }

        public DataTable CreateAccesionTable(string tableName = "")
        {
            if (tableName == "")
            {
                tableName = "get_accesion";
            }
            DataTable dt = new DataTable() { TableName = tableName };
            DataColumn dCId = new DataColumn() { ColumnName = "accesion_id" };
            dt.Columns.Add(dCId);
            dt.PrimaryKey = new DataColumn[] { dCId };
            dt.Columns.Add(new DataColumn() { ColumnName = "accession_number_part1", DataType = System.Type.GetType("System.String") });
            dt.Columns.Add(new DataColumn() { ColumnName = "accession_number_part2", DataType = System.Type.GetType("System.Int32") });
            dt.Columns.Add(new DataColumn() { ColumnName = "accession_number_part3", DataType = System.Type.GetType("System.String") });
            DataRow dr = dt.NewRow();
            dr["accesion_id"] = 1;
            dr["accession_number_part1"] = "CWI";
            dr["accession_number_part2"] = 855;
            dr["accession_number_part3"] = "DD";
            dt.Rows.Add(dr);
            return dt;
        }

        public DataTable CreateInventoryActionTable(string tableName = "")
        {
            if (tableName == "")
            {
                tableName = "get_inventory_action";
            }
            DataTable dt = new DataTable() { TableName = tableName };
            DataColumn dCId = new DataColumn() { ColumnName = "inventory_action_id" };
            dt.Columns.Add(dCId);
            dt.PrimaryKey = new DataColumn[] { dCId };
            dt.Columns.Add(new DataColumn() { ColumnName = "action_name_code", DataType = System.Type.GetType("System.String") });
            dt.Columns.Add(new DataColumn() { ColumnName = "started_date", DataType = System.Type.GetType("System.DateTime") });
            dt.Columns.Add(new DataColumn() { ColumnName = "completed_date", DataType = System.Type.GetType("System.DateTime") });
            dt.Columns.Add(new DataColumn() { ColumnName = "started_date_code", DataType = System.Type.GetType("System.String") });
            dt.Columns.Add(new DataColumn() { ColumnName = "completed_date_code", DataType = System.Type.GetType("System.String") });
            dt.Columns.Add(new DataColumn() { ColumnName = "quantity", DataType = System.Type.GetType("System.Double") });
            dt.Columns.Add(new DataColumn() { ColumnName = "quantity_unit_code", DataType = System.Type.GetType("System.String") });
            dt.Columns.Add(new DataColumn() { ColumnName = "form_code", DataType = System.Type.GetType("System.String") });
            dt.Columns.Add(new DataColumn() { ColumnName = "cooperator_id", DataType = System.Type.GetType("System.Int32") });
            dt.Columns.Add(new DataColumn() { ColumnName = "method_id", DataType = System.Type.GetType("System.Int32") });
            DataRow dr = dt.NewRow();
            dr["inventory_action_id"] = 1;
            dr["action_name_code"] = "TEST_VIABILITY";
            dr["started_date"] = DateTime.Now;
            dt.Rows.Add(dr);
            return dt;
        }


    }
}
