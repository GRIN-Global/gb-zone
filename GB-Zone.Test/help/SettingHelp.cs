﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace GB_Zone.Test.help
{
    class SettingHelp
    {
        private string path;
        public SettingHelp(string pathSettingFile)
        {
            path = pathSettingFile;
        }

        public List<SettingData> ReadImportServer()
        {
            List<SettingData> servers = new List<SettingData>();
            XElement xml = XElement.Load(path);
            List<XElement> elements = xml.Descendants("server").ToList();
            foreach (XElement el in elements)
            {
                SettingData server = new SettingData();
                server.Server = el.Attribute("id").Value;
                server.Url = el.Attribute("url").Value;
                XElement user = el.Descendants("userTest").ToArray()[0];
                server.User = user.Element("user").Value;
                server.Pass = user.Element("pass").Value;
                XElement modulo = el.Descendants("module").ToArray()[0];
                server.Module = modulo.Attribute("id").Value;
                XElement form = modulo.Descendants("form").ToArray()[0];
                server.Form = form.Attribute("id").Value;
                servers.Add(server);
            }
            return servers;
        }

        public List<string> GetAllDataview()
        {
            List<string> dataviews = new List<string>();
            List<SettingData> servers = new List<SettingData>();
            XElement xml = XElement.Load(path);
            List<XElement> xDataviews = xml.Descendants("dataviewName").ToList();
            foreach (XElement xData in xDataviews)
            {
                dataviews.Add(xData.Value);
            }
            return dataviews;
        }

        public List<string> GetAllDataviewByServer(string server)
        {
            List<string> dataviews = new List<string>();
            List<SettingData> servers = new List<SettingData>();
            XElement xml = XElement.Load(path);
            XElement element = xml.Descendants("server").Where(e => e.Attribute("id").Value == server).DefaultIfEmpty(null).FirstOrDefault();
            if (element != null)
            {
                List<XElement> xDataviews = element.Descendants("dataviewName").ToList();
                foreach (XElement xData in xDataviews)
                {
                    dataviews.Add(xData.Value);
                }
            }
            return dataviews;
        }
    }

    class SettingData
    {
        public string Server { set; get; }
        public string Module { set; get; }
        public string Form { set; get; }
        public string Url { set; get; }
        public string User { set; get; }
        public string Pass { set; get; }
    }
}
