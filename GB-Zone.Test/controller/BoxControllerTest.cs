﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using GrinGlobal.Zone.Controllers;
using Moq;
using System.Web.Mvc;
using MvcContrib.TestHelper;
using System.Data;
using GrinGlobal.Zone.Classes;
using GB_Zone.Test.help;
using GrinGlobal.Zone.Helpers;
using System.Collections.Generic;
using GrinGlobal.Zone.Helpers.setting;

namespace GB_Zone.Test.controller
{
    [TestClass]
    public class BoxControllerTest
    {
        private string moduleId = "Inventory";
        private string formId = "get_storage_box";
        private string serverId = "01";
        private TestControllerBuilder builder;
        private PathHelp pathH = new PathHelp();
        [TestMethod]
        public void BoxController_Index()
        {
            builder = new TestControllerBuilder();
            builder.Session.Add("server", serverId);
            string module = moduleId;
            string form = formId;
            string startupPath = System.IO.Directory.GetCurrentDirectory();


            BoxController con = new BoxController();
            builder.InitializeController(con);
            con.InjectDependencyTest(pathH.GetSettingFile());
            ViewResult action = con.Index(module, form) as ViewResult;


            Assert.IsNotNull(action);
            Assert.IsNotNull(action.Model);
            Assert.IsNotNull(action.ViewData["moduleId"]);
            Assert.IsNotNull(action.ViewData["formId"]);
            Assert.IsNotNull(action.ViewData["formUrl"]);
            Assert.IsNotNull(action.ViewData["formController"]);
            Assert.IsNotNull(action.ViewData["formIdHtml"]);
            Assert.IsNotNull(action.ViewData["titleMenu"]);
        }

        [TestMethod]
        public void BoxController_IndexForm_ReturnsValue()
        {
            // Arrange
            builder = GetBuilder();
            var mock = new Mock<IGrinGlobalSoapHelp>();
            var formValues = GetForm();
            DataSet ds = new DataSet();
            DataTable dt = new DataTable();
            dt.TableName = "gbz_get_storage_box";
            dt.Columns.Add("inventory_number", typeof(string));
            ds.Tables.Add(dt);
            Dictionary<string, string> parameters = new Dictionary<string, string>()
            {
                [":inventoryNumber"] = "AL09-F04-07"
            };
            string parametersS = ":inventoryNumber:AL09-F04-07";
            mock.Setup(m => m.GetParameters(formValues, "", null)).Returns(parameters);
            mock.Setup(m => m.Parameters_DictionaryToString(parameters)).Returns(parametersS);
            mock.Setup(m => m.GetData(It.IsAny<string>(), "")).Returns(ds);
            // Act
            BoxController con = new BoxController();
            builder.InitializeController(con);
            con.InjectDependencyTest(mock.Object);
            con.InjectDependencyTest(pathH.GetSettingFile());
            ActionResult action = con.Index(formValues) as ActionResult;

            // Assert
            Assert.IsNotNull(action);
        }

        [TestMethod]
        public void BoxController_IndexForm_ReturnsNull()
        {
            // Arrange
            builder = GetBuilder();
            var mock = new Mock<IGrinGlobalSoapHelp>();
            var formValues = GetForm();
            DataSet ds = new DataSet();
            DataTable dt = new DataTable();
            ds.Tables.Add(dt);
            Dictionary<string, string> parameters = new Dictionary<string, string>()
            {
                [":inventoryNumber"] = "AL09-F04-07"
            };
            string parametersS = ":inventoryNumber:AL09-F04-07";
            mock.Setup(m => m.GetParameters(formValues, "", null)).Returns(parameters);
            mock.Setup(m => m.Parameters_DictionaryToString(parameters)).Returns(parametersS);
            mock.Setup(m => m.GetData(It.IsAny<string>(), "")).Returns(ds);
            // Act
            BoxController con = new BoxController();
            builder.InitializeController(con);
            con.InjectDependencyTest(pathH.GetSettingFile());
            con.InjectDependencyTest(mock.Object);
            ActionResult action = con.Index(formValues) as ActionResult;

            // Assert
            Assert.IsNotNull(action);
        }

        [TestMethod]
        public void BoxController_GridView_ReturnsValue()
        {
            // Arrange
            builder = GetBuilder();
            var mock = new Mock<IGrinGlobalSoapHelp>();
            var formValues = GetForm();
            DataSet ds = new DataSet();
            DataTable dt = new DataTable();
            ds.Tables.Add(dt);
            Dictionary<string, string> parameters = new Dictionary<string, string>()
            {
                [":inventoryNumber"] = "AL09-F04-07"
            };
            string parametersS = ":inventoryNumber:AL09-F04-07";
            mock.Setup(m => m.GetParameters(formValues, "", null)).Returns(parameters);
            mock.Setup(m => m.Parameters_DictionaryToString(parameters)).Returns(parametersS);
            mock.Setup(m => m.GetData(It.IsAny<string>(), "")).Returns(ds);
            // Act
            BoxController con = new BoxController();
            builder.InitializeController(con);
            con.InjectDependencyTest(pathH.GetSettingFile());
            con.InjectDependencyTest(mock.Object);
            ActionResult action = con.GridView(parametersS) as ActionResult;

            // Assert
            Assert.IsNotNull(action);
        }

        [TestMethod]
        public void BoxController_BatchUpdateAction_SaveEmpyt()
        {
            // Arrange
            builder = GetBuilder();
            var mock = new Mock<IGrinGlobalSoapHelp>();
            var formValues = GetForm();
            DataSet ds = new DataSet();
            DataTable dt = new DataTable();
            dt.TableName = "storage_gbz_get_storage_address";
            dt.Columns.Add("inventory_number", typeof(string));
            ds.Tables.Add(dt);
            Dictionary<string, string> parameters = new Dictionary<string, string>()
            {
                [":inventoryNumber"] = "AL09-F04-07"
            };
            string parametersS = ":inventoryNumber:AL09-F04-07";
            SettingsHelp setH = new SettingsHelp("01", "Inventory", "get_storage_box", pathH.GetSettingFile());
            mock.Setup(m => m.GetParameters(formValues, "", null)).Returns(parameters);
            mock.Setup(m => m.Parameters_DictionaryToString(parameters)).Returns(parametersS);
            mock.Setup(m => m.GetData(It.IsAny<string>(), "")).Returns(ds);
            mock.Setup(m => m.SetH).Returns(setH);
            // Act
            BoxController con = new BoxController();
            builder.InitializeController(con);
            con.InjectDependencyTest(pathH.GetSettingFile());
            con.InjectDependencyTest(mock.Object);
            ActionResult action = con.BatchUpdateAction(parametersS) as ActionResult;

            // Assert
            Assert.IsNotNull(action);
        }

        private FormCollection GetForm()
        {
            return new FormCollection() {
                { "moduleId", moduleId },
                { "formId", formId},
                { ":inventoryNumber", "AL09-F04-07" }
            };
        }

        private TestControllerBuilder GetBuilder()
        {
            TestControllerBuilder builder = new TestControllerBuilder();
            builder.Session.Add("server", serverId);
            builder.TempDataDictionary.Add("moduleId", moduleId);
            builder.TempDataDictionary.Add("formId", formId);
            builder.TempDataDictionary.Add("server", serverId);
            return builder;
        }
    }
}
