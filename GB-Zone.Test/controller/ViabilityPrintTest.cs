﻿using System;
using System.Text;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using MvcContrib.TestHelper;
using GB_Zone.Test.help;
using GrinGlobal.Zone.Controllers;
using System.Web.Mvc;
using System.Data;
using GrinGlobal.Zone.Helpers;
using Moq;
using GrinGlobal.Zone.Helpers.setting;

namespace GB_Zone.Test.controller
{
    /// <summary>
    /// Summary description for ViabilityPrintQR
    /// </summary>
    [TestClass]
    public class ViabilityPrintTest
    {
        private string serverId = "01";
        private string moduleId = "Viability";
        private string formId = "invenotry_viability_print";
        private TestControllerBuilder builder;
        private PathHelp pathH = new PathHelp();
        [TestMethod]
        public void ViabilityPrintController_Index()
        {
            //Arrange
            builder = GetBuilder();
            ViabilityPrintController con = new ViabilityPrintController();
            builder.InitializeController(con);
            con.InjectDependencyTest(pathH.GetSettingFile());
            //Act
            ViewResult action = con.Index(moduleId, formId) as ViewResult;

            //Assert
            Assert.IsNotNull(action);
            Assert.IsNotNull(action.Model);
        }

        [TestMethod]
        public void ViabilityPrintController_IndexForm_ReturnsValue()
        {
            // Arrange
            TestControllerBuilder builder = GetBuilder();
            var mock = new Mock<IGrinGlobalSoapHelp>();

            DataSet ds = GetDataSet();
            DataSet dsCombo = GetDataSetCombo();


            mock.Setup(m => m.GetParameters(It.IsAny<FormCollection>(), It.IsAny<string>(), It.IsAny<Dictionary<string, string>>())).Returns(GetParameter());
            mock.Setup(m => m.Parameters_DictionaryToString(It.IsAny<Dictionary<string, string>>())).Returns(GetParameterS());
            mock.Setup(m => m.Parameters_DictionaryToString(It.IsAny<Dictionary<string, string>>())).Returns(GetParameterS());
            mock.Setup(m => m.GetData(It.IsAny<string>(), "")).Returns(ds);
            mock.Setup(m => m.GetData("", It.IsAny<string>())).Returns(dsCombo);
            mock.Setup(m => m.SetH).Returns(GetSetting());
            // Act
            ViabilityPrintController con = new ViabilityPrintController();
            builder.InitializeController(con);
            con.InjectDependencyTest(mock.Object);
            con.InjectDependencyTest(pathH.GetSettingFile());
            ActionResult action = con.Index(GetForm()) as ActionResult;

            // Assert
            Assert.IsNotNull(action);
        }

        [TestMethod]
        public void ViabilityPrintController_IndexForm_ReturnsNull()
        {
            // Arrange
            TestControllerBuilder builder = GetBuilder();
            var mock = new Mock<IGrinGlobalSoapHelp>();

            DataSet ds = GetDataSet();
            DataSet dsCombo = GetDataSetCombo();

            mock.Setup(m => m.GetParameters(It.IsAny<FormCollection>(), It.IsAny<string>(), It.IsAny<Dictionary<string, string>>())).Returns(GetParameter());
            mock.Setup(m => m.Parameters_DictionaryToString(It.IsAny<Dictionary<string, string>>())).Returns(GetParameterS());
            mock.Setup(m => m.Parameters_DictionaryToString(It.IsAny<Dictionary<string, string>>())).Returns(GetParameterS());
            mock.Setup(m => m.GetData(It.IsAny<string>(), "")).Returns(ds);
            mock.Setup(m => m.GetData("", It.IsAny<string>())).Returns(dsCombo);
            mock.Setup(m => m.SetH).Returns(GetSetting());
            // Act
            ViabilityPrintController con = new ViabilityPrintController();
            builder.InitializeController(con);
            con.InjectDependencyTest(pathH.GetSettingFile());
            con.InjectDependencyTest(mock.Object);
            mock.Setup(m => m.SetH).Returns(GetSetting());
            ActionResult action = con.Index(GetForm()) as ActionResult;

            // Assert
            Assert.IsNotNull(action);
        }

        [TestMethod]
        public void ViabilityPrintController_GridView_ReturnsValue()
        {
            // Arrange
            TestControllerBuilder builder = GetBuilder();
            var mock = new Mock<IGrinGlobalSoapHelp>();
            DataSet ds = GetDataSet();
            DataSet dsCombo = GetDataSetCombo();
            mock.Setup(m => m.GetParameters(It.IsAny<FormCollection>(), It.IsAny<string>(), It.IsAny<Dictionary<string, string>>())).Returns(GetParameter());
            mock.Setup(m => m.Parameters_DictionaryToString(It.IsAny<Dictionary<string, string>>())).Returns(GetParameterS());
            mock.Setup(m => m.Parameters_StringToDictionary(It.IsAny<string>())).Returns(GetParameter());
            mock.Setup(m => m.GetData(It.IsAny<string>(), "")).Returns(ds);
            mock.Setup(m => m.SetH).Returns(GetSetting());
            mock.Setup(m => m.GetData("", It.IsAny<string>())).Returns(dsCombo);
            // Act
            ViabilityPrintController con = new ViabilityPrintController();
            builder.InitializeController(con);
            con.InjectDependencyTest(pathH.GetSettingFile());
            con.InjectDependencyTest(mock.Object);
            ActionResult action = con.GridView(GetParameterS()) as ActionResult;

            // Assert
            Assert.IsNotNull(action);
        }

        [TestMethod]
        public void ViabilityPrintController_DetailPartitial_ReturnsValue()
        {
            // Arrange
            TestControllerBuilder builder = GetBuilder();
            var mock = new Mock<IGrinGlobalSoapHelp>();
            DataSet ds = GetDataSet();
            DataSet dsCombo = GetDataSetCombo();
            mock.Setup(m => m.GetParameters(It.IsAny<FormCollection>(), It.IsAny<string>(), It.IsAny<Dictionary<string, string>>())).Returns(GetParameter());
            mock.Setup(m => m.Parameters_DictionaryToString(It.IsAny<Dictionary<string, string>>())).Returns(GetParameterS());
            mock.Setup(m => m.Parameters_StringToDictionary(It.IsAny<string>())).Returns(GetParameter());
            mock.Setup(m => m.GetData(It.IsAny<string>(), "")).Returns(ds);
            mock.Setup(m => m.SetH).Returns(GetSetting());
            mock.Setup(m => m.GetData("", It.IsAny<string>())).Returns(dsCombo);
            mock.Setup(m => m.GetOnlyParameters(It.IsAny<string>())).Returns(GetParameter());
            // Act
            ViabilityPrintController con = new ViabilityPrintController();
            builder.InitializeController(con);
            con.InjectDependencyTest(pathH.GetSettingFile());
            con.InjectDependencyTest(mock.Object);
            ActionResult action = con.DetailPartitial(GetParameterS()) as ActionResult;

            // Assert
            Assert.IsNotNull(action);
        }

        [TestMethod]
        public void OrderController_SaveInventoryViability_Save()
        {
            // Arrange
            TestControllerBuilder builder = GetBuilder();
            var mock = new Mock<IGrinGlobalSoapHelp>();
            DataSet ds = GetDataSet();
            DataSet dsCombo = GetDataSetCombo();
            mock.Setup(m => m.GetParameters(It.IsAny<FormCollection>(), It.IsAny<string>(), It.IsAny<Dictionary<string, string>>())).Returns(GetParameter());
            mock.Setup(m => m.Parameters_DictionaryToString(It.IsAny<Dictionary<string, string>>())).Returns(GetParameterS());
            mock.Setup(m => m.Parameters_StringToDictionary(It.IsAny<string>())).Returns(GetParameter());
            mock.Setup(m => m.GetData(It.IsAny<string>(), "")).Returns(ds);
            mock.Setup(m => m.SetH).Returns(GetSetting());
            mock.Setup(m => m.GetData("", It.IsAny<string>())).Returns(dsCombo);
            mock.Setup(m => m.GetData(It.IsAny<string>(), It.IsAny<string>())).Returns(ds);
            mock.Setup(m => m.GetOnlyParameters(It.IsAny<string>())).Returns(GetParameter());
            mock.Setup(m => m.SaveDataAction(It.IsAny<string>(), It.IsAny<Dictionary<string, string>>(), It.IsAny<string>(), It.IsAny<DataTable>())).Returns(ds);
            mock.Setup(m => m.SaveData(It.IsAny<string>(), It.IsAny<DataTable>(), It.IsAny<string>())).Returns(ds);
            mock.Setup(m => m.GetDataActionOne(It.IsAny<string>(), It.IsAny<Dictionary<string, string>>(), It.IsAny<string>())).Returns(GetAcction());
            // Act
            ViabilityPrintController con = new ViabilityPrintController();
            builder.InitializeController(con);
            con.InjectDependencyTest(pathH.GetSettingFile());
            con.InjectDependencyTest(mock.Object);
            ActionResult action = con.SaveInventoryViability(GetForm()) as ActionResult;

            // Assert
            Assert.IsNotNull(action);
        }
        private DataSet GetDataSet()
        {
            DataSet ds = new DataSet();
            DataTable dt = new DataTable();
            dt.TableName = "viability_gbz_get_inventory_viability_order";
            DataColumn dcId = new DataColumn();
            dcId.ColumnName = "inventory_viability_id";
            dt.Columns.Add(dcId);
            dt.PrimaryKey = new DataColumn[] { dcId };
            dt.Columns.Add(new DataColumn() { ColumnName = "tested_date", DataType = System.Type.GetType("System.DateTime") });
            dt.Columns.Add(new DataColumn() { ColumnName = "percent_viable", DataType = System.Type.GetType("System.Int32") });
            dt.Columns.Add(new DataColumn() { ColumnName = "inventory_id", DataType = System.Type.GetType("System.Int32") });
            ds.Tables.Add(dt);
            DataTable viab = new DataTable();
            viab.TableName = "get_inventory_viability";
            viab.Columns.Add(new DataColumn() { ColumnName = "inventory_id", DataType = System.Type.GetType("System.Int32") });
            viab.Columns.Add(new DataColumn() { ColumnName = "inventory_viability_id", DataType = System.Type.GetType("System.Int32") });
            viab.Columns.Add(new DataColumn() { ColumnName = "inventory_viability_rule_id", DataType = System.Type.GetType("System.Int32") });
            ds.Tables.Add(viab);
            return ds;
        }

        private DataSet GetDataSetCombo()
        {
            DataSet ds = new DataSet();
            DataTable dt = new DataTable();
            dt.TableName = "viability_gbz_get_inventory_viability_rule";
            DataColumn dCId = new DataColumn();
            dCId.ColumnName = "inventory_viability_rule_id";
            dt.Columns.Add(dCId);
            dt.PrimaryKey = new DataColumn[] { dCId };
            dt.Columns.Add(new DataColumn() { ColumnName = "inventory_id", DataType = System.Type.GetType("System.Int32") });
            dt.Columns.Add(new DataColumn() { ColumnName = "inventory_viability_id", DataType = System.Type.GetType("System.Int32") });
            ds.Tables.Add(dt);
            return ds;
        }

        private SettingsHelp GetSetting()
        {
            return new SettingsHelp(serverId, moduleId, formId, pathH.GetSettingFile());
        }

        private FormCollection GetForm()
        {
            return new FormCollection() {
                { "order_request_id", "123" },
                {"parameters", ":order_request_id;123" }
            };
        }

        private string GetParameterS()
        {
            return "order_request_id:123";
        }

        private Dictionary<string, string> GetParameter()
        {
            return new Dictionary<string, string>()
            {
                ["order_request_id"] = "123",
                ["inventory_id"] = "12"
            };
        }

        private TestControllerBuilder GetBuilder()
        {
            TestControllerBuilder builder = new TestControllerBuilder();
            builder.Session.Add("server", serverId);
            builder.TempDataDictionary.Add("moduleId", moduleId);
            builder.TempDataDictionary.Add("formId", formId);
            builder.TempDataDictionary.Add("server", serverId);
            return builder;
        }

        private DataTable GetAcction()
        {
            DataTable dt = new DataTable();
            dt.TableName = "invenotry_acction";
            DataColumn dCId = new DataColumn();
            dCId.ColumnName = "invenotry_acction_id";
            dt.Columns.Add(dCId);
            dt.PrimaryKey = new DataColumn[] { dCId };
            dt.Columns.Add(new DataColumn() { ColumnName = "inventory_id", DataType = System.Type.GetType("System.Int32") });
            dt.Columns.Add(new DataColumn() { ColumnName = "inventory_viability_id", DataType = System.Type.GetType("System.Int32") });
            return dt;
        }
    }
}
