﻿using System;
using System.Text;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using MvcContrib.TestHelper;
using GB_Zone.Test.help;
using GrinGlobal.Zone.Controllers;
using System.Web.Mvc;
using System.Data;
using GrinGlobal.Zone.Helpers;
using Moq;
using GrinGlobal.Zone.Helpers.setting;
using Newtonsoft.Json;

namespace GB_Zone.Test.controller
{
    /// <summary>
    /// Summary description for ViabilityDataCollectionQR
    /// </summary>
    [TestClass]
    public class ViabilityDataCollectionTest
    {
        private string serverId = "01";
        private string moduleId = "Viability";
        private string formId = "invenotry_viability_datacollection";
        private TestControllerBuilder builder;
        private PathHelp pathH = new PathHelp();
        private DataTableHelp dataTable = new DataTableHelp();

        [TestMethod]
        public void ViabilityDataCollectionController_Index()
        {
            //Arrange
            builder = GetBuilder();
            ViabilityDataCollectionController con = new ViabilityDataCollectionController();
            builder.InitializeController(con);
            con.InjectDependencyTest(pathH.GetSettingFile());
            //Act
            ViewResult action = con.Index(moduleId, formId) as ViewResult;
            //Assert
            Assert.IsNotNull(action);
            Assert.IsNotNull(action.Model);
        }

        [TestMethod]
        public void ViabilityDataCollectionController_SearchInventory_ReturnsValue()
        {
            // Arrange
            TestControllerBuilder builder = GetBuilder();
            var mock = new Mock<IGrinGlobalSoapHelp>();
            mock.Setup(m => m.GetParameters(It.IsAny<FormCollection>(), It.IsAny<string>(), It.IsAny<Dictionary<string, string>>())).Returns(GetParameter());
            mock.Setup(m => m.Parameters_DictionaryToString(It.IsAny<Dictionary<string, string>>())).Returns(GetParameterS());
            mock.Setup(m => m.Parameters_StringToDictionary(It.IsAny<string>())).Returns(GetParameter());
            mock.Setup(m => m.GetData(It.IsAny<string>(), "")).Returns(GetDataSet());
            mock.Setup(m => m.SetH).Returns(GetSetting());
            mock.Setup(m => m.GetData("", It.IsAny<string>())).Returns(GetDataSet());
            mock.Setup(m => m.GetData(It.IsAny<string>(), It.IsAny<string>())).Returns(GetDataSet());
            mock.Setup(m => m.GetOnlyParameters(It.IsAny<string>())).Returns(GetParameter());
            mock.Setup(m => m.SaveDataAction(It.IsAny<string>(), It.IsAny<Dictionary<string, string>>(), It.IsAny<string>(), It.IsAny<DataTable>())).Returns(GetDataSet());
            mock.Setup(m => m.SaveData(It.IsAny<string>(), It.IsAny<DataTable>(), It.IsAny<string>())).Returns(GetDataSet());
            // Act
            ViabilityDataCollectionController con = new ViabilityDataCollectionController();
            builder.InitializeController(con);
            con.InjectDependencyTest(mock.Object);
            con.InjectDependencyTest(pathH.GetSettingFile());
            ActionResult action = con.SearchInventory(GetForm()) as ActionResult;
            // Assert
            Assert.IsNotNull(action);
        }

        [TestMethod]
        public void ViabilityDataCollectionController_Save_ReturnsValues()
        {
            // Arrange
            TestControllerBuilder builder = GetBuilder();
            var mock = new Mock<IGrinGlobalSoapHelp>();
            mock.Setup(m => m.GetParameters(It.IsAny<FormCollection>(), It.IsAny<string>(), It.IsAny<Dictionary<string, string>>())).Returns(GetParameter());
            mock.Setup(m => m.Parameters_DictionaryToString(It.IsAny<Dictionary<string, string>>())).Returns(GetParameterS());
            mock.Setup(m => m.Parameters_StringToDictionary(It.IsAny<string>())).Returns(GetParameter());
            mock.Setup(m => m.GetData(It.IsAny<string>(), "")).Returns(GetDataSet());
            mock.Setup(m => m.SetH).Returns(GetSetting());
            mock.Setup(m => m.GetData("", It.IsAny<string>())).Returns(GetDataSet());
            mock.Setup(m => m.GetData(It.IsAny<string>(), It.IsAny<string>())).Returns(GetDataSet());
            mock.Setup(m => m.GetOnlyParameters(It.IsAny<string>())).Returns(GetParameter());
            mock.Setup(m => m.SaveDataAction(It.IsAny<string>(), It.IsAny<Dictionary<string, string>>(), It.IsAny<string>(), It.IsAny<DataTable>())).Returns(GetDataSet());
            mock.Setup(m => m.SaveData(It.IsAny<string>(), It.IsAny<DataTable>(), It.IsAny<string>())).Returns(GetDataSet());
            // Act
            ViabilityDataCollectionController con = new ViabilityDataCollectionController();
            builder.InitializeController(con);
            con.InjectDependencyTest(pathH.GetSettingFile());
            con.InjectDependencyTest(mock.Object);
            mock.Setup(m => m.SetH).Returns(GetSetting());
            JsonResult action = con.Save(GetForm()) as JsonResult;
            // Assert
            Assert.IsNotNull(action);
            JsonResponse json = JsonConvert.DeserializeObject<JsonResponse>(action.Data.ToString());
            Assert.AreEqual(json.Success, 200);
        }

        [TestMethod]
        public void ViabilityDataCollectionController_GridView_ReturnsValue()
        {
            // Arrange
            TestControllerBuilder builder = GetBuilder();
            var mock = new Mock<IGrinGlobalSoapHelp>();
            mock.Setup(m => m.GetParameters(It.IsAny<FormCollection>(), It.IsAny<string>(), It.IsAny<Dictionary<string, string>>())).Returns(GetParameter());
            mock.Setup(m => m.Parameters_DictionaryToString(It.IsAny<Dictionary<string, string>>())).Returns(GetParameterS());
            mock.Setup(m => m.Parameters_StringToDictionary(It.IsAny<string>())).Returns(GetParameter());
            mock.Setup(m => m.GetData(It.IsAny<string>(), "")).Returns(GetDataSet());
            mock.Setup(m => m.SetH).Returns(GetSetting());
            mock.Setup(m => m.GetData("", It.IsAny<string>())).Returns(GetDataSet());
            mock.Setup(m => m.GetData(It.IsAny<string>(), It.IsAny<string>())).Returns(GetDataSet());
            mock.Setup(m => m.GetOnlyParameters(It.IsAny<string>())).Returns(GetParameter());
            mock.Setup(m => m.SaveDataAction(It.IsAny<string>(), It.IsAny<Dictionary<string, string>>(), It.IsAny<string>(), It.IsAny<DataTable>())).Returns(GetDataSet());
            mock.Setup(m => m.SaveData(It.IsAny<string>(), It.IsAny<DataTable>(), It.IsAny<string>())).Returns(GetDataSet());
            // Act
            ViabilityDataCollectionController con = new ViabilityDataCollectionController();
            builder.InitializeController(con);
            con.InjectDependencyTest(pathH.GetSettingFile());
            con.InjectDependencyTest(mock.Object);
            ActionResult action = con.GridViewInventoryViabilityData(GetForm()) as ActionResult;

            // Assert
            Assert.IsNotNull(action);
        }

        [TestMethod]
        public void ViabilityDataCollectionController_DetailPartitial_ReturnsValue()
        {
            // Arrange
            TestControllerBuilder builder = GetBuilder();
            var mock = new Mock<IGrinGlobalSoapHelp>();
            mock.Setup(m => m.GetParameters(It.IsAny<FormCollection>(), It.IsAny<string>(), It.IsAny<Dictionary<string, string>>())).Returns(GetParameter());
            mock.Setup(m => m.Parameters_DictionaryToString(It.IsAny<Dictionary<string, string>>())).Returns(GetParameterS());
            mock.Setup(m => m.Parameters_StringToDictionary(It.IsAny<string>())).Returns(GetParameter());
            mock.Setup(m => m.GetData(It.IsAny<string>(), "")).Returns(GetDataSet());
            mock.Setup(m => m.SetH).Returns(GetSetting());
            mock.Setup(m => m.GetData("", It.IsAny<string>())).Returns(GetDataSet());
            mock.Setup(m => m.GetData(It.IsAny<string>(), It.IsAny<string>())).Returns(GetDataSet());
            mock.Setup(m => m.GetOnlyParameters(It.IsAny<string>())).Returns(GetParameter());
            mock.Setup(m => m.SaveDataAction(It.IsAny<string>(), It.IsAny<Dictionary<string, string>>(), It.IsAny<string>(), It.IsAny<DataTable>())).Returns(GetDataSet());
            mock.Setup(m => m.SaveData(It.IsAny<string>(), It.IsAny<DataTable>(), It.IsAny<string>())).Returns(GetDataSet());
            // Act
            ViabilityDataCollectionController con = new ViabilityDataCollectionController();
            builder.InitializeController(con);
            con.InjectDependencyTest(pathH.GetSettingFile());
            con.InjectDependencyTest(mock.Object);
            ActionResult action = con.GridViewInventoryViability(GetForm()) as ActionResult;

            // Assert
            Assert.IsNotNull(action);
        }

        private SettingsHelp GetSetting()
        {
            return new SettingsHelp(serverId, moduleId, formId, pathH.GetSettingFile());
        }

        private FormCollection GetForm()
        {
            return new FormCollection() {
                { "order_request_id", "123" },
                {"parameters", ":order_request_id;123" },
                { "inventory_viability_id","1"},
                {"count_number","1" },
                {"replication_number","1" },
                { "tested_date",DateTime.Now.Year+"/"+DateTime.Now.Month+"/"+DateTime.Now.Day},
                { "counter_cooperator_id","1"}
            };
        }

        private string GetParameterS()
        {
            return "order_request_id:123";
        }

        private Dictionary<string, string> GetParameter()
        {
            return new Dictionary<string, string>()
            {
                ["order_request_id"] = "123",
                ["inventory_id"] = "12"
            };
        }

        private TestControllerBuilder GetBuilder()
        {
            TestControllerBuilder builder = new TestControllerBuilder();
            builder.Session.Add("server", serverId);
            builder.TempDataDictionary.Add("moduleId", moduleId);
            builder.TempDataDictionary.Add("formId", formId);
            builder.TempDataDictionary.Add("server", serverId);
            return builder;
        }


        private DataSet GetDataSet()
        {
            DataSet ds = new DataSet();
            ds.Tables.Add(dataTable.CreateInventoryTable("viability_wizard_get_inventory"));
            ds.Tables.Add(dataTable.CreateInventoryVaibilityTable("viability_wizard_get_inventory_viability"));
            ds.Tables.Add(dataTable.CreateInventoryViabilityDataTable("get_inventory_viability_data"));
            ds.Tables.Add(dataTable.CreateInventoryViabilityDataTable("viability_wizard_get_inventory_viability_data"));
            ds.Tables.Add(dataTable.CreateInventoryViabilityRuleTable("viability_gbz_get_inventory_viability_rule"));
            ds.Tables.Add(dataTable.CreateInventoryViabilityRuleTable("viability_wizard_get_inventory_viability_rule"));
            ds.Tables.Add(dataTable.CreateInventoryActionTable("get_inventory_action"));
            ds.Tables.Add(dataTable.CreateAccesionTable("get_accession"));
            return ds;
        }
    }
}
