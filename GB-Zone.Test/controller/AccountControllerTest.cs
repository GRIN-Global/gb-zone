﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using GrinGlobal.Zone.Controllers;
using System.Web.Mvc;
using Moq;
using MvcContrib.TestHelper;
using System.Data;
using GrinGlobal.Zone.Models;

namespace GB_Zone.Test.controller
{
    [TestClass]
    public class AccountControllerTest
    {
        public void AccountController_Index()
        {
            // Arrange

            // Act
            AccountController cont = new AccountController();
            ActionResult res = cont.Index();

            // Assert
            Assert.IsNotNull(res);
        }

        [TestMethod]
        public void AccountController_Loging()
        {
            // Arrange

            // Act
            AccountController cont = new AccountController();
            ActionResult res = cont.Login();

            // Assert
            Assert.IsNotNull(res);
        }

        [TestMethod]
        public void AccountController_Loging_ErrorUser()
        {
            // Arrange
            //var mock = new Mock<IAccountViewModels>();
            //mock.Setup(m => m.validateGG(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>()));

            // Act
            AccountController con = new AccountController();
            //ActionResult action = con.Login(mock.Object, It.IsAny<string>()) as ActionResult;

            // Assert
            //Assert.IsNotNull(action);
        }

        [TestMethod]
        public void AccountController_Loging_OkUser()
        {/*
            // Arrange
            TestControllerBuilder builder = new TestControllerBuilder();
            var mock = new Mock<AccountViewModels>();
            //var mockAuthentication = new Mock<IAuth>();
            //mockAuthentication.Setup(m => m.DoAuth(It.IsAny<string>(), false));
            DataTable ds = new DataTable();
            ds.Columns.Add("is_enabled", typeof(string));
            ds.Columns.Add("server", typeof(string));
            ds.Columns.Add("username", typeof(string));
            ds.Columns.Add("userkey", typeof(string));
            DataRow dr = ds.NewRow();
            dr["is_enabled"] = "Y";
            ds.Rows.Add(dr);
            mock.Setup(m => m.validateGG(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>())).Returns(ds);
            mock.SetupGet(m => m.Server).Returns("01");
            mock.SetupGet(m => m.UserName).Returns("USUARIO");
            mock.SetupGet(m => m.Password).Returns("PASSS");
            //var mockAuthen = new Mock<IFormsAuthenticationService>();
//            mockAuthen.Setup(m => m.SignIn(It.IsAny<string>()));

            // Act
            AccountController con = new AccountController();
            builder.InitializeController(con);
            //ActionResult action = con.Login(mock.Object, It.IsAny<string>()) as ActionResult;

            // Assert
            //Assert.IsNotNull(action);
            */
        }
    }
}
