﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using GrinGlobal.Zone.Controllers;
using Moq;
using System.Web.Mvc;
using MvcContrib.TestHelper;
using System.Data;
using GrinGlobal.Zone.Classes;
using GB_Zone.Test.help;
using GrinGlobal.Zone.Helpers;
using System.Collections.Generic;
using GrinGlobal.Zone.Helpers.setting;

namespace GB_Zone.Test.controller
{
    [TestClass]
    public class OrderControllerTest
    {
        private string serverId = "01";
        private string moduleId = "Order";
        private string formId = "gbz_get_order_check";
        private TestControllerBuilder builder;
        private PathHelp pathH = new PathHelp();
        [TestMethod]
        public void OrderController_Index()
        {
            builder = GetBuilder();
            OrderController con = new OrderController();
            builder.InitializeController(con);
            con.InjectDependencyTest(pathH.GetSettingFile());
            ViewResult action = con.Index(moduleId, formId) as ViewResult;


            Assert.IsNotNull(action);
            Assert.IsNotNull(action.Model);
            Assert.IsNotNull(action.ViewData["moduleId"]);
            Assert.IsNotNull(action.ViewData["formId"]);
        }

        [TestMethod]
        public void OrderController_IndexForm_ReturnsValue()
        {
            // Arrange
            TestControllerBuilder builder = GetBuilder();
            var mock = new Mock<IGrinGlobalSoapHelp>();

            DataSet ds = GetDataSet();

            DataTable dtA = new DataTable();
            dtA.TableName = "get_order_request_item_action";
            mock.Setup(m => m.GetParameters(GetForm(), "", null)).Returns(GetParameter());
            mock.Setup(m => m.Parameters_DictionaryToString(GetParameter())).Returns(GetParameterS());
            mock.Setup(m => m.Parameters_DictionaryToString(null)).Returns(GetParameterS());
            mock.Setup(m => m.GetData(It.IsAny<string>(), "")).Returns(ds);
            mock.Setup(m => m.SetH).Returns(GetSetting());
            mock.Setup(m => m.GetDataActionOne(It.IsAny<string>(), new Dictionary<string, string>(), It.IsAny<string>())).Returns(dtA);
            // Act
            OrderController con = new OrderController();
            builder.InitializeController(con);
            con.InjectDependencyTest(mock.Object);
            con.InjectDependencyTest(pathH.GetSettingFile());
            ActionResult action = con.Index(GetForm()) as ActionResult;

            // Assert
            Assert.IsNotNull(action);
        }

        [TestMethod]
        public void OrderController_IndexForm_ReturnsNull()
        {
            // Arrange
            TestControllerBuilder builder = GetBuilder();
            var mock = new Mock<IGrinGlobalSoapHelp>();

            DataSet ds = GetDataSet();
            DataTable dtA = new DataTable();
            dtA.TableName = "get_order_request_item_action";
            mock.Setup(m => m.GetParameters(GetForm(), "", null)).Returns(GetParameter());
            mock.Setup(m => m.Parameters_DictionaryToString(GetParameter())).Returns(GetParameterS());
            mock.Setup(m => m.Parameters_DictionaryToString(null)).Returns(GetParameterS());
            mock.Setup(m => m.GetData(It.IsAny<string>(), "")).Returns(ds);
            mock.Setup(m => m.SetH).Returns(GetSetting());
            mock.Setup(m => m.GetDataActionOne(It.IsAny<string>(), new Dictionary<string, string>(), It.IsAny<string>())).Returns(dtA);
            // Act
            OrderController con = new OrderController();
            builder.InitializeController(con);
            con.InjectDependencyTest(pathH.GetSettingFile());
            con.InjectDependencyTest(mock.Object);
            mock.Setup(m => m.SetH).Returns(GetSetting());
            ActionResult action = con.Index(GetForm()) as ActionResult;

            // Assert
            Assert.IsNotNull(action);
        }

        [TestMethod]
        public void OrderController_GridView_ReturnsValue()
        {
            // Arrange
            TestControllerBuilder builder = GetBuilder();
            var mock = new Mock<IGrinGlobalSoapHelp>();
            DataSet ds = GetDataSet();
            DataTable dtA = new DataTable();
            dtA.TableName = "get_order_request_item_action";
            mock.Setup(m => m.GetParameters(GetForm(), "", null)).Returns(GetParameter());
            mock.Setup(m => m.Parameters_DictionaryToString(GetParameter())).Returns(GetParameterS());
            mock.Setup(m => m.GetData(It.IsAny<string>(), "")).Returns(ds);
            mock.Setup(m => m.SetH).Returns(GetSetting());
            mock.Setup(m => m.GetDataActionOne(It.IsAny<string>(), new Dictionary<string, string>(), It.IsAny<string>())).Returns(dtA);
            // Act
            OrderController con = new OrderController();
            builder.InitializeController(con);
            con.InjectDependencyTest(pathH.GetSettingFile());
            con.InjectDependencyTest(mock.Object);
            ActionResult action = con.GridView(GetParameterS()) as ActionResult;

            // Assert
            Assert.IsNotNull(action);
        }

        [TestMethod]
        public void OrderController_BatchUpdateAction_SaveEmpyt()
        {
            // Arrange
            TestControllerBuilder builder = GetBuilder();
            var mock = new Mock<IGrinGlobalSoapHelp>();
            DataTable dtA = new DataTable();
            dtA.TableName = "get_order_request_item_action";
            mock.Setup(m => m.GetParameters(GetForm(), "", null)).Returns(GetParameter());
            mock.Setup(m => m.Parameters_DictionaryToString(GetParameter())).Returns(GetParameterS());
            mock.Setup(m => m.GetData(It.IsAny<string>(), "")).Returns(GetDataSet());
            mock.Setup(m => m.SetH).Returns(GetSetting());
            mock.Setup(m => m.GetDataActionOne(It.IsAny<string>(), new Dictionary<string, string>(), It.IsAny<string>())).Returns(dtA);
            // Act
            OrderController con = new OrderController();
            builder.InitializeController(con);
            con.InjectDependencyTest(pathH.GetSettingFile());
            con.InjectDependencyTest(mock.Object);
            ActionResult action = con.BatchUpdateAction(GetParameterS()) as ActionResult;

            // Assert
            Assert.IsNotNull(action);
        }
        private DataSet GetDataSet()
        {
            DataSet ds = new DataSet();
            DataTable dt = new DataTable();
            dt.TableName = "check_gbz_get_order_list";
            DataColumn dCView = new DataColumn();
            dCView.ColumnName = "data_view_action";
            DataColumn dCChcek = new DataColumn();
            dCChcek.ColumnName = "check_item";
            DataColumn dCInv = new DataColumn();
            dCInv.ColumnName = "inventory_number";
            DataColumn dCId = new DataColumn();
            dCId.ColumnName = "order_request_item_id";
            dt.Columns.Add(dCId);
            dt.PrimaryKey = new DataColumn[] { dCId };
            dt.Columns.Add(dCView);
            dt.Columns.Add(dCChcek);
            dt.Columns.Add(dCInv);
            ds.Tables.Add(dt);
            return ds;
        }

        private SettingsHelp GetSetting()
        {
            return new SettingsHelp(serverId, moduleId, formId, pathH.GetSettingFile());
        }

        private FormCollection GetForm()
        {
            return new FormCollection() {
                { "moduleId", moduleId},
                { "formId", formId },
                { ":orderrequestid", "123" }
            };
        }

        private string GetParameterS()
        {
            return ":orderrequestid:123";
        }

        private Dictionary<string, string> GetParameter()
        {
            return new Dictionary<string, string>()
            {
                [":orderrequestid"] = "123"
            };
        }

        private TestControllerBuilder GetBuilder()
        {
            TestControllerBuilder builder = new TestControllerBuilder();
            builder.Session.Add("server", serverId);
            builder.TempDataDictionary.Add("moduleId", moduleId);
            builder.TempDataDictionary.Add("formId", formId);
            builder.TempDataDictionary.Add("server", serverId);
            return builder;
        }
    }
}
