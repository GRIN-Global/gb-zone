﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using GB_Zone.Test.help;

namespace GB_Zone.Test
{
    [TestClass]
    public class EssentialTest
    {
        [TestMethod]
        public void SettingFile()
        {
            PathHelp setting = new PathHelp();

            string path = setting.GetSettingFile();

            Assert.AreNotEqual(path, "");
        }
    }
}
