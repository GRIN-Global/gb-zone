﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using GB_Zone.Test.help;
using GrinGlobal.Zone.Helpers;
using System.Collections.Generic;
using MvcContrib.TestHelper;
using GrinGlobal.Zone.Controllers;
using Microsoft.AspNetCore.Http;
using System.Net;
using System.Text;
using System.IO;
using System.Net.Http;
using System.Xml;
using System.Diagnostics;

namespace GB_Zone.Test.dataview
{
    [TestClass]
    public class DataviewTest
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        private PathHelp path = new PathHelp();

        [TestMethod]
        public void ValidateExist()
        {
            // Arrange
            SettingHelp hSet = new SettingHelp(path.GetSettingFile());
            List<SettingData> sers = hSet.ReadImportServer();
            // Act
            foreach (SettingData ser in sers)
            {
                List<string> dataviews = hSet.GetAllDataviewByServer(ser.Server);
                foreach (string dataviewName in dataviews)
                {
                    int success = CallWebService(ser, dataviewName);
                    // Assert
                    if(success != 200)
                    {
                        log.Error(string.Format("###### Error with dataview {0}", dataviewName));
                    }
                    Assert.AreEqual(success, 200);
                    //Assert.AreNotEqual(dic.Count, 0);
                }
            }
        }

        private int CallWebService(SettingData ser, string dataviewName)
        {
            int success = 300;
            var _url = ser.Url;
            var _action = "http://tempuri.org/GetData";

            XmlDocument soapEnvelopeXml = CreateSoapEnvelope(ser, dataviewName);
            HttpWebRequest webRequest = CreateWebRequest(_url, _action);
            InsertSoapEnvelopeIntoWebRequest(soapEnvelopeXml, webRequest);

            // begin async call to web request.
            IAsyncResult asyncResult = webRequest.BeginGetResponse(null, null);

            // suspend this thread until call is complete. You might want to
            // do something usefull here like update your UI.
            asyncResult.AsyncWaitHandle.WaitOne();

            // get the response from the completed web request.
            XmlDocument respond = new XmlDocument();
            try
            {
                using (WebResponse webResponse = webRequest.EndGetResponse(asyncResult))
                {
                    using (StreamReader rd = new StreamReader(webResponse.GetResponseStream()))
                    {
                        string xmlData = rd.ReadToEnd();
                        respond.LoadXml(xmlData);
                        success = 200;
                    }
                }
            }
            catch (WebException webex)
            {
                WebResponse errResp = webex.Response;
                using (Stream respStream = errResp.GetResponseStream())
                {
                    StreamReader reader = new StreamReader(respStream);
                    string text = reader.ReadToEnd();
                    if (text.Contains("No dataview named"))
                    {
                        success = 404;
                    }
                }
            }
            return success;
        }

        private static HttpWebRequest CreateWebRequest(string url, string action)
        {
            HttpWebRequest webRequest = (HttpWebRequest)WebRequest.Create(url);
            webRequest.Headers.Add("SOAPAction", action);
            webRequest.ContentType = "text/xml;charset=\"utf-8\"";
            webRequest.Accept = "text/xml";
            webRequest.Method = "POST";
            return webRequest;
        }

        private XmlDocument CreateSoapEnvelope(SettingData ser, string dataviewName)
        {
            XmlDocument soapEnvelopeDocument = new XmlDocument();
            string strRequest = @"<?xml version='1.0' encoding='utf-8'?>
<soap:Envelope xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance' xmlns:xsd='http://www.w3.org/2001/XMLSchema' xmlns:soap='http://schemas.xmlsoap.org/soap/envelope/'>
  <soap:Body>
    <GetData xmlns='http://tempuri.org/'>
      <suppressExceptions>0</suppressExceptions>
      <userName>@user</userName>
      <password>@pass</password>
      <dataviewName>@dataviewName</dataviewName>
      <delimitedParameterList></delimitedParameterList>
      <offset>0</offset>
      <limit>1</limit>
      <options>0</options>
    </GetData>
  </soap:Body>
</soap:Envelope>
";
            strRequest = strRequest.Replace("'", "\"");
            strRequest = strRequest.Replace("@user", ser.User).Replace("@pass", ser.Pass).Replace("@dataviewName", dataviewName);
            soapEnvelopeDocument.LoadXml(strRequest);
            return soapEnvelopeDocument;
        }

        private void InsertSoapEnvelopeIntoWebRequest(XmlDocument soapEnvelopeXml, HttpWebRequest webRequest)
        {
            using (Stream stream = webRequest.GetRequestStream())
            {
                soapEnvelopeXml.Save(stream);
            }
        }
    }
}
